<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 4/11/2017
 * Time: 11:22 AM
 */
namespace app\assets;

use app\components\View;
use yii\web\AssetBundle;

class BitcoinAsset extends AssetBundle {

	public $basePath  = '@webroot';

	public $baseUrl   = '@web';

	public $js        = [
		'crypto/cryptobox.min.js',
	];

	public $jsOptions = ['position' => View::POS_HEAD];

	public $depends   = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapPluginAsset',
	];
}