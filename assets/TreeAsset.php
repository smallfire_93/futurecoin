<?php
/**
 * Created by PhpStorm.
 * User: Yamon-PC
 * Date: 01-Mar-17
 * Time: 5:16 PM
 */
namespace app\assets;

use yii\web\AssetBundle;

class TreeAsset extends AssetBundle {

	public $basePath = '@webroot';

	public $baseUrl  = '@web';

	public $css      = [];

	public $js       = [
		'tree/jquery.jOrgChart.js',
		'js/orgChart.js',
		'tree/prettify.js',
	];
}