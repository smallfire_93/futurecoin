<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 11/12/2017
 * Time: 4:32 PM
 */

namespace app\commands;

use app\components\Model;
use app\models\CurrentBonus;
use yii\console\Controller;

class CurrentController extends Controller {

	/**
	 */
	public function actionIndex() {
		$currentBonus = CurrentBonus::find()->where([
			'>',
			'direct_bonus',
			0,
		])->orWhere([
			'>',
			'matching_bonus',
			0,
		])->orWhere([
			'>',
			'weak_branch_bonus',
			0,
		])->all();
		foreach($currentBonus as $bonus) {
			$bonus->updateAttributes(['total_bonus' => $bonus->direct_bonus + $bonus->matching_bonus + $bonus->weak_branch_bonus]);
			$bonus->updateAttributes(['current_cash' => Model::getCash($bonus->matching_bonus + $bonus->direct_bonus + $bonus->weak_branch_bonus)]);
			$bonus->updateAttributes(['current_trading' => Model::getTradding($bonus->matching_bonus + $bonus->direct_bonus + $bonus->weak_branch_bonus)]);
		}
	}
}