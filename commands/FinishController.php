<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */
namespace app\commands;

use app\models\BonusTransaction;
use app\models\CashTransaction;
use app\models\CurrentBonus;
use app\models\LevelCurrent;
use app\models\TradingTransaction;
use app\models\User;
use Codeception\Event\PrintResultEvent;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class FinishController extends Controller {

	/**
	 * This command echoes what you have entered as the message.
	 *
	 * @param string $message the message to be echoed.
	 *
	 */
	public function actionIndex() {
		$this->returnBonus();
	}

	public function returnCurrentLevel($bonus) {
		/** @var CurrentBonus $bonus */
		$current_level = LevelCurrent::findOne(['id' => $bonus->user_id]);
		if($current_level) {
		} else {
			$current_level = new LevelCurrent();
			//			$current_level
		}
	}

	/**
	 * @return array|\yii\db\ActiveRecord[]
	 *
	 */
	public function returnBonus() {
		$bonuses = CurrentBonus::find()->where([
			'>',
			'total_bonus',
			0,
		])->orderBy('weak_branch_bonus ASC')->all();
		if($bonuses) {
			foreach($bonuses as $bonus) {
				/** @var CurrentBonus $bonus */
				$account = $bonus->users->accounts;
				if($account) {
					$cash_transaction          = new CashTransaction();
					$cash_transaction->user_id = $bonus->users->id;
					$cash_transaction->type    = $cash_transaction::TYPE_BON;
					$cash_transaction->money   = $bonus->current_cash;
					$cash_transaction->status  = $cash_transaction::STATUS_SUCCESS;
					$cash_transaction->date    = date('Y-m-d H:i:s');
					$cash_transaction->save();
					$trading          = new TradingTransaction();
					$trading->user_id = $bonus->users->id;
					$trading->type    = $cash_transaction::TYPE_BON;
					$trading->money   = $bonus->current_trading;
					$trading->status  = $cash_transaction::STATUS_SUCCESS;
					$trading->date    = date('Y-m-d H:i:s');
					$trading->save();
					$bonus_transaction          = new BonusTransaction;
					$bonus_transaction->user_id = $bonus->users->id;;
					$bonus_transaction->direct_bonus      = $bonus->direct_bonus;
					$bonus_transaction->matching_bonus    = $bonus->matching_bonus;
					$bonus_transaction->receipted_date    = date('Y-m-d H:i:s');
					$bonus_transaction->cash              = $bonus->current_cash;
					$bonus_transaction->total_bonus       = $bonus->total_bonus;
					$bonus_transaction->trading           = $bonus->current_trading;
					$bonus_transaction->weak_branch_bonus = $bonus->weak_branch_bonus;
					$bonus_transaction->save();
					//TODO thêm bonus transasction;
					$bonus->users->updateAttributes(['total_bonus' => $bonus->users->total_bonus + $bonus->total_bonus]);
					$bonus->users->accounts->updateAttributes([
						'cash_account'    => $account->cash_account + $bonus->current_cash,
						'trading_account' => $account->trading_account + $bonus->current_trading,
					]);
					$bonus->updateAttributes([
						'current_cash'      => 0,
						'current_trading'   => 0,
						'total_bonus'       => 0,
						'direct_bonus'      => 0,
						'matching_bonus'    => 0,
					]);
					$bonus->users->resetBranch();
				}
			}
		}
	}
}
