<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */
namespace app\commands;

use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class HelloController extends Controller {

	/**
	 * This command echoes what you have entered as the message.
	 *
	 * @param string $message the message to be echoed.
	 */
	public function actionIndex($message = 'hello world') {
			$money = $this->getTotalMoney(2000000,12,2000000);
		echo $money . "\n";
	}

	public function getMoney($money) {
		$rate  = 0.07;
		$money = $money + ($money * $rate);
		return $money;
	}

	public function getTotalMoney($money, $i = 1, $total = 0) {
		$new_money = $this->getMoney($money);
		$total     = $new_money;
		if($i < 12) {
			$i     = $i + 1;
			$total = $this->getTotalMoney($new_money, $i, $total);
		}
		return $total;
	}
}
