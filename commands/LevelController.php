<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */
namespace app\commands;

use app\models\LevelCurrent;
use app\models\User;
use Codeception\Event\PrintResultEvent;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class LevelController extends Controller {

	/**
	 * This command echoes what you have entered as the message.
	 *
	 * @param string $message the message to be echoed.
	 *
	 */
	public function actionIndex() {
		$level_users = LevelCurrent::find()->where([
			'>=',
			'current_bv',
			10000,
		])->all();
		if($level_users) {
			foreach($level_users as $level_user) {
				$level_order = $level_user->getCurrentLevel();
				if($level_order) {
					$level_user->updateAttributes(['level_id' => $level_order]);
				}
			}
		}
	}
}
