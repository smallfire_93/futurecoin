<?php
/**
 * @link      http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license   http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\components\Model;
use app\models\User;
use Codeception\Event\PrintResultEvent;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since  2.0
 */
class MatchController extends Controller {

	/**
	 * This command echoes what you have entered as the message.
	 *
	 * @param string $message the message to be echoed.
	 *
	 */
	public function actionIndex() {
		//			$money = $this->getTotalMoney(2000000,12,2000000);
		$users = $this->getUserMatching();
		if($this->getUserMatching()) {
			foreach($users as $user) {
				/** @var User $user */
				if($user->getLeftChild()->exists() && $user->getRightChild()->exists()) {
					$matching_array = $this->getTotalMatching($user->id, 0.1, [], $user->accounts->package->level, 1);
					$matching_bonus = array_sum($matching_array);
					if($matching_bonus > 0) {
						$user->currentBonus->updateAttributes([
							'matching_bonus' => $matching_bonus,
//							'total_bonus'    => $user->currentBonus->matching_bonus + $user->currentBonus->direct_bonus + $user->currentBonus->weak_branch_bonus,
						]);
//						$user->currentBonus->updateAttributes(['current_cash' => Model::getCash($user->currentBonus->matching_bonus + $user->currentBonus->direct_bonus + $user->currentBonus->weak_branch_bonus) ]);
//						$user->currentBonus->updateAttributes(['current_trading' => Model::getTradding($user->currentBonus->matching_bonus + $user->currentBonus->direct_bonus + $user->currentBonus->weak_branch_bonus)]);
					}
				}
			}
		}
	}

	public function getUserMatching() {
		$first_condition = User::find()->innerJoinWith('package')->where(['status' => User::STATUS_ACTIVATED])->andWhere('package.level >0')->all();
		return $first_condition;
	}

	public function addMatchingBonus(User $user) {
		if($user->accounts->package->level) {
			$level = $user->accounts->package->level;
		}
	}

	/**
	 * @return array
	 */
	public function getDirectMatching($user, $rate) {
		$directs = User::find()->where(['sponsor_id' => $user])->all();
		$total   = [];
		foreach($directs as $direct) {
			if($direct->currentBonus) {
				$total[$direct->id] = $rate * $direct->currentBonus->weak_branch_bonus;
			}
		}
		return $total;
	}

	public function getTotalMatching($user, $rate, $total = [], $level, $dif) {
		$total_direct = $this->getDirectMatching($user, $rate);
		if(count($total_direct) > 0 && is_array($total_direct)) {
			$total = ArrayHelper::merge($total, $total_direct);
		}
		if($dif < $level) {
			if(($dif > 2 && $dif < 5) || $dif > 5) {
				$rate = $rate + 0.05;
			}
			$dif = $dif + 1;
			foreach($total_direct as $id => $value) {
				$total = $this->getTotalMatching($id, $rate, $total, $level, $dif);
			}
		}
		return $total;
	}

	public function getTotal($id) {
		/**@var self[] $lv1s */
		$array_children = [];
		$lv1s           = User::find()->where([
			'sponsor_id' => $id,
		])->all();
		foreach($lv1s as $lv1) {
			$array_children[] = $lv1->id;
		}
		return $array_children;
	}

	/**
	 * @param       $parent_id
	 * @param array $array_children
	 *
	 * @return array
	 */
	public function getTotalChildren($parent_id, $array_children = []) {
		$tree = $this->getTotal($parent_id);
		if(count($tree) > 0 && is_array($tree)) {
			$array_children = ArrayHelper::merge($array_children, $tree);
		}
		foreach($tree as $item => $id) {
			$array_children = $this->getTotalChildren($id, $array_children);
		}
		return $array_children;
	}
}
