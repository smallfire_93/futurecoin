<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 7/22/2017
 * Time: 4:35 PM
 */

namespace app\commands;

use app\models\StartSetting;
use yii\console\Controller;

class StartController extends Controller {

	public function actionIndex() {
		$model = StartSetting::find()->where(['DATE_FORMAT(web_start,"%Y-%m-%d")' => date('Y-m-d', strtotime(date('Y-m-d') . ' -1 day'))])->one();
		if($model) {
			$new_day            = new StartSetting();
			$new_day->web_start = date('Y-m-d H:i:s');
			if($model->token_price >= 0.2) {
				$new_day->token_price   = 0.1;
				$new_day->coin_price    = 0.05;
				$new_day->token_split   = $model->token_split + 1;
				$new_day->split_percent = 0;
			} else {
				if($model->token_price + 0.001162796 >= 0.2) {
					$new_day->token_price   = 0.2;
					$new_day->coin_price    = 0.1;
					$new_day->token_split   = $model->token_split;
					$new_day->split_percent = 100;
				} else {
					$new_day->token_price   = $model->token_price + 0.0017075;
					$new_day->coin_price    = $new_day->token_price / 2;
					$new_day->split_percent = $model->split_percent + 1.162;
					$new_day->token_split   = $model->token_split;
				};
			}
			$new_day->save();
		}
	}
}