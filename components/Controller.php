<?php

namespace app\components;

use app\models\User;
use Yii;
use yii\helpers\Url;

class Controller extends \yii\web\Controller {

	/**@var User */
	public $user;

	/**
	 * Khởi tạo người dùng đã đăng nhập
	 * {@inheritDoc}
	 */
	public function init() {
		parent::init();
		if(!Yii::$app->session->isActive) {
			Yii::$app->session->open();
		}
		$this->user         = \Yii::$app->user->identity;
		$this->view->params = array_merge_recursive($this->view->params, Yii::$app->params);
	}

	/**
	 * {@inheritDoc}
	 */
	public function beforeAction($action) {
		//if(Yii::$app->controller->action->id!='reset-key'){
		//	echo 'a';die;
		//}else{
		//	echo "b";die;
		//}
		if(Yii::$app->user->isGuest && Yii::$app->controller->action->id !== 'login' && Yii::$app->controller->action->id !== 'sign-up' && Yii::$app->controller->action->id != 'reset-key' && Yii::$app->controller->action->id != 'reset') {
			$this->redirect(Url::to(['/site/login']));
			return false;
		} else {
			return parent::beforeAction($action);
		}
	}
}