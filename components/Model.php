<?php

namespace app\components;

use app\models\form\SignUpForm;
use app\models\Package;
use app\models\TokenTransaction;
use app\models\User;
use Yii;
use yii\console\Application;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\UploadedFile;

class Model extends ActiveRecord {

	/**@var User */
	public $user;

	const FEE = 25;

	/**
	 *  * Khởi tạo người dùng đã đăng nhập
	 */
	/**
	 * {@inheritDoc}
	 */
	public function __construct($config = []) {
		parent::__construct($config);
		if(!Yii::$app instanceof Application) {
			$this->user = Yii::$app->user->identity;
		}
	}

	public function uploadPicture($picture = '', $attribute) {
		// get the uploaded file instance. for multiple file uploads
		// the following data will return an array (you may need to use
		// getInstances method)
		$img = UploadedFile::getInstance($this, $attribute);
		// if no image was uploaded abort the upload
		if(empty($img)) {
			return false;
		}
		// generate a unique file name
		$dir = Yii::getAlias('@app/web') . '/uploads/' . $this->tableName() . '/';
		if(!is_dir($dir)) {
			@mkdir($dir, 0777, true);
		}
		$ext            = $img->getExtension();
		$this->$picture = $this->getPrimaryKey() . '_' . "$picture" . ".{$ext}";
		// the uploaded image instance
		return $img;
	}

	public function getPictureUrl($picture = '') {
		Yii::$app->params['uploadUrl'] = Yii::$app->urlManager->baseUrl . '/uploads/' . $this->tableName() . '/';
		$image                         = !empty($this->$picture) ? $this->$picture : Yii::$app->urlManager->baseUrl . '/uploads/no_image_thumb.gif';
		clearstatcache();
		if(is_file(Yii::getAlias("@app/web") . '/uploads/' . $this->tableName() . '/' . $image)) {
			return Yii::$app->params['uploadUrl'] . $image;
		} else {
			return $image;
		}
	}

	public function getPictureFile($picture = '') {
		$dir = Yii::getAlias('@app/web') . '/uploads/' . $this->tableName() . '/';
		return isset($this->$picture) ? $dir . $this->$picture : null;
	}

	/**
	 * Active người dùng
	 * Cộng token khi kick gói
	 */
	public function activeUser($package_id) {
		$package = Package::findOne($package_id);
		if($package != null) {
			if($this->user->accounts->package_id == null) {
				$this->user->accounts->updateAttributes(['package_id' => $package_id]);
			} else {
				if($this->user->accounts->package->money < $package->money) {
					$this->user->accounts->updateAttributes(['package_id' => $package_id]);
				};
			}
			if($this->user->status != User::STATUS_ACTIVATED) {
				$this->user->updateAttributes([
					'status'    => User::STATUS_ACTIVATED,
					'active_at' => date('Y-m-d H:i:s'),
				]);
			}
			$this->user->accounts->updateAttributes(['token' => $this->user->accounts->token + $package->token]);
			$this->user->accounts->updateAttributes(['total_tokens' => $this->user->accounts->total_tokens + $package->token]);
			$token_tran           = new TokenTransaction();
			$token_tran->user_id  = $this->user->id;
			$token_tran->quantity = $package->token;
			$token_tran->type     = $token_tran::TYPE_PAC;
			$token_tran->date     = date('Y-m-d H:i:s');
			$token_tran->status   = $token_tran::STATUS_SUC;
			$token_tran->save();
			return true;
		} else {
			return false;
		}
	}

	public function activeUserManual($package_id, $user_id) {
		$user    = User::findOne($user_id);
		$package = Package::findOne($package_id);
		if($package != null) {
			if($user->accounts->package_id == null) {
				$user->accounts->updateAttributes(['package_id' => $package_id]);
			} else {
				if($user->accounts->package->money < $package->money) {
					$user->accounts->updateAttributes(['package_id' => $package_id]);
				};
			}
			if($user->status != User::STATUS_ACTIVATED) {
				$user->updateAttributes([
					'status'    => User::STATUS_ACTIVATED,
					'active_at' => date('Y-m-d H:i:s'),
				]);
			}
			$user->accounts->updateAttributes(['token' => $user->accounts->token + $package->token]);
			$user->accounts->updateAttributes(['total_tokens' => $user->accounts->total_tokens + $package->token]);
			$token_tran           = new TokenTransaction();
			$token_tran->user_id  = $user->id;
			$token_tran->quantity = $package->token;
			$token_tran->type     = $token_tran::TYPE_PAC;
			$token_tran->date     = date('Y-m-d H:i:s');
			$token_tran->status   = $token_tran::STATUS_SUC;
			$token_tran->save();
			return true;
		} else {
			return false;
		}
	}

	/**
	 *Hàm trả về mảng những tuyến dưới cho cây nhị phân
	 */
	public function getArrayChildren($id, $level = 0) {
		/**@var User $lv1s */
		$array_children = [];
		$lv1s           = User::find()->where([
			'parent_id' => $id,
		])->orderBy(['position' => SORT_ASC])->all();
		$count          = User::find()->where([
			'parent_id' => $id,
		])->orderBy(['position' => SORT_ASC])->count();
		$user_model     = new SignUpForm();
		if($id == $user_model->getLastRight($this->user)->id) {
			$url_right = true;
		} else {
			$url_right = false;
		}
		if($id == $user_model->getLastLeft($this->user)->id) {
			$url_left = true;
		} else {
			$url_left = false;
		}
		if($count > 1) {
			foreach($lv1s as $lv1) {
				if($level <= 0) {
					$child = $this->getArrayChildren($lv1->id, 1);
				} else {
					$child = [];
				}
				$array_children[] = [
					'name'     => $lv1->username,
					'active'   => $lv1->status,
					'children' => $child,
					'href'     => Url::to([
						'/user/network',
						'key' => $lv1->password,
					]),
				];
			}
		} elseif($count == 1) {
			foreach($lv1s as $lv1) {
				if($level <= 0) {
					$child = $this->getArrayChildren($lv1->id, 1);
				} else {
					$child = [];
				}
				if($lv1->position == User::POSITION_LEFT) {
					$array_children = [
						[
							'name'     => $lv1->username,
							'active'   => $lv1->status,
							'children' => $child,
							'href'     => Url::to([
								'/user/network',
								'key' => $lv1->password,
							]),
						],
						[
							'name'     => 'New user',
							'active'   => User::STATUS_REGISTER,
							'children' => [],
							'href'     => $url_right ? Url::to([
								'/site/sign-up',
								'key' => User::POSITION_RIGHT,
							]) : '#',
						],
					];
				} else {
					$array_children = [
						[
							'name'     => 'New user',
							'active'   => User::STATUS_REGISTER,
							'children' => [],
							'href'     => $url_left ? Url::to([
								'/site/sign-up',
								'key' => User::POSITION_LEFT,
							]) : '#',
						],
						[
							'name'     => $lv1->username,
							'active'   => $lv1->status,
							'children' =>$child,
							'href'     => Url::to([
								'/user/network',
								'key' => $lv1->password,
							]),
						],
					];
				}
			}
		} else {
			$array_children = [
				[
					'name'     => 'New user',
					'active'   => User::STATUS_REGISTER,
					'children' => [],
					'href'     => $url_left ? Url::to([
						'/site/sign-up',
						'key' => User::POSITION_LEFT,
					]) : '#',
				],
				[
					'name'     => 'New user',
					'active'   => User::STATUS_REGISTER,
					'children' => [],
					'href'     => $url_right ? Url::to([
						'/site/sign-up',
						'key' => User::POSITION_RIGHT,
					]) : '#',
				],
			];
		}
		//		$user_model = new SignUpForm();
		//		$new_user   = [];
		//		if($id == $user_model->getLastLeft($this->user)->id) {
		//			$new_user = [
		//				[
		//					'name'     => 'New user',
		//					'active'   => User::STATUS_REGISTER,
		//					'children' => null,
		//					'href'     => Url::to([
		//						'/site/sign-up',
		//						'key' => User::POSITION_LEFT,
		//					]),
		//				],
		//			];
		//		}
		//		if($id == $user_model->getLastRight($this->user)->id) {
		//			$new_user = [
		//				[
		//					'name'     => 'New user',
		//					'active'   => User::STATUS_REGISTER,
		//					'children' => null,
		//					'href'     => Url::to([
		//						'/site/sign-up',
		//						'key' => User::POSITION_RIGHT,
		//					]),
		//				],
		//			];
		//		}
		//		$array_children=ArrayHelper::merge($array_children,)
		return $array_children;
	}

	/**
	 * Hàm generate cây nhị phân
	 */
	function printTree($tree) {
		if(!is_null($tree) && count($tree) > 0) {
			echo '<ul>';
			foreach($tree as $node) {
				echo '<li>';
				if($node['active'] == User::STATUS_ACTIVATED) {
					echo '<a href=' . $node['href'] . '><img src=' . Url::base() . '/tree/image/network_2.png' . ' alt="" style="display: block;margin: 0 auto;width: 70px;height: 67px" class="node-image"></a>';
				} elseif($node['active'] == User::STATUS_REGISTER) {
					echo '<a href=' . $node['href'] . '><img src=' . Url::base() . '/tree/image/sign-up.png' . ' alt="" style="display: block;margin: 0 auto; opacity:0.5;width: 70px;height: 67px" class="node-image"></a>';
				} else {
					echo '<a href=' . $node['href'] . '><img src=' . Url::base() . '/tree/image/network.png' . ' alt="" style="display: block;margin: 0 auto;width: 70px;height: 67px" class="node-image"></a>';
				}
				echo $node['name'];
				$this->printTree($node['children']);
				echo '</li>';
			}
			echo '</ul>';
		}
	}

	/**
	 * Hàm trả về mảng tuyến dưới trực tiếp cho cây matching
	 */
	public function getArrayDownline($id) {
		$array_children = [];
		$lv1s           = User::find()->where([
			'sponsor_id' => $id,
		])->all();
		foreach($lv1s as $lv1) {
			$array_children[] = [
				'name'     => $lv1->username,
				'active'   => $lv1->status,
				'children' => $this->getArrayDownline($lv1->id),
			];
		}
		return empty($array_children) ? null : $array_children;
	}

	/**
	 * Hàm generate cây trực hệ
	 */
	function printMatching($tree) {
		if(!is_null($tree) && count($tree) > 0) {
			echo '<ul>';
			foreach($tree as $node) {
				echo '<li>';
				if($node['active'] == User::STATUS_ACTIVATED) {
					echo '<img src=' . Url::base() . '/tree/image/matching_active.png' . ' alt="" style="display: block;margin: 0 auto;">';
				} else {
					echo '<img src=' . Url::base() . '/tree/image/matching_inactive.png' . ' alt="" style="display: block;margin: 0 auto;">';
				}
				echo $node['name'];
				$this->printMatching($node['children']);
				echo '</li>';
			}
			echo '</ul>';
		}
	}

	/**
	 * Trả về array parent
	 */
	public function getTotalP($id) {
		$user         = User::findOne($id);
		$array_parent = [];
		if($user) {
			if($user->parent_id != null) {
				$array_parent = [$user->parent_id];
			}
		}
		return $array_parent;
	}

	public function getTotalParent($child_id, $array_parent = []) {
		$tree = $this->getTotalP($child_id);
		if(count($tree) > 0 && is_array($tree)) {
			$array_parent = ArrayHelper::merge($array_parent, $tree);
		}
		$child = User::findOne($child_id);
		if($child) {
			$array_parent = $this->getTotalParent($child->parent_id, $array_parent);
		}
		return $array_parent;
	}

	/**
	 * Hàm trả về team bonus(10% nhánh yếu)
	 */
	public function getTeamBonus($id) {
		$user          = User::findOne($id);
		$current_bonus = $user->currentBonus;
		if($current_bonus->current_bv_left >= $current_bonus->current_bv_right) {
			$team_bonus = $current_bonus->current_bv_right * 0.1;
		} else {
			$team_bonus = $current_bonus->current_bv_left * 0.1;
		}
		return $team_bonus;
	}

	/**
	 * Trả về nhánh yếu hiện tại
	 */
	public function getWeakBranch($id) {
		$user          = User::findOne($id);
		$current_bonus = $user->currentBonus;
		if($current_bonus->current_bv_left >= $current_bonus->current_bv_right) {
			$branch = User::POSITION_RIGHT;
		} else {
			$branch = User::POSITION_LEFT;
		}
		return $branch;
	}

	/**
	 * Trả về nhánh yếu tháng hiện tại
	 */
	public function getWeakBranchMonth($id) {
		$user          = User::findOne($id);
		$current_level = $user->currentLevel;
		if($current_level->current_bv_left >= $current_level->current_bv_right) {
			$branch = User::POSITION_RIGHT;
		} else {
			$branch = User::POSITION_LEFT;
		}
		return $branch;
	}

	/**
	 * Downline
	 */
	public function getTotalD($id) {
		/**@var self[] $lv1s */
		$array_children = [];
		$lv1s           = User::find()->where([
			'sponsor_id' => $id,
		])->all();
		foreach($lv1s as $lv1) {
			$array_children[] = $lv1->id;
		}
		return $array_children;
	}

	/**
	 *Upline
	 */
	public function getTotalU($id) {
		$user          = User::findOne($id);
		$array_sponsor = [];
		if($user) {
			if($user->sponsor_id != null) {
				$array_sponsor = [$user->sponsor_id];
			}
		}
		return $array_sponsor;
	}

	/**
	 * @param       $sponsor_id
	 * @param array $array_children
	 *
	 * @return array
	 */
	public function getTotalDownline($sponsor_id, $array_children = []) {
		$tree = $this->getTotalD($sponsor_id);
		if(count($tree) > 0 && is_array($tree)) {
			$array_children = ArrayHelper::merge($array_children, $tree);
		}
		foreach($tree as $item => $id) {
			$array_children = $this->getTotalDownline($id, $array_children);
		}
		return $array_children;
	}

	/**
	 * return total upline
	 */
	public function getTotalUpline($child_id, $array_sponsor = []) {
		$tree = $this->getTotalU($child_id);
		if(count($tree) > 0 && is_array($tree)) {
			$array_sponsor = ArrayHelper::merge($array_sponsor, $tree);
		}
		$child = User::findOne($child_id);
		if($child) {
			$array_sponsor = $this->getTotalUpline($child->sponsor_id, $array_sponsor);
		}
		return $array_sponsor;
	}

	public static function getNumber($number = null) {
		if($number == null) {
			$number = 0;
		}
		return $number;
	}

	public static function getCash($money) {
		return 0.6 * $money;
	}

	public static function getTradding($money) {
		return 0.4 * $money;
	}
}
