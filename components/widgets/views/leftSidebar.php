<?php
use yii\helpers\Url;
use yii\widgets\Menu;

?>

<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
	<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
	<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
	<div class="page-sidebar navbar-collapse collapse">
		<!-- BEGIN SIDEBAR MENU -->
		<!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
		<!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
		<!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
		<!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<?php
		echo Menu::widget([
			'items'           => [
				[
					'options'  => ['class' => 'sidebar-toggler-wrapper'],
					'template' => '<div class="sidebar-toggler">
				</div>',
				],
				[
					'options'  => ['class' => 'sidebar-toggler-wrapper'],
					'template' => '<form class="sidebar-search " action="extra_search.html" method="POST">
					<a href="javascript:;" class="remove">
						<i class="icon-close"></i>
					</a>
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Search...">
						<span class="input-group-btn">
							<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
							</span>
					</div>
				</form>',
				],
				[
					'options'  => ['class' => ''],
					'url'      => ['site/index'],
					'label'    => '<i class="icon-home"></i>
					<span class="title">Dashboard</span>',
					'template' => '<a href="{url}">{label}</a>',
				],
				[
					'options'  => ['class' => ''],
					'url'      => 'javascript:;',
					'label'    => '<i class="icon-docs"></i>
					<span class="title">Info/News</span>
					<span class="arrow"></span>',
					'items'    => [
						[
							'label' => 'About Us',
							'url'   => ['site/about'],
						],
					],
					'template' => '<a href="{url}">{label}</a>',
				],
				[
					'options'  => ['class' => ''],
					'url'      => ['#'],
					'label'    => '<i class="icon-docs"></i>
					<span class="title">Whitepaper</span>',
					'template' => '<a href="{url}">{label}</a>',
				],
				[
					'options'  => ['class' => ''],
					'url'      => 'javascript:;',
					'label'    => '<i class="icon-user"></i>
					<span class="title">My profile</span>
					<span class="arrow"></span>',
					'items'    => [
						[
							'label' => 'My data',
							'url'   => ['site/profile'],
						],
						[
							'label' => '<i class="icon-key"></i>Change password',
							'url'   => ['user/change-password'],
						],
						[
							'label' => 'Change security password',
							'url'   => ['user/change-security'],
						],
						[
							'label' => '<i class="icon-tag"></i>KYC',
							'url'   => ['site/kyc'],
						],
						//						[
						//							'label' => '<i class="icon-handbag"></i>My order',
						//							'url'   => ['#'],
						//						],
						//						[
						//							'label' => '<i class="icon-pencil"></i>My invoice',
						//							'url'   => ['#'],
						//						],
					],
					'template' => '<a href="{url}">{label}</a>',
				],
				[
					'options'  => ['class' => ''],
					'url'      => 'javascript:;',
					'label'    => '	<i class="icon-briefcase"></i>
					<span class="title">My Network</span>
					<span class="arrow"></span>',
					'items'    => [
						[
							'label' => '<i class="fa fa-cubes" aria-hidden="true"></i>
							Network tree',
							'url'   => ['/user/network'],
						],
//						[
//							'label' => '<i class="fa fa-cubes" aria-hidden="true"></i>
//							Matching tree',
//							'url'   => ['/user/matching'],
//						],
					],
					'template' => '<a href="{url}">{label}</a>',
				],
//				[
//					'options'  => ['class' => ''],
//					'url'      => 'javascript:;',
//					'label'    => '	<i class="icon-wallet"></i>
//					<span class="title">My Wallet</span>
//					<span class="arrow"></span>',
//					'items'    => [
//						[
//							'label' => '<i class="icon-folder"></i>
//							Cash account',
//							'url'   => ['cash-transaction/index'],
//						],
//						[
//							'label' => '<i class="icon-folder"></i>
//							Trading account',
//							'url'   => ['trading-transaction/index'],
//						],
//						[
//							'label' => '<i class="icon-folder"></i>
//							Token account',
//							'url'   => ['token-transaction/index'],
//						],
//						[
//							'label' => '<i class="fa fa-cubes" aria-hidden="true"></i>
//							Marketcoins account',
//							'url'   => ['coin-transaction/index'],
//						],
//					],
//					'template' => '<a href="{url}">{label}</a>',
//				],
//				[
//					'options'  => ['class' => ''],
//					'url'      => ['current-bonus/index'],
//					'label'    => '	<i class="icon-present"></i>
//					<span class="title">Bonus</span>',
//					'template' => '<a href="{url}">{label}</a>',
//				],
//				[
//					'options'  => ['class' => ''],
//					'url'      => 'javascript:;',
//					'label'    => '	<i class="icon-basket"></i>
//					<span class="title">Shop</span>
//					<span class="arrow"></span>',
//					'items'    => [
//						[
//							'label' => '<i class="icon-folder"></i>
//							My package',
//							'url'   => ['package-transaction/index'],
//						],
//						[
//							'label' => '<i class="fa fa-money" aria-hidden="true"></i>
//							Buying package',
//							'url'   => ['package/index'],
//						],
//					],
//					'template' => '<a href="{url}">{label}</a>',
//				],
				[
					'options'  => ['class' => ''],
					'url'      => 'javascript:;',
					'label'    => '	<i class="icon-basket"></i>
					<span class="title">Exchange</span>
					<span class="arrow"></span>',
					'items'    => [
						[
							'label' => '<i class="fa fa-money" aria-hidden="true"></i>
							Coins exchange',
							'url'   => ['#'],
						],
					],
					'template' => '<a href="{url}">{label}</a>',
				],
				[
					'options'  => ['class' => ''],
					'url'      => [
						'site/support',
					],
					'label'    => '<i class="icon-envelope"></i>
					<span class="title">Support</span>',
					'template' => '<a href="{url}" data-method="POST">{label}</a>',
				],
				[
					'options'  => ['class' => ''],
					'url'      => [
						'site/logout',
					],
					'label'    => '<i class="icon-delete"></i>
					<span class="title">Logout</span>',
					'template' => '<a href="{url}" data-method="POST">{label}</a>',
				],
			],
			'encodeLabels'    => false,
			'submenuTemplate' => "\n<ul class='sub-menu'>\n{items}\n</ul>\n",
			'options'         => array(
				'class'              => 'page-sidebar-menu',
				'data-keep-expanded' => "false",
				'data-auto-scroll'   => 'true',
				'data-slide-speed'   => "200",
			),
			'linkTemplate'    => '<a href="{url}"><span class="title">{label}</span></a><span class="arrow "></span>',
			'activeCssClass'  => 'active open',
			'activateParents' => true,
		])
		?>
		<!-- END SIDEBAR MENU -->
	</div>
</div>
<!-- END SIDEBAR -->

