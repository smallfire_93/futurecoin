<?php
namespace app\controllers;

use app\components\Model;
use app\models\form\SendCashForm;
use app\models\form\WithdrawForm;
use Yii;
use app\models\CashTransaction;
use app\models\search\CashTransactionSearch;
use app\components\Controller;
use yii\bootstrap\ActiveForm;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * CashTransactionController implements the CRUD actions for CashTransaction model.
 */
class CashTransactionController extends Controller {

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all CashTransaction models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new CashTransactionSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single CashTransaction model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	public function actionSendDownline() {
		$model    = new SendCashForm();
		$data     = new Model();
		$children = $data->getTotalDownline($this->user->id);
		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}
		if($model->load(Yii::$app->request->post())) {
			if($model->sendDown()) {
				return $this->redirect(['index']);
			}
		}
		return $this->render('send-downline', [
			'model'    => $model,
			'children' => $children,
		]);
	}

	public function actionSendUpline() {
		$model    = new SendCashForm();
		$data     = new Model();
		$children = $data->getTotalUpline($this->user->id);
		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}
		if($model->load(Yii::$app->request->post())) {
			if($model->sendUp()) {
				return $this->redirect(['index']);
			}
		}
		return $this->render('send-upline', [
			'model'    => $model,
			'children' => $children,
		]);
	}

	public function actionWithdraw($type = null) {
		if($type != 1) {
			return $this->render('withdraw-button');
		}
		$model = new WithdrawForm;
		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}
		if($model->load(Yii::$app->request->post()) && $model->validate()) {
			$transaction          = new CashTransaction();
			$transaction->user_id = $this->user->id;
			$transaction->type    = $transaction::TYPE_WIT;
			$transaction->money   = $model->money;
			$transaction->status  = $transaction::STATUS_PENDING;
			$transaction->date    = date('Y-m-d H:i:s');
			if($transaction->save()) {
				$this->user->accounts->updateAttributes(['cash_account' => $this->user->accounts->cash_account - $model->money]);
			};
			$this->redirect(['index']);
		}
		return $this->render('withdraw', ['model' => $model]);
	}

	/**
	 * Deletes an existing CashTransaction model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	/**
	 * Finds the CashTransaction model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return CashTransaction the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if(($model = CashTransaction::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
