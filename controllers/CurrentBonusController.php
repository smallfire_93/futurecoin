<?php
namespace app\controllers;

use app\components\Model;
use app\models\LevelCurrent;
use app\models\search\BonusTransactionSearch;
use app\models\search\PackageTransactionSearch;
use Yii;
use app\models\CurrentBonus;
use app\models\search\CurrentBonusSearch;
use app\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CurrentBonusController implements the CRUD actions for CurrentBonus model.
 */
class CurrentBonusController extends Controller {

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all CurrentBonus models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel   = new PackageTransactionSearch();
		$dataProvider  = $searchModel->transaction(Yii::$app->request->queryParams);
		$current_level = LevelCurrent::find()->where(['user_id' => $this->user->id])->one();
		$current_level = $current_level ? ($current_level->level ? $current_level->level->name : 'None') : 'None';
		if(!$this->user->currentBonus) {
			$current_bonus          = new CurrentBonus();
			$current_bonus->user_id = $this->user->id;
			if($current_bonus->save()) {
				$direct         = Model::getNumber($current_bonus->direct_bonus);
				$branch_bonus   = Model::getNumber($current_bonus->weak_branch_bonus);
				$matching_bonus = Model::getNumber($current_bonus->matching_bonus);
				$total_bonus    = Model::getNumber($current_bonus->total_bonus);
				$bv_left        = Model::getNumber($current_bonus->current_bv_left);
				$bv_right       = Model::getNumber($current_bonus->current_bv_right);
			};
		} else {
			$direct         = Model::getNumber($this->user->currentBonus->direct_bonus);
			$branch_bonus   = Model::getNumber($this->user->currentBonus->weak_branch_bonus);
			$matching_bonus = Model::getNumber($this->user->currentBonus->matching_bonus);
			$total_bonus    = Model::getNumber($this->user->currentBonus->total_bonus);
			$bv_left        = Model::getNumber($this->user->currentBonus->current_bv_left);
			$bv_right       = Model::getNumber($this->user->currentBonus->current_bv_right);
		}
		$searchData = new BonusTransactionSearch();
		$bonusData  = $searchData->search(Yii::$app->request->queryParams);
		return $this->render('index', [
			'searchModel'    => $searchModel,
			'dataProvider'   => $dataProvider,
			'direct'         => $direct,
			'branch_bonus'   => $branch_bonus,
			'matching_bonus' => $matching_bonus,
			'total_bonus'    => $total_bonus,
			'bv_left'        => $bv_left,
			'bv_right'       => $bv_right,
			'current_level'  => $current_level,
			'bonusData'      => $bonusData,
		]);
	}

	public function actionStatistic() {
		$searchModel  = new CurrentBonusSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->render('statistic', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single CurrentBonus model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new CurrentBonus model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new CurrentBonus();
		if($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect([
				'view',
				'id' => $model->id,
			]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing CurrentBonus model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel($id);
		if($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect([
				'view',
				'id' => $model->id,
			]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing CurrentBonus model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	/**
	 * Finds the CurrentBonus model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return CurrentBonus the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if(($model = CurrentBonus::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
