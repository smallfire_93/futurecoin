<?php
namespace app\controllers;

use app\models\CashTransaction;
use app\models\Cryptobox;
use app\models\CryptoPayments;
use app\models\form\CheckSecurityPasswordForm;
use app\models\PackageTransaction;
use Yii;
use app\models\Package;
use app\models\search\PackageSearch;
use app\components\Controller;
use yii\bootstrap\ActiveForm;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * PackageController implements the CRUD actions for Package model.
 */
class PackageController extends Controller {

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all Package models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new PackageSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$packages     = Package::find()->all();
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'packages'     => $packages,
		]);
	}

	/**
	 * Displays a single Package model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Package model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Package();
		if($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect([
				'view',
				'id' => $model->id,
			]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	public function actionProductPurchase($id) {
		$package = $this->findModel($id);
		$model   = new CheckSecurityPasswordForm();
		if($package) {
			// ...
			// Also you need to use IPN function cryptobox_new_payment($paymentID = 0, $payment_details = array(), $box_status = "")
			// for send confirmation email, update database, update user membership, etc.
			// You need to modify file - cryptobox.newpayment.php, read more - https://gourl.io/api-php.html#ipn
			// ...
			return $this->render('product-purchase', [
				'package' => $package,
				'model'   => $model,
				//				'box'            => $box,
				//				'message'        => $message,
				//				'languages_list' => $languages_list,
				//				'package_name'   => $package_name,
			]);
		} else {
			return $this->goHome();
		}
	}

	/**
	 *Hàm thanh toán bằng tài khoản tiền mặt
	 */
	public function actionCashPackage($id) {
		$model   = new CheckSecurityPasswordForm();
		$package = $this->findModel($id);
		//TODO validate ajax
		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}
		if($model->load(Yii::$app->request->post()) && $model->validate()) {
			if($this->user->accounts->package_id == null) {
				$model->user->accounts->updateAttributes(['cash_account' => $model->user->accounts->cash_account - $package->money - $package::FEE]);
			} else {
				$model->user->accounts->updateAttributes(['cash_account' => $model->user->accounts->cash_account - $package->money]);
			}
			$package->updateDirectBonus();
			$package->updateTeamBonus();
			$transaction                = new CashTransaction();
			$transaction->user_id       = $this->user->id;
			$transaction->type          = $transaction::TYPE_BUY;
			$transaction->money         = $package->money;
			$transaction->status        = $transaction::STATUS_SUCCESS;
			$transaction->date          = date('Y-m-d H:i:s');
			$package_tran               = new PackageTransaction();
			$package_tran->package_id   = $package->id;
			$package_tran->type         = $package_tran::TYPE_CASH;
			$package_tran->user_id      = $this->user->id;
			$package_tran->created_date = date('Y-m-d H:i:s');
			if($package_tran->save()) {
			};
			if($transaction->save() && $package->save()) {
				if($this->user->accounts->package_id == null) {
					$fee_tran          = new CashTransaction();
					$fee_tran->user_id = $this->user->id;
					$fee_tran->money   = $fee_tran::FEE;
					$fee_tran->status  = $fee_tran::STATUS_SUCCESS;
					$fee_tran->type    = $fee_tran::TYPE_FEE;
					$fee_tran->date    = date('Y-m-d H:i:s');
					$fee_tran->save();
				}
				if($package->activeUser($package->id)) {
					return $this->redirect(['/package-transaction']);
				} else {
					$model->user->accounts->updateAttributes(['cash_account' => $model->user->accounts->cash_account + $package->money]);
					echo "Get some error";
					die;
				}
			} else {
				$transaction->delete();
				$package_tran->delete();
				echo 'Transaction error';
				die;
			}
		}
		return $this->renderAjax('cash-package', [
			'model'   => $model,
			'package' => $package,
			'id'      => $package->id,
		]);
	}

	//	public function actionCreateTable() {
	//		$connection = Yii::$app->db;
	//		$users      = $connection->createCommand('CREATE TABLE `crypto_payments` (
	//  `paymentID` int(11) unsigned NOT NULL AUTO_INCREMENT,
	//  `boxID` int(11) unsigned NOT NULL DEFAULT \'0\',
	//  `boxType` enum(\'paymentbox\',\'captchabox\') NOT NULL,
	//  `orderID` varchar(50) NOT NULL DEFAULT \'\',
	//  `userID` varchar(50) NOT NULL DEFAULT \'\',
	//  `countryID` varchar(3) NOT NULL DEFAULT \'\',
	//  `coinLabel` varchar(6) NOT NULL DEFAULT \'\',
	//  `amount` double(20,8) NOT NULL DEFAULT \'0.00000000\',
	//  `amountUSD` double(20,8) NOT NULL DEFAULT \'0.00000000\',
	//  `unrecognised` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
	//  `addr` varchar(34) NOT NULL DEFAULT \'\',
	//  `txID` char(64) NOT NULL DEFAULT \'\',
	//  `txDate` datetime DEFAULT NULL,
	//  `txConfirmed` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
	//  `txCheckDate` datetime DEFAULT NULL,
	//  `processed` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
	//  `processedDate` datetime DEFAULT NULL,
	//  `recordCreated` datetime DEFAULT NULL,
	//  PRIMARY KEY (`paymentID`),
	//  KEY `boxID` (`boxID`),
	//  KEY `boxType` (`boxType`),
	//  KEY `userID` (`userID`),
	//  KEY `countryID` (`countryID`),
	//  KEY `orderID` (`orderID`),
	//  KEY `amount` (`amount`),
	//  KEY `amountUSD` (`amountUSD`),
	//  KEY `coinLabel` (`coinLabel`),
	//  KEY `unrecognised` (`unrecognised`),
	//  KEY `addr` (`addr`),
	//  KEY `txID` (`txID`),
	//  KEY `txDate` (`txDate`),
	//  KEY `txConfirmed` (`txConfirmed`),
	//  KEY `txCheckDate` (`txCheckDate`),
	//  KEY `processed` (`processed`),
	//  KEY `processedDate` (`processedDate`),
	//  KEY `recordCreated` (`recordCreated`),
	//  KEY `key1` (`boxID`,`orderID`),
	//  KEY `key2` (`boxID`,`orderID`,`userID`),
	//  UNIQUE KEY `key3` (`boxID`, `orderID`, `userID`, `txID`, `amount`, `addr`)
	//) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;')->queryAll();
	//		return true;
	//	}
	public function actionCallback() {
		$this->layout = false;
		return $this->render('callback');
	}

	/**
	 * Hàm thanh toán bit-coin
	 */
	public function actionBitPackage($id, $order_id = null) {
		$package = Package::findOne($id);
		if($package) {
			if($order_id == null) {
				$order               = new PackageTransaction();
				$order->created_date = date('Y-m-d H:i:s');
				$order->package_id   = $package->id;
				$order->type         = $order::TYPE_BITCOIN;
				$order->user_id      = $this->user->id;
				$order->status       = $order::STATUS_PENDING;
				$order->save();
			} else {
				$order = PackageTransaction::findOne($order_id);
			}
			$package_name = $package->name;
			$userID       = $this->user->id;                // place your registered userID or md5(userID) here (user1, user7, uo43DC, etc).
			// you don't need to use userID for unregistered website visitors
			// if userID is empty, system will autogenerate userID and save in cookies
			$userFormat = "COOKIE";            // save userID in cookies (or you can use IPADDRESS, SESSION)
			$orderID    = $order->id;    // invoice number - 000383
			//			$amountUSD    = $package->getAmount();                // invoice amount - 2.21 USD
			$amountUSD    = $package->getAmount();
			$period       = "NOEXPIRY";        // one time payment, not expiry
			$def_language = "en";                // default Payment Box Language
			$public_key   = "10628AAWVO3qBitcoin77BTCPUB6QOoxIvN3ZJEy7fwGIVFUCo";  //mr van from gourl.io
			$private_key  = "10628AAWVO3qBitcoin77BTCPRV5n2mpN3jlMQMF6B4DN6X3KH";// mr van from gourl.io
			if($package->money <= 125) {
				$public_key  = "12946AAgkwF9Bitcoin77BTCPUBzD1fUcIQcpykF3Xd9FlnMGu";  //mr giang from gourl.io
				$private_key = "12946AAgkwF9Bitcoin77BTCPRV7rDw3duxPKq0UIdzxAyySPM";// mr giang from gourl.io
			}
			//$public_key  = "10032AAhehmySpeedcoin77SPDPUBfQeYlbsHfHDTa38BYKyub"; // from gourl.io
			//			$private_key = "10032AAhehmySpeedcoin77SPDPRVCGcqwD5Cq5qmHoV98OVG6";// from gourl.io
			// IMPORTANT: Please read description of options here - https://gourl.io/api-php.html#options
			// *** For convert Euro/GBP/etc. to USD/Bitcoin, use function convert_currency_live() with Google Finance
			// *** examples: convert_currency_live("EUR", "BTC", 22.37) - convert 22.37 Euro to Bitcoin
			// *** convert_currency_live("EUR", "USD", 22.37) - convert 22.37 Euro to USD
			/********************************/
			/** PAYMENT BOX **/
			$options = array(
				"public_key"  => $public_key,
				// your public key from gourl.io
				"private_key" => $private_key,
				// your private key from gourl.io
				"webdev_key"  => "DEV907GCDAE3CD99A288AAG12474059",
				// optional, gourl affiliate key
				"orderID"     => $orderID,
				// order id or product name
				"userID"      => $userID,
				// unique identifier for every user
				"userFormat"  => $userFormat,
				// save userID in COOKIE, IPADDRESS or SESSION
				"amount"      => 0,
				// product price in coins OR in USD below
				"amountUSD"   => $amountUSD,
				// we use product price in USD
				"period"      => $period,
				// payment valid period
				"language"    => $def_language
				// text on EN - english, FR - french, etc
			);
			// Initialise Payment Class
			$box = new Cryptobox ($options);
			//			$box->cryptobox_reset();
			if(isset($_POST['check'])) {
				if($box->check_payment_live()) {
				};
				//				$this->layout = 'bitcoin';
				// coin name
				$coinName = $box->coin_name();
				// Successful Cryptocoin Payment received
				if($box->check_ajax_payment()) {
					$message = 1;
					if($order->status !== $order::STATUS_SUCCESS) {
						$order->updateAttributes(['status' => $order::STATUS_SUCCESS]);
						$package->updateDirectBonus();
						$package->updateTeamBonus();
						$package->activeUser($package->id);
					}
					return json_encode($message);
				} else {
					$message = 2;
					//					return json_encode($message);
					return json_encode($message);
				}
			}
			// Optional - Language selection list for payment box (html code)
			$languages_list = $box->display_language_box($def_language);
			return $this->render('bit-package', [
				'box'            => $box,
				//				'message'        => $message,
				'languages_list' => $languages_list,
				'package_name'   => $package_name,
				'package'        => $package,
				'order_id'       => $order->id,
			]);
		} else {
			return $this->goHome();
		}
	}

	/**
	 * Updates an existing Package model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	/**
	 * Deletes an existing Package model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	/**
	 * Finds the Package model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Package the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if(($model = Package::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
