<?php

namespace app\controllers;

use app\models\Package;
use Yii;
use app\models\PackageTransaction;
use app\models\search\PackageTransactionSearch;
use app\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PackageTransactionController implements the CRUD actions for PackageTransaction model.
 */
class PackageTransactionController extends Controller {

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all PackageTransaction models.
	 * @return mixed
	 */
	public function actionIndex() {
		$package      = Package::find()->joinWith('packageTransaction')->where(['user_id' => $this->user->id])->andWhere(['package_transaction.status' => PackageTransaction::STATUS_SUCCESS])->orderBy(['package.money' => SORT_DESC])->one();
		$searchModel  = new PackageTransactionSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		if($package) {
			$active_date    = strtotime($this->user->active_at);
			$available_date = strtotime('+54 days', $active_date);
			$available_date = date('Y-m-d H:i:s', $available_date);
		} else {
			$available_date = null;
		}
		return $this->render('index', [
			'searchModel'    => $searchModel,
			'dataProvider'   => $dataProvider,
			'package'        => $package,
			'available_date' => $available_date,
		]);
	}

	/**
	 * Displays a single PackageTransaction model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new PackageTransaction model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new PackageTransaction();
		if($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect([
				'view',
				'id' => $model->id,
			]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing PackageTransaction model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel($id);
		if($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect([
				'view',
				'id' => $model->id,
			]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing PackageTransaction model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	/**
	 * Finds the PackageTransaction model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return PackageTransaction the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if(($model = PackageTransaction::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
