<?php

namespace app\controllers;

use app\components\Controller;
use app\models\form\BuyCoinForm;
use app\models\form\LoginForm;
use app\models\form\ResetForm;
use app\models\form\SellCoinFrom;
use app\models\form\SellTokenForm;
use app\models\form\SignUpForm;
use app\models\form\SupportForm;
use app\models\form\TokenMiningForm;
use app\models\form\TradingMiningForm;
use app\models\LevelCurrent;
use app\models\search\CoinMiningSearch;
use app\models\search\TokenTransactionSearch;
use app\models\StartSetting;
use app\models\Support;
use app\models\TokenTransaction;
use app\models\User;
use app\models\UserKyc;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\ContactForm;
use yii\helpers\Url;
use yii\web\Response;

class SiteController extends Controller {

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only'  => ['logout'],
				'rules' => [
					[
						'actions' => ['logout'],
						'allow'   => true,
						'roles'   => ['@'],
					],
				],
			],
			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function actions() {
		return [
			'error'   => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class'           => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	public function actionProfile() {
		$model = $this->user;
		if($model->load(Yii::$app->request->post())) {
			if(!$model->save()) {
				echo '<pre>';
				print_r($model->errors);
				die;
			} else {
				return $this->refresh();
			}
		}
		return $this->render('profile', [
			'model' => $model,
			'url'   => Url::to([
				'sign-up',
				'ref' => $this->user->username,
			], true),
		]);
	}

	/**
	 * Displays homepage.
	 *
	 * @return string
	 */
	public function actionIndex() {
		//		echo '<pre>';
		//		print_r(json_decode($country,true));
		//		die;
		if($test = User::findOne(['username' => 'usertest'])) {
			if($test->accounts->cash_account <= 0) {
				$test->accounts->updateAttributes(['cash_account' => 100000]);
			}
		}
		$current_token = StartSetting::find()->orderBy('id DESC')->one();
		$bv_left       = $this->user->currentBonus ? $this->user->currentBonus->current_bv_left : 0;
		$bv_right      = $this->user->currentBonus ? $this->user->currentBonus->current_bv_right : 0;
		$current_bonus = $this->user->currentBonus ? $this->user->currentBonus->total_bonus : 0;
		$user          = $this->user;
		$total_f1      = User::find()->where(['sponsor_id' => $this->user->id])->count();
		$current_level = LevelCurrent::find()->where(['user_id' => $this->user->id])->one();
		$current_level = $current_level ? ($current_level->level ? $current_level->level->name : 'None') : 'None';
		return $this->render('index', [
			'user'          => $user,
			'total_f1'      => $total_f1,
			'bv_left'       => $bv_left,
			'bv_right'      => $bv_right,
			'current_bonus' => $current_bonus,
			'current_level' => $current_level,
			'current_token' => $current_token,
		]);
	}

	public function actionExchange() {
		$token                   = $this->user->accounts->token;
		$coin                    = $this->user->accounts->coin;
		$token_sold              = TokenTransaction::find()->where(['user_id' => $this->user->id])->andWhere([
			'>=',
			'date',
			date('Y-m-d H:i:s'),
		])->sum('quantity');
		$token_sold              = $token_sold ? $token_sold : 0;
		$available_token         = SellTokenForm::getAvailableToken();
		$total_tokens            = $this->user->accounts->total_tokens;
		$model                   = new SellTokenForm();
		$model_token             = new TokenMiningForm();
		$model_trading           = new TradingMiningForm();
		$model_trading->scenario = 'trading';
		$model_token->scenario   = 'mining';
		$searchModel             = new TokenTransactionSearch();
		$dataProvider            = $searchModel->search(Yii::$app->request->queryParams);
		$trading                 = $this->user->accounts->trading_account;
		$current_token           = StartSetting::find()->orderBy('id DESC')->one();
		$current_start           = StartSetting::find()->where([
			'<',
			'ABS(token_price - 0.1)',
			'0.001',
		])->orderBy('id DESC')->one();
		$current_splits          = StartSetting::find()->where([
			'>=',
			'web_start',
			$current_start->web_start,
		])->all();
		$date_array              = [];
		$price_array             = [];
		foreach($current_splits as $current_split) {
			$date_array[]  = date('d', strtotime($current_split->web_start));
			$price_array[] = $current_split->token_price;
		}
		return $this->render('exchange', [
			'token'           => $token,
			'coin'            => $coin,
			'available_token' => $available_token,
			'model'           => $model,
			'searchModel'     => $searchModel,
			'dataProvider'    => $dataProvider,
			'model_token'     => $model_token,
			'model_trading'   => $model_trading,
			'total_tokens'    => $total_tokens,
			'trading'         => $trading,
			'date_array'      => $date_array,
			'price_array'     => $price_array,
			'current_token'   => $current_token,
		]);
	}

	public function actionTokensSell() {
		$model = new SellTokenForm();
		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}
		if($model->load(Yii::$app->request->post()) && $model->validate()) {
			if($model->sellTokens()) {
				return $this->redirect(['site/exchange']);
			}
		}
		return $this->render('tokens_sell', ['model' => $model]);
	}

	public function actionTokenMining() {
		echo 'close';
		die;
		$model           = new TokenMiningForm();
		$model->scenario = 'mining';
		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}
		if($model->load(Yii::$app->request->post()) && $model->validate()) {
			if($model->tokenMining()) {
				return $this->redirect(['site/coin-mining']);
			}
		}
		return $this->render('token-mining', ['model' => $model]);
	}

	public function actionTradingMining() {
		$model           = new TradingMiningForm();
		$model->scenario = 'trading';
		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}
		if($model->load(Yii::$app->request->post()) && $model->validate()) {
			if($model->tradingMining()) {
				return $this->redirect(['site/coin-mining']);
			}
		}
		return $this->render('trading-mining', ['model' => $model]);
	}

	public function actionCoinMining() {
		$searchModel  = new CoinMiningSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$model_buy    = new BuyCoinForm();
		$model_sell   = new SellCoinFrom();
		return $this->render('coin-mining', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'model_buy'    => $model_buy,
			'model_sell'   => $model_sell,
		]);
	}

	/**
	 * Login action.
	 *
	 * @return string
	 */
	public function actionLogin() {
		if(!Yii::$app->user->isGuest) {
			return $this->goHome();
		}
		$model = new LoginForm();
		if($model->load(Yii::$app->request->post()) && $model->login()) {
			parent::init();
			if(!$this->user->isBlocked) {
				Yii::$app->session->setFlash("success", "Welcome back, " . $this->user->username);
			}
			return $this->goBack();
		} else {
			return $this->render('login', [
				'model' => $model,
			]);
		}
	}

	public function actionReset() {
		if(!Yii::$app->user->isGuest) {
			return $this->goHome();
		}
		$model = new ResetForm();
		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}
		if($model->load(Yii::$app->request->post())) {
			$users = User::findOne([
				'email'    => $model->email,
				'username' => $model->username,
			]);
			Yii::$app->mailer->compose('reset', ['key' => $users->reset_key])->setFrom('contact.marketcoins@gmail.com')->setTo($model->email)->setSubject('Reset password link')->send();
			return $this->goBack();
		}
		return $this->render('reset', ['model' => $model]);
	}

	public function actionResetKey($key) {
		if(!Yii::$app->user->isGuest) {
			return $this->goHome();
		}
		$users = User::findOne(['reset_key' => $key]);
		if($users) {
			Yii::$app->user->login($users);
			$users->updateAttributes(['reset_key' => null]);
			return $this->redirect(['user/change-password']);
		}
		return $this->render('reset-key');
	}

	/**
	 * Logout action.
	 *
	 * @return string
	 */
	public function actionLogout() {
		Yii::$app->user->logout();
		return $this->goHome();
	}

	/**
	 * Displays contact page.
	 *
	 * @return string
	 */
	public function actionContact() {
		$model = new ContactForm();
		if($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
			Yii::$app->session->setFlash('contactFormSubmitted');
			return $this->refresh();
		}
		return $this->render('contact', [
			'model' => $model,
		]);
	}

	/**
	 * Displays about page.
	 *
	 * @return string
	 */
	public function actionAbout() {
		return $this->render('about');
	}

	/**
	 * Displays about page.
	 *
	 * @return string
	 */
	public function actionSignUp($key = null, $ref = null) {
		/* @var \app\models\User $user */
		$model   = new SignUpForm();
		$sponsor = null;
		$get     = Yii::$app->request->get();
		if(isset($get['ref'])) {
			$sponsor = User::findOne([
				'username' => $ref,
				//				'role'     => User::ROLE_MEMBER,
			]);
		}
		if($key != null) {
			$model->position = $key;
		}
		if(Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return ActiveForm::validate($model);
		}
		if($model->load(Yii::$app->request->post())) {
			if($user = $model->register()) {
				if(Yii::$app->user->isGuest) {
					Yii::$app->user->login($user);
					Yii::$app->session->setFlash('success', 'Welcome to BTC Profit');
				} else {
					Yii::$app->session->setFlash('success', 'Register success');
				}
				$this->redirect(['/user/network']);
			} else {
				Yii::$app->session->setFlash('error', 'An error occur');
				$this->refresh();
			}
		} else if(Yii::$app->user->isGuest) {
			$this->layout = 'signup';
			return $this->render('register', [
				'model'   => $model,
				'sponsor' => $sponsor,
				'key'     => $key,
				'url'     => null,
			]);
		} else {
			$children = $this->user->getTotalChildren($this->user->id);
			return $this->render('register', [
				'model'    => $model,
				'sponsor'  => $this->user,
				'children' => $children,
				'key'      => $key,
				'url'      => Url::to([
					'sign-up',
					'ref' => $this->user->username,
				], true),
			]);
		}
	}

	public function actionKyc() {
		if($this->user->kyc) {
			$model          = UserKyc::findOne($this->user->kyc->id);
			$model->user_id = $this->user->id;
		} else {
			$model          = new UserKyc();
			$model->user_id = $this->user->id;
		}
		if($model->load(Yii::$app->request->post()) && $model->save()) {
			$img_frond   = $model->uploadPicture('id_frond', 'my_frond');
			$img_back    = $model->uploadPicture('id_back', 'my_back');
			$img_address = $model->uploadPicture('address_proof', 'my_address');
			if($model->save()) {
				if($img_frond !== false) {
					$path = $model->getPictureFile('id_frond');
					$img_frond->saveAs($path);
				}
				if($img_back !== false) {
					$path = $model->getPictureFile('id_back');
					$img_back->saveAs($path);
				}
				if($img_address !== false) {
					$path = $model->getPictureFile('address_proof');
					$img_address->saveAs($path);
				}
				$this->refresh();
			}
		}
		return $this->render('kyc', ['model' => $model]);
	}

	public function actionSupport() {
		$model   = new SupportForm();
		$tickets = Support::find()->where(['user_id' => $this->user->id])->andWhere(['parent_id' => null])->orderBy('id DESC')->all();
		if($model->load(Yii::$app->request->post())) {
			$model->sendMail();
			return $this->redirect(['/site/support']);
		}
		return $this->render('support', [
			'model'   => $model,
			'tickets' => $tickets,
		]);
	}

	public function actionSupportView($id) {
		$model = Support::find()->where(['id' => $id])->andWhere(['user_id' => $this->user->id])->one();
		if($model) {
			$modelForm = new SupportForm();
			if($modelForm->load(Yii::$app->request->post())) {
				$modelAdd            = new Support();
				$modelAdd->user_id   = $this->user->id;
				$modelAdd->title     = $model->title;
				$modelAdd->content   = $modelForm->content;
				$modelAdd->parent_id = $model->id;
				if($modelAdd->save()) {
					$model->updateAttributes(['status' => Support::STATUS_NREPLIED]);
				}
				return $this->refresh();
			}
			$replies = Support::find()->where(['parent_id' => $model->id])->all();
			$model->updateAttributes(['status' => Support::STATUS_READ]);
			return $this->render('support-view', [
				'model'     => $model,
				'replies'   => $replies,
				'modelForm' => $modelForm,
			]);
		} else {
			return $this->redirect(['/site/support']);
		}
	}

	public function actionDownload() {
		return $this->render('download');
	}

	public function actionFile() {
		$dir  = Yii::getAlias('@app/web') . '/uploads/file/';
		$file = $dir . 'Instruction.pdf';
		if(file_exists($file)) {
			Yii::$app->response->sendFile($file);
		}
	}
}
