<?php

namespace app\controllers;

use app\models\form\ChangePasswordForm;
use app\models\form\ChangeSecurityPasswordForm;
use Yii;
use app\models\User;
use app\models\search\UserSearch;
use app\components\Controller;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller {

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all User models.
	 * @return mixed
	 */
	public function actionMatching() {
		$searchModel  = new UserSearch();
		$dataProvider = $searchModel->getDirect(Yii::$app->request->queryParams);
		$model        = new User();
		$children     = $model->getArrayDownline($model->user->id);
		return $this->render('matching', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'model'        => $model,
			'children'     => $children,
		]);
	}

	/**
	 * Displays a single User model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new User model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionNetwork($key = null) {
		$model = new User();
		$right = $model->getPositionRight();
		$left  = $model->getPositionLeft();
		if($key != null) {
			$parent = $model::findOne(['password' => $key]);
			if($parent) {
				$grand_parent = $model::findOne($parent->parent_id);
				if(!$grand_parent) {
					$disabled = true;
				} else {
					$grand_parent = $model::findOne($parent->parent_id)->password;
					$disabled     = false;
				}
				$children[] = [
					'name'     => $parent->username,
					'active'   => $parent->status,
					'children' => $model->getArrayChildren($parent->id),
					'href'     => Url::to([
						'/user/network',
						'key' => $parent->password,
					]),
				];
			} else {
				$grand_parent = null;
				$disabled     = true;
				$children[]   = [
					'name'     => $model->user->username,
					'active'   => $model->user->status,
					'children' => $model->getArrayChildren($model->user->id),
					'href'     => Url::to([
						'/user/network',
						'key' => $model->user->password,
					]),
				];
			}
		} else {
			$grand_parent = null;
			$disabled     = true;
			$children[]   = [
				'name'     => $model->user->username,
				'active'   => $model->user->status,
				'children' => $model->getArrayChildren($model->user->id),
				'href'     => Url::to([
					'/user/network',
					'key' => $model->user->password,
				]),
			];
		}
		//						echo '<pre>';
		//						print_r($children);
		//						die;
		if($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect([
				'view',
				'id' => $model->id,
			]);
		} else {
			return $this->render('network', [
				'model'        => $model,
				'children'     => $children,
				'grand_parent' => $grand_parent,
				'disabled'     => $disabled,
				'right'        => $right,
				'left'         => $left,
			]);
		}
	}

	/**
	 * Updates an existing User model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel($id);
		if($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect([
				'view',
				'id' => $model->id,
			]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing User model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	/**
	 * Finds the User model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return User the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if(($model = User::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	public function actionChangePassword() {
		$model = new ChangePasswordForm();
		if($model->load(Yii::$app->request->post()) && $model->changePass()) {
			return $this->goHome();
		}
		return $this->render('change-password', ['model' => $model]);
	}

	public function actionChangeSecurity() {
		$model = new ChangeSecurityPasswordForm();
		if($model->load(Yii::$app->request->post()) && $model->changePass()) {
			return $this->goHome();
		}
		return $this->render('change-security', ['model' => $model]);
	}
}
