<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\BaseMessage instance of newly created mail message */
?>
	<h2>Thank you to contact to marketcoins.org. We will reply within 5-7 business days.</h2>
<?= Html::a('Go home', Url::home(true)) ?>