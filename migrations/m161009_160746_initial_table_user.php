<?php
use yii\db\Migration;

class m161009_160746_initial_table_user extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%user}}', [
			'id'             => $this->primaryKey(),
			'parent_id'      => $this->integer(),
			'sponsor_id'     => $this->integer(),
			'level'          => $this->smallInteger(1)->notNull()->defaultValue(0),
			'role'           => $this->smallInteger()->notNull()->defaultValue(0),
			'username'       => $this->string(64)->notNull()->unique(),
			'password'       => $this->string(255)->notNull(),
			'password_2'     => $this->string(255)->notNull(),
			'image'          => $this->string(225),
			'full_name'      => $this->string(255)->notNull(),
			'phone'          => $this->string(16)->notNull(),
			'email'          => $this->string(128)->notNull(),
			'status'         => $this->smallInteger(1)->notNull()->defaultValue(0),
			'gender'         => $this->smallInteger(1)->notNull()->defaultValue(0),
			'position'       => $this->smallInteger(1)->notNull()->defaultValue(0),
			'block_not'      => $this->string()->defaultValue(null),
			'blocked_at'     => $this->integer()->defaultValue(null),
			'last_login_at'  => $this->integer(),
			'last_activated' => $this->integer(),
			'created_at'     => $this->integer(),
			'updated_at'     => $this->integer(),
		], $tableOptions);
		$this->createIndex('email', '{{%user}}', 'email', 1);
		$this->insert('{{%user}}', [
			'id'         => '1',
			'role'       => '1',
			'username'   => 'admin',
			'password'   => Yii::$app->security->generatePasswordHash('123456'),
			'password_2' => Yii::$app->security->generatePasswordHash('123456'),
			'full_name'  => 'Admin Manager',
			'phone'      => '0949968789',
			'email'      => 'admin@gmail.com',
			'position'   => 0,
		]);
		$this->addForeignKey('fk_user_payment_to_sponsor', '{{%user}}', 'sponsor_id', 'user', 'id', 'NO ACTION', 'NO ACTION');
	}

	public function down() {
		echo "m161009_160746_initial_tables cannot be reverted.\n";
		return false;
	}
}
