<?php
use yii\db\Migration;

class m161009_161159_initial_table_setting extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%setting}}', [
			'id'          => $this->primaryKey(),
			'parent_id'   => $this->integer()->notNull()->defaultValue(0),
			'code'        => $this->string(255)->notNull(),
			'name'        => $this->string(255),
			'desc'        => $this->string(),
			'type'        => $this->smallInteger(2)->notNull()->defaultValue(1),
			'store_range' => $this->string(255),
			'store_dir'   => $this->string(255),
			'value'       => $this->text(),
			'sort_order'  => $this->integer()->notNull()->defaultValue(1),
		], $tableOptions);
		$this->createIndex('parent_id', '{{%setting}}', 'parent_id', 0);
		$this->createIndex('code', '{{%setting}}', 'code', 0);
		$this->createIndex('sort_order', '{{%setting}}', 'sort_order', 0);
		$this->insert('{{%setting}}', [
			'id'          => '1',
			'parent_id'   => '0',
			'code'        => 'general',
			'name'        => 'Cấu hình chung',
			'desc'        => '',
			'type'        => '0',
			'store_range' => '',
			'store_dir'   => '',
			'value'       => '',
			'sort_order'  => '1',
		]);
		$this->insert('{{%setting}}', [
			'id'          => '2',
			'parent_id'   => '1',
			'code'        => 'general_website_close',
			'name'        => 'Đóng website',
			'desc'        => 'Đóng website và thông báo bảo trì',
			'type'        => '16',
			'store_range' => 'Không,Có',
			'store_dir'   => '',
			'value'       => 'Không',
			'sort_order'  => '1',
		]);
	}

	public function down() {
		echo "m161009_161159_initial_table_setting cannot be reverted.\n";
		return false;
	}
}
