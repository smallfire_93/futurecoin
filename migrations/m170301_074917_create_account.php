<?php
use yii\db\Migration;

class m170301_074917_create_account extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('{{%account}}', [
			'id'              => $this->primaryKey(),
			'user_id'         => $this->integer(),
			'cash_account'    => $this->float()->notNull()->defaultValue(0),
			'trading_account' => $this->float()->notNull()->defaultValue(0),
			'token'           => $this->float()->notNull()->defaultValue(0),
			'coin'            => $this->float()->notNull()->defaultValue(0),
			'package_id'      => $this->integer(),
		], $tableOptions);
		$this->addForeignKey('fk_account_to_user', 'account', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
	}

	public function down() {
		echo "m170301_074917_create_account cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
