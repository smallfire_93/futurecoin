<?php
use yii\db\Migration;

class m170301_074926_create_packages extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('package', [
			'id'    => $this->primaryKey(),
			'name'  => $this->string(255),
			'money' => $this->integer(),
			'token' => $this->integer(),
			'level' => $this->integer(),
		], $tableOptions);
		$this->addForeignKey('account_fk_package', 'account', 'package_id', 'package', 'id', 'NO ACTION', 'NO ACTION');
	}

	public function down() {
		echo "m170301_074926_create_packages cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
