<?php
use yii\db\Migration;

class m170314_030706_update_admin extends Migration {

	public function up() {
		$this->update('user', ['role' => 2], 'username="admin"');
	}

	public function down() {
		echo "m170314_030706_update_admin cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
