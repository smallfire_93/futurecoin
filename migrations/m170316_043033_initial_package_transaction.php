<?php
use yii\db\Migration;

class m170316_043033_initial_package_transaction extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('package_transaction', [
			'id'           => $this->primaryKey(),
			'user_id'      => $this->integer()->notNull(),
			'package_id'   => $this->integer(),
			'type'         => $this->smallInteger(1),
			'created_date' => $this->timestamp()->null(),
		], $tableOptions);
		$this->addForeignKey('package_transaction_fk_user_id', 'package_transaction', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('package_transaction_fk_package_id', 'package_transaction', 'package_id', 'package', 'id', 'CASCADE', 'CASCADE');
	}

	public function down() {
		echo "m170316_043033_initial_package_transaction cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
