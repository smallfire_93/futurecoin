<?php
use yii\db\Migration;

class m170316_043719_initial_coin_mining extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('coin_mining', [
			'id'                => $this->primaryKey(),
			'user_id'           => $this->integer()->notNull(),
			'start_date'        => $this->timestamp()->null(),
			'begin_return_coin' => $this->dateTime()->null(),
		], $tableOptions);
	}

	public function down() {
		echo "m170316_043719_initial_coin_mining cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
