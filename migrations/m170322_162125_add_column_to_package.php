<?php
use yii\db\Migration;

class m170322_162125_add_column_to_package extends Migration {

	public function up() {
		$this->addColumn('package', 'image', $this->string(255));
	}

	public function down() {
		echo "m170322_162125_add_column_to_package cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
