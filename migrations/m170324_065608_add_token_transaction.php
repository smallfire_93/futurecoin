<?php
use yii\db\Migration;

class m170324_065608_add_token_transaction extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->addColumn('user', 'active_at', $this->dateTime()->null());
		$this->createTable('token_transaction', [
			'id'       => $this->primaryKey(),
			'user_id'  => $this->integer(),
			'type'     => $this->smallInteger(1)->defaultValue(1),
			'date'     => $this->timestamp()->null(),
			'quantity' => $this->integer(),
			'price'    => $this->float(),
			'money'    => $this->float(),
		], $tableOptions);
	}

	public function down() {
		echo "m170324_065608_add_token_transaction cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
