<?php
use yii\db\Migration;
use yii\db\mysql\Schema;

class m170325_150850_add_column_to_user extends Migration {

	public function up() {
		$this->addColumn('user', 'total_bonus', $this->float()->defaultValue(0));
		$this->addColumn('{{%setting}}', 'icon', Schema::TYPE_STRING . '(255) NOT NULL DEFAULT ""');
	}

	public function down() {
		echo "m170325_150850_add_column_to_user cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
