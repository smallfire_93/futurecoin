<?php
use yii\db\Migration;

class m170326_171520_initial_table_cash_transaction extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('cash_transaction', [
			'id'      => $this->primaryKey(),
			'user_id' => $this->integer(),
			'type'    => $this->smallInteger(1),
			'money'   => $this->float(),
			'status'  => $this->smallInteger(1),
			'date'    => $this->timestamp()->null(),
		], $tableOptions);
		$this->addColumn('token_transaction', 'status', $this->smallInteger(1));
	}

	public function down() {
		echo "m170326_171520_initial_table_cash_transaction cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
