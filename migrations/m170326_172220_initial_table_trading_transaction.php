<?php
use yii\db\Migration;

class m170326_172220_initial_table_trading_transaction extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('trading_transaction', [
			'id'       => $this->primaryKey(),
			'user_id'  => $this->integer(),
			'type'     => $this->smallInteger(1),
			'money'    => $this->float(),
			'status'   => $this->smallInteger(1),
			'quantity' => $this->integer(),
			'date'     => $this->timestamp()->null(),
		], $tableOptions);
	}

	public function down() {
		echo "m170326_172220_initial_table_trading_transaction cannot be reverted.\n";
		return false;
	}
}
