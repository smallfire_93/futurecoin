<?php
use yii\db\Migration;

class m170326_172434_initial_table_coin_transaction extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('coin_transaction', [
			'id'       => $this->primaryKey(),
			'user_id'  => $this->integer(),
			'type'     => $this->smallInteger(1),
			'money'    => $this->float(),
			'status'   => $this->smallInteger(1),
			'quantity' => $this->integer(),
			'date'     => $this->timestamp()->null(),
		], $tableOptions);
	}

	public function down() {
		echo "m170326_172434_initial_table_coin_transaction cannot be reverted.\n";
		return false;
	}
}
