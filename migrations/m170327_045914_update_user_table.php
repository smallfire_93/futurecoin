<?php
use yii\db\Migration;

class m170327_045914_update_user_table extends Migration {

	public function up() {
		$this->renameColumn("user", 'block_not', 'block_note');
		$this->addColumn('user', 'bit_coin_address', $this->string(125));
	}

	public function down() {
		echo "m170327_045914_update_user_table cannot be reverted.\n";
		return false;
	}
}
