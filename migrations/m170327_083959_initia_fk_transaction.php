<?php
use yii\db\Migration;

class m170327_083959_initia_fk_transaction extends Migration {

	public function up() {
		$this->addForeignKey('cash_transaction_fk_user', 'cash_transaction', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('trading_transaction_fk_user', 'trading_transaction', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('token_transaction_fk_user', 'token_transaction', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('coin_transaction_fk_user', 'coin_transaction', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
	}

	public function down() {
		echo "m170327_083959_initia_fk_transaction cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
