<?php
use yii\db\Migration;
use yii\db\mysql\Schema;

class m170327_165531_update_user_information extends Migration {

	public function up() {
		$this->addColumn('user', 'bank_account_number', Schema::TYPE_CHAR . '(255) NULL');
		$this->addColumn('user', 'bank_account_holder', Schema::TYPE_CHAR . '(255) NULL');
		$this->addColumn('user', 'bank_branch_name', Schema::TYPE_CHAR . '(255) NULL');
		$this->addColumn('user', 'bank_name', Schema::TYPE_CHAR . '(255) NULL');
	}

	public function down() {
		echo "m170327_165531_update_user_information cannot be reverted.\n";
		return false;
	}
}
