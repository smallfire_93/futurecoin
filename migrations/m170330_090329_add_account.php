<?php
use yii\db\Migration;

class m170330_090329_add_account extends Migration {

	public function up() {
		$this->insert('account', [
			'id'      => 1,
			'user_id' => 1,
		]);
	}

	public function down() {
		echo "m170330_090329_add_account cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
