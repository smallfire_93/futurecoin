<?php
use yii\db\Migration;

class m170412_101910_add_current_bonus extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('current_bonus', [
			'id'               => $this->primaryKey(),
			'user_id'          => $this->integer(),
			'current_cash'     => $this->float()->null()->defaultValue(0),
			'current_trading'  => $this->float()->null()->defaultValue(0),
			'current_bv_left'  => $this->float()->null()->defaultValue(0),
			'current_bv_right' => $this->float()->null()->defaultValue(0),
			'weak_branch'      => $this->smallInteger(1)->null()->defaultValue(0),
			'weak_branch_bv'   => $this->float()->null()->defaultValue(0),
			'total_bonus'      => $this->float()->null()->defaultValue(0),
		], $tableOptions);
		$this->addForeignKey('current_bonus_fk_user', 'current_bonus', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
		$this->addColumn('user', 'left_bv', $this->float()->null()->defaultValue(0));
		$this->addColumn('user', 'right_bv', $this->float()->null()->defaultValue(0));
	}

	public function down() {
		echo "m170412_101910_add_current_bonus cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
