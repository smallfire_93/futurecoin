<?php
use yii\db\Migration;

class m170416_171142_initial_bonus_transaction extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('bonus_transaction', [
			'id'             => $this->primaryKey(),
			'user_id'        => $this->integer(),
			'receipted_date' => $this->timestamp(),
			'total_bonus'    => $this->float(),
			'cash'           => $this->float(),
			'trading'        => $this->float(),
			'direct_bonus'   => $this->float(),
			'matching_bonus' => $this->float(),
			'weak_branch_bonus' => $this->float(),
		], $tableOptions);
		$this->addForeignKey('bonus_transaction_fk_user', 'bonus_transaction', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
		$this->addColumn('current_bonus', 'direct_bonus', $this->float()->null()->defaultValue(0));
		$this->addColumn('current_bonus', 'matching_bonus', $this->float()->null()->defaultValue(0));
		$this->addColumn('current_bonus', 'weak_branch_bonus', $this->float()->null()->defaultValue(0));
	}

	public function down() {
		echo "m170416_171142_initial_bonus_transaction cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
