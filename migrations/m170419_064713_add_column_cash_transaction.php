<?php
use yii\db\Migration;

class m170419_064713_add_column_cash_transaction extends Migration {

	public function up() {
		$this->addColumn('cash_transaction', 'receive_id', $this->integer());
	}

	public function down() {
		echo "m170419_064713_add_column_cash_transaction cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
