<?php
use yii\db\Migration;

class m170423_001259_delete_index_email extends Migration {

	public function up() {
		$this->dropIndex('email', 'user');
	}

	public function down() {
		echo "m170423_001259_delete_index_email cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
