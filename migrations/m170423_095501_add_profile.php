<?php
use yii\db\Migration;

class m170423_095501_add_profile extends Migration {

	public function up() {
		$this->addColumn('user', 'country', $this->string());
		$this->addColumn('user', 'address', $this->string());
		$this->addColumn('user', 'id_number', $this->string());
	}

	public function down() {
		echo "m170423_095501_add_profile cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
