<?php
use yii\db\Migration;

class m170423_133014_add_column_user extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('user_kyc', [
			'id'             => $this->primaryKey(),
			'user_id'        => $this->integer(),
			'id_frond'       => $this->string(),
			'id_back'        => $this->string(),
			'address_proof'  => $this->string(),
			'address_proof2' => $this->string(),
			'kyc_status'     => $this->smallInteger(1),
		], $tableOptions);
		$this->addForeignKey('kyc_fk_user', 'user_kyc', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
	}

	public function down() {
		echo "m170423_133014_add_column_user cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
