<?php
use yii\db\Migration;

class m170423_142416_alter_kyc extends Migration {

	public function up() {
		$this->alterColumn('user_kyc', 'kyc_status', $this->smallInteger(1)->defaultValue(0));
	}

	public function down() {
		echo "m170423_142416_alter_kyc cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
