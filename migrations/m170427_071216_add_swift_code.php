<?php
use yii\db\Migration;

class m170427_071216_add_swift_code extends Migration {

	public function up() {
		$this->addColumn('user', 'swift_code', $this->string());
	}

	public function down() {
		echo "m170427_071216_add_swift_code cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
