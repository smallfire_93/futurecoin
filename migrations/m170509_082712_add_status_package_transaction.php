<?php
use yii\db\Migration;

class m170509_082712_add_status_package_transaction extends Migration {

	public function up() {
		$this->addColumn('package_transaction', 'status', $this->smallInteger(1));
	}

	public function down() {
		echo "m170509_082712_add_status_package_transaction cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
