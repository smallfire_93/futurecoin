<?php
use yii\db\Migration;

class m170509_082857_alter_status_transaction extends Migration {

	public function up() {
		$this->alterColumn('package_transaction', 'status', $this->smallInteger(1)->defaultValue(1));
	}

	public function down() {
		echo "m170509_082857_alter_status_transaction cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
