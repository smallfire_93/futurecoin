<?php

use yii\db\Migration;

class m170510_034226_alter_package_status extends Migration
{
    public function up()
    {
	    $this->alterColumn('package_transaction', 'status', $this->smallInteger(1)->defaultValue(1)->notNull());
    }

    public function down()
    {
        echo "m170510_034226_alter_package_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
