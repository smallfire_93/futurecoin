<?php
use yii\db\Migration;

class m170514_141622_add_split_table extends Migration {

	public function up() {
		$this->alterColumn('token_transaction', 'quantity', $this->float());
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('start_setting', [
			'id'          => $this->primaryKey(),
			'web_start'   => $this->dateTime(),
			'coin_price'  => $this->float(),
			'token_price' => $this->float(),
			'token_split' => $this->float(),
		], $tableOptions);
	}

	public function down() {
		echo "m170514_141622_add_split_table cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
