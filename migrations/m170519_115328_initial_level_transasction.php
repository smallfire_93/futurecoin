<?php
use yii\db\Migration;

class m170519_115328_initial_level_transasction extends Migration {

	public function up() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('level_current', [
			'id'         => $this->primaryKey(),
			'user_id'    => $this->integer()->notNull(),
			'level_id'   => $this->integer(),
			'current_bv' => $this->integer()->notNull(),
			'number_sap' => $this->integer()->defaultValue(0)->notNull(),
			'number_rub' => $this->integer()->defaultValue(0)->notNull(),
			'number_eme' => $this->integer()->defaultValue(0)->notNull(),
			'number_dia' => $this->integer()->defaultValue(0)->notNull(),
			'number_bla' => $this->integer()->defaultValue(0)->notNull(),
			'number_gre' => $this->integer()->defaultValue(0)->notNull(),
		], $tableOptions);
		$this->addColumn('cash_transaction', 'level_id', $this->integer());
	}

	public function down() {
		echo "m170519_115328_initial_level_transasction cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
