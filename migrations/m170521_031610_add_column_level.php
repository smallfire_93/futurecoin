<?php
use yii\db\Migration;

class m170521_031610_add_column_level extends Migration {

	public function up() {
		$this->addColumn('level', 'bv_range', $this->integer());
	}

	public function down() {
		echo "m170521_031610_add_column_level cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
