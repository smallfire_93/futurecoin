<?php
use yii\db\Migration;

class m170524_020953_addcolumn_user extends Migration {

	public function up() {
		$this->addColumn('user', 'zip_code', $this->integer());
		$this->addColumn('user', 'expire_date', $this->dateTime());
		$this->addColumn('user', 'city', $this->string());
		$this->addColumn('account', 'token_sell', $this->float()->notNull()->defaultValue(0));
	}

	public function down() {
		echo "m170524_020953_addcolumn_user cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
