<?php
use yii\db\Migration;

class m170526_200708_alter_table_level_current extends Migration {

	public function up() {
		$this->addColumn('level_current', 'current_bv_right', $this->integer());
		$this->addColumn('level_current', 'current_bv_left', $this->integer());
	}

	public function down() {
		echo "m170526_200708_alter_table_level_current cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
