<?php
use yii\db\Migration;

class m170528_080335_add_columns_user extends Migration {

	public function up() {
		$this->addColumn('account', 'total_tokens', $this->float()->defaultValue(0));
		$accounts = \app\models\Account::find()->where([
			'!=',
			'token',
			0,
		])->all();
		foreach($accounts as $account) {
			$account->updateAttributes(['total_tokens' => $account->token]);
		}
	}

	public function down() {
		echo "m170528_080335_add_columns_user cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
