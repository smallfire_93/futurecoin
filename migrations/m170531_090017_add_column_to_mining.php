<?php
use yii\db\Migration;

class m170531_090017_add_column_to_mining extends Migration {

	public function up() {
		$this->addColumn('coin_mining', 'quantity', $this->float()->notNull());
		$this->addColumn('coin_mining', 'type', $this->integer(1)->notNull());
	}

	public function down() {
		echo "m170531_090017_add_column_to_mining cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
