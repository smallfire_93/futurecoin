<?php
use yii\db\Migration;

class m170531_134532_alter_column_coin extends Migration {

	public function up() {
		$this->alterColumn('coin_transaction', 'quantity', $this->float());
	}

	public function down() {
		echo "m170531_134532_alter_column_coin cannot be reverted.\n";
		return false;
	}
	/*
	// Use safeUp/safeDown to run migration code within a transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}
