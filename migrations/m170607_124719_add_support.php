<?php
use yii\db\Migration;

class m170607_124719_add_support extends Migration {

	public function safeUp() {
		$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		$this->createTable('support', [
			'id'      => $this->primaryKey(),
			'user_id' => $this->integer(),
			'title'   => $this->string(255),
			'content' => $this->text(),
		], $tableOptions);
	}

	public function safeDown() {
		echo "m170607_124719_add_support cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m170607_124719_add_support cannot be reverted.\n";

		return false;
	}
	*/
}
