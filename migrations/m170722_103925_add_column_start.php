<?php
use yii\db\Migration;

class m170722_103925_add_column_start extends Migration {

	public function safeUp() {
		$this->addColumn('account', 'split_number', $this->integer()->defaultValue(0));
		$this->insert('start_setting', [
			'id'          => '1',
			'web_start'   => date('Y-m-d H:i:s'),
			'coin_price'  => '0.05',
			'token_price' => '0.1',
			'token_split' => '1',
		]);
	}

	public function safeDown() {
		echo "m170722_103925_add_column_start cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m170722_103925_add_column_start cannot be reverted.\n";

		return false;
	}
	*/
}
