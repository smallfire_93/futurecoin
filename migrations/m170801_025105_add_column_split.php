<?php
use yii\db\Migration;

class m170801_025105_add_column_split extends Migration {

	public function safeUp() {
		$this->addColumn('start_setting', 'split_percent', $this->float());
	}

	public function safeDown() {
		echo "m170801_025105_add_column_split cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m170801_025105_add_column_split cannot be reverted.\n";

		return false;
	}
	*/
}
