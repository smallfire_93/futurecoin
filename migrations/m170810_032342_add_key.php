<?php
use yii\db\Migration;

class m170810_032342_add_key extends Migration {

	public function safeUp() {
		$this->addColumn('user', 'reset_key', $this->string(255)->defaultValue(null));
	}

	public function safeDown() {
		echo "m170810_032342_add_key cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m170810_032342_add_key cannot be reverted.\n";

		return false;
	}
	*/
}
