<?php
use yii\db\Migration;

/**
 * Class m171202_082647_add_column_support
 */
class m171202_082647_add_column_support extends Migration {

	/**
	 * @inheritdoc
	 */
	public function safeUp() {
		$this->addColumn('support', 'status', $this->integer()->defaultValue(0));
		$this->addColumn('support', 'parent_id', $this->integer());
//		$this->update('support', ['parent_id' => new \yii\db\Expression('user_id')]);
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {
		echo "m171202_082647_add_column_support cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m171202_082647_add_column_support cannot be reverted.\n";

		return false;
	}
	*/
}
