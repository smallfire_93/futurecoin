<?php
use yii\db\Migration;

/**
 * Class m171202_180527_add_column
 */
class m171202_180527_add_column extends Migration {

	/**
	 * @inheritdoc
	 */
	public function safeUp() {
		$this->addColumn('support', 'flag', $this->integer()->defaultValue(0));
	}

	/**
	 * @inheritdoc
	 */
	public function safeDown() {
		echo "m171202_180527_add_column cannot be reverted.\n";
		return false;
	}
	/*
	// Use up()/down() to run migration code without a transaction.
	public function up()
	{

	}

	public function down()
	{
		echo "m171202_180527_add_column cannot be reverted.\n";

		return false;
	}
	*/
}
