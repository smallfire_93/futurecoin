<?php
namespace app\models;

use app\components\Model;
use Yii;

/**
 * This is the model class for table "account".
 *
 * @property integer $id
 * @property integer $user_id
 * @property double  $cash_account
 * @property double  $trading_account
 * @property double  $token
 * @property double  $total_tokens
 * @property double  $coin
 * @property integer $package_id
 *
 * @property Package $package
 * @property User    $user
 */
class Account extends Model {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'account';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'user_id',
					'package_id',
				],
				'integer',
			],
			[
				[
					'cash_account',
					'trading_account',
					'token',
					'total_tokens',
					'coin',
				],
				'number',
			],
			[
				['package_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Package::className(),
				'targetAttribute' => ['package_id' => 'id'],
			],
			[
				['user_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => User::className(),
				'targetAttribute' => ['user_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'              => 'ID',
			'user_id'         => 'User ID',
			'cash_account'    => 'Cash Account',
			'trading_account' => 'Trading Account',
			'token'           => 'Token',
			'total_tokens'    => 'Total token',
			'coin'            => 'Coin',
			'package_id'      => 'Package ID',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPackage() {
		return $this->hasOne(Package::className(), ['id' => 'package_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUsers() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}
}
