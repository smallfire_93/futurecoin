<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bonus_transaction".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $receipted_date
 * @property double $total_bonus
 * @property double $cash
 * @property double $trading
 * @property double $direct_bonus
 * @property double $matching_bonus
 * @property double $weak_branch_bonus
 *
 * @property User $user
 */
class BonusTransaction extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bonus_transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'integer'],
            [['receipted_date'], 'safe'],
            [['total_bonus', 'cash', 'trading', 'direct_bonus', 'matching_bonus', 'weak_branch_bonus'], 'number'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'receipted_date' => 'Receipted Date',
            'total_bonus' => 'Total Bonus',
            'cash' => 'Cash',
            'trading' => 'Trading',
            'direct_bonus' => 'Direct Bonus',
            'matching_bonus' => 'Matching Bonus',
            'weak_branch_bonus' => 'Weak Branch Bonus',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
