<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cash_transaction".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $receive_id
 * @property integer $type
 * @property double  $money
 * @property integer $status
 * @property string  $date
 *
 * @property User    $users
 * @property User    $receiver
 */
class CashTransaction extends \app\components\Model {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'cash_transaction';
	}

	const TYPE           = [
		1 => 'Buy package',
		2 => 'Bonus',
		3 => 'Withdraw bonus',
		4 => 'Buy package fee',
		5 => 'Send upline',
		6 => 'Send downline',
		7 => 'Receive',
		8 => 'Sell tokens',
		9 => 'Reject withdraw',
	];

	const TYPE_BUY       = 1;

	const TYPE_BON       = 2;

	const TYPE_WIT       = 3;

	const TYPE_FEE       = 4;

	const TYPE_UPLINE    = 5;

	const TYPE_DOWNLINE  = 6;

	const TYPE_RECEIVE   = 7;

	const TYPE_TOKEN     = 8;

	const TYPE_REJECT    = 9;

	const STATUS         = [
		'Pending',
		'Success',
		'Error',
		'Reject',
	];

	const STATUS_SUCCESS = 1;

	const STATUS_REJECT  = 3;

	const STATUS_PENDING = 0;

	public $bit_address;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'user_id',
					'type',
					'status',
					'receive_id',
					'level_id',
				],
				'integer',
			],
			[
				['money'],
				'number',
			],
			[
				[
					'date',
					'bit_address',
				],
				'safe',
			],
			[
				['user_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => User::className(),
				'targetAttribute' => ['user_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'         => 'ID',
			'user_id'    => 'User',
			'type'       => 'Type',
			'money'      => 'Money',
			'status'     => 'Status',
			'date'       => 'Date',
			'receive_id' => 'Receiver',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUsers() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	public function getReceiver() {
		return $this->hasOne(User::className(), ['id' => 'receive_id']);
	}
}
