<?php
namespace app\models;

use app\components\Model;
use Yii;

/**
 * This is the model class for table "coin_mining".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $quantity
 * @property integer $type
 * @property string  $start_date
 * @property string  $begin_return_coin
 */
class CoinMining extends Model {

	const MINING_TYPE = [
		'Token',
		'Trading',
	];

	const TOKEN       = 0;

	const TRADING     = 1;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'coin_mining';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				['user_id'],
				'required',
			],
			[
				[
					'user_id',
					'type',
				],
				'integer',
			],
			[
				[
					'start_date',
					'begin_return_coin',
					'quantity',
				],
				'safe',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'                => 'ID',
			'user_id'           => 'User ID',
			'quantity'          => 'Return quantity',
			'type'              => 'Mining type',
			'start_date'        => 'Start Date',
			'begin_return_coin' => 'Begin Return Coins',
		];
	}
}
