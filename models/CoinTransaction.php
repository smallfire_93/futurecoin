<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "coin_transaction".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $type
 * @property double  $money
 * @property integer $status
 * @property double $quantity
 * @property string  $date
 *
 * @property User    $user
 */
class CoinTransaction extends \app\components\Model {

	const STATUS  = [
		'Pending',
		'Success',
	];

	const PENDING = 0;

	const SUCCESS = 1;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'coin_transaction';
	}

	const TYPE    = [
		'Receive from mining',
		'Buy',
		'Sell',
	];

	const RECEIVE = 0;

	const BUY     = 1;

	const SELL    = 2;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'user_id',
					'type',
					'status',
				],
				'integer',
			],
			[
				[
					'money',
					'quantity',
				],
				'number',
			],
			[
				['date'],
				'safe',
			],
			[
				['user_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => User::className(),
				'targetAttribute' => ['user_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'       => 'ID',
			'user_id'  => 'User ID',
			'type'     => 'Type',
			'money'    => 'Money',
			'status'   => 'Status',
			'quantity' => 'Quantity',
			'date'     => 'Date',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}
}
