<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "crypto_payments".
 *
 * @property string $paymentID
 * @property string $boxID
 * @property string $boxType
 * @property string $orderID
 * @property string $userID
 * @property string $countryID
 * @property string $coinLabel
 * @property double $amount
 * @property double $amountUSD
 * @property integer $unrecognised
 * @property string $addr
 * @property string $txID
 * @property string $txDate
 * @property integer $txConfirmed
 * @property string $txCheckDate
 * @property integer $processed
 * @property string $processedDate
 * @property string $recordCreated
 */
class CryptoPayments extends \app\components\Model
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'crypto_payments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['boxID', 'unrecognised', 'txConfirmed', 'processed'], 'integer'],
            [['boxType'], 'required'],
            [['boxType'], 'string'],
            [['amount', 'amountUSD'], 'number'],
            [['txDate', 'txCheckDate', 'processedDate', 'recordCreated'], 'safe'],
            [['orderID', 'userID'], 'string', 'max' => 50],
            [['countryID'], 'string', 'max' => 3],
            [['coinLabel'], 'string', 'max' => 6],
            [['addr'], 'string', 'max' => 34],
            [['txID'], 'string', 'max' => 64],
            [['boxID', 'orderID', 'userID', 'txID', 'amount', 'addr'], 'unique', 'targetAttribute' => ['boxID', 'orderID', 'userID', 'txID', 'amount', 'addr'], 'message' => 'The combination of Box ID, Order ID, User ID, Amount, Addr and Tx ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'paymentID' => 'Payment ID',
            'boxID' => 'Box ID',
            'boxType' => 'Box Type',
            'orderID' => 'Order ID',
            'userID' => 'User ID',
            'countryID' => 'Country ID',
            'coinLabel' => 'Coin Label',
            'amount' => 'Amount',
            'amountUSD' => 'Amount Usd',
            'unrecognised' => 'Unrecognised',
            'addr' => 'Addr',
            'txID' => 'Tx ID',
            'txDate' => 'Tx Date',
            'txConfirmed' => 'Tx Confirmed',
            'txCheckDate' => 'Tx Check Date',
            'processed' => 'Processed',
            'processedDate' => 'Processed Date',
            'recordCreated' => 'Record Created',
        ];
    }
}
