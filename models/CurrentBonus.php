<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "current_bonus".
 *
 * @property integer $id
 * @property integer $user_id
 * @property double  $current_cash
 * @property double  $current_trading
 * @property double  $current_bv_left
 * @property double  $current_bv_right
 * @property integer $weak_branch
 * @property double  $weak_branch_bv
 * @property double  $total_bonus
 * @property double  $direct_bonus
 * @property double  $matching_bonus
 * @property double  $weak_branch_bonus
 *
 * @property User    $users
 */
class CurrentBonus extends \app\components\Model {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'current_bonus';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'user_id',
					'weak_branch',
				],
				'integer',
			],
			[
				[
					'current_cash',
					'current_trading',
					'current_bv_left',
					'current_bv_right',
					'weak_branch_bv',
					'total_bonus',
					'direct_bonus',
					'matching_bonus',
					'weak_branch_bonus',
				],
				'number',
			],
			[
				['user_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => User::className(),
				'targetAttribute' => ['user_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'                => 'ID',
			'user_id'           => 'User ID',
			'current_cash'      => 'Current Cash',
			'current_trading'   => 'Current Trading',
			'current_bv_left'   => 'Current Bv Left',
			'current_bv_right'  => 'Current Bv Right',
			'weak_branch'       => 'Weak Branch',
			'weak_branch_bv'    => 'Weak Branch Bv',
			'total_bonus'       => 'Total Bonus',
			'direct_bonus'      => 'Direct Bonus',
			'matching_bonus'    => 'Matching Bonus',
			'weak_branch_bonus' => 'Weak Branch Bonus',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUsers() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}
}
