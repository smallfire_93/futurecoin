<?php
namespace app\models;

use app\components\Model;
use Yii;

/**
 * This is the model class for table "level".
 *
 * @property integer $id
 * @property string  $name
 * @property integer $bv
 * @property integer $number_sap
 * @property integer $number_rub
 * @property integer $number_eme
 * @property integer $number_dia
 * @property integer $number_bla
 * @property integer $number_gre
 * @property integer $order_by
 * @property double  $bonus
 */
class Level extends Model {

	const LEVEL_SAP = 1;

	const LEVEL_RUB = 2;

	const LEVEL_EME = 3;

	const LEVEL_DIA = 4;

	const LEVEL_BLA = 5;

	const LEVEL_GRE = 6;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'level';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'name',
					'bv',
					'bonus',
				],
				'required',
			],
			[
				[
					'bv',
					'bv_range',
					'number_sap',
					'number_rub',
					'number_eme',
					'number_dia',
					'number_bla',
					'number_gre',
					'order_by',
				],
				'integer',
			],
			[
				['bonus'],
				'number',
			],
			[
				['name'],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'         => 'ID',
			'name'       => 'Name',
			'bv'         => 'Bv',
			'bv_range'   => 'Bv range',
			'number_sap' => 'Number Sap',
			'number_rub' => 'Number Rub',
			'number_eme' => 'Number Eme',
			'number_dia' => 'Number Dia',
			'number_bla' => 'Number Bla',
			'number_gre' => 'Number Gre',
			'bonus'      => 'Bonus',
			'order_by'   => 'Order',
		];
	}

	/**
	 * @inheritdoc
	 * @return LevelQuery the active query used by this AR class.
	 */
	public static function find() {
		return new LevelQuery(get_called_class());
	}
}
