<?php
namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "level_current".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $level_id
 * @property integer $current_bv
 * @property integer $current_bv_left
 * @property integer $current_bv_right
 * @property integer $number_sap
 * @property integer $number_rub
 * @property integer $number_eme
 * @property integer $number_dia
 * @property integer $number_bla
 * @property integer $number_gre
 * @property User    $users
 * @property Level   $level
 */
class LevelCurrent extends \app\components\Model {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'level_current';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'user_id',
					'current_bv',
				],
				'required',
			],
			[
				[
					'user_id',
					'level_id',
					'current_bv',
					'number_sap',
					'number_rub',
					'number_eme',
					'number_dia',
					'number_bla',
					'number_gre',
					'current_bv_left',
					'current_bv_right',
				],
				'integer',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'         => 'ID',
			'user_id'    => 'User ID',
			'level_id'   => 'Level ID',
			'current_bv' => 'Current Bv',
			'number_sap' => 'Number Sap',
			'number_rub' => 'Number Rub',
			'number_eme' => 'Number Eme',
			'number_dia' => 'Number Dia',
			'number_bla' => 'Number Bla',
			'number_gre' => 'Number Gre',
		];
	}

	/**
	 * @inheritdoc
	 * @return LevelCurrentQuery the active query used by this AR class.
	 */
	public static function find() {
		return new LevelCurrentQuery(get_called_class());
	}

	public function getCurrentLevel() {
		$level = Level::find()->where([
			'<=',
			'bv',
			$this->current_bv,
		])->andWhere([
			'>',
			'bv_range',
			$this->current_bv,
		])->one();
		if($level) {
			$order_level = $level->order_by;
			while(!$this->checkCondition($order_level) && $order_level >= 1) {
				$order_level = $order_level - 1;
			}
			$level_id = $order_level;
			//			if($level->name==)
		} else {
			$level_id = false;
		}
		return $level_id;
	}

	public function getUsers() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	public function getLevel() {
		return $this->hasOne(Level::className(), ['id' => 'level_id']);
	}

	/**
	 * Hàm trả về mảng gồm level của các f1
	 */
	public function getLevelDirect($order) {
		$array_level = [];
		//		if($order >= 4) {
		$user_begin     = User::find()->where(['parent_id' => $this->user_id])->andWhere(['position' => $this->getWeakBranch($this->user_id)])->one();
		$array_children = $this->users->getTotalChildren($user_begin->id);
		$array_children = ArrayHelper::merge($array_children, [$user_begin->id]);
		$total_directs  = User::find()->where(['sponsor_id' => $this->user_id])->andWhere([
			'IN',
			'id',
			$array_children,
		])->all();
		//		} else {
		//			$total_directs = User::find()->where(['sponsor_id' => $this->user_id])->all();
		//		}
		if($total_directs) {
			foreach($total_directs as $total_direct) {
				if($total_direct->currentLevel) {
					if($total_direct->currentLevel->level_id != null) {
						$array_level[] = $total_direct->currentLevel->level_id;
					}
				}
			}
		}
		return $array_level;
	}

	public function checkCondition($order) {
		$count = array_count_values($this->getLevelDirect($order));
		switch($order) {
			case  Level::LEVEL_SAP:
				$check = true;
				break;
			case Level::LEVEL_RUB:
				if(array_key_exists(Level::LEVEL_SAP, $count)) {
					if($count[Level::LEVEL_SAP] >= 1) {
						$check = true;
					} else {
						$check = false;
					}
				} else {
					$check = false;
				}
				break;
			case Level::LEVEL_EME:
				if(array_key_exists(Level::LEVEL_RUB, $count)) {
					if($count[Level::LEVEL_RUB] >= 1) {
						$check = true;
					} else {
						$check = false;
					}
				} else {
					$check = false;
				}
				break;
			case Level::LEVEL_DIA:
				if(array_key_exists(Level::LEVEL_EME, $count)) {
					if($count[Level::LEVEL_EME] >= 1) {
						$check = true;
					} else {
						$check = false;
					}
				} else {
					$check = false;
				}
				break;
			case  Level::LEVEL_BLA:
				if(array_key_exists(Level::LEVEL_DIA, $count)) {
					if($count[Level::LEVEL_DIA] >= 2) {
						$check = true;
					} else {
						$check = false;
					}
				} else {
					$check = false;
				}
				break;
			case Level::LEVEL_GRE:
				if(array_key_exists(Level::LEVEL_DIA, $count)) {
					if($count[Level::LEVEL_DIA] >= 2 || $count) {
						$check = true;
					} else {
						$check = false;
					}
				} else {
					$check = false;
				}
				break;
			default:
				$check = false;
		}
		return $check;
	}
}
