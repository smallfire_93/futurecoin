<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[LevelCurrent]].
 *
 * @see LevelCurrent
 */
class LevelCurrentQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return LevelCurrent[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return LevelCurrent|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
