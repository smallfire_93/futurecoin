<?php

namespace app\models;

use app\components\Model;
use Yii;

/**
 * This is the model class for table "package".
 *
 * @property integer   $id
 * @property string    $name
 * @property string    $image
 * @property integer   $money
 * @property integer   $token
 * @property integer   $level
 *
 * @property Account[] $accounts
 */
class Package extends Model {

	/**
	 * @inheritdoc
	 */
	public $package_img;

	public static function tableName() {
		return 'package';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'money',
					'token',
					'level',
				],
				'integer',
			],
			[
				[
					'name',
					'image',
				],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'    => 'ID',
			'name'  => 'Name',
			'money' => 'Money',
			'image' => 'Image',
			'token' => 'Token',
			'level' => 'Level',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getAccounts() {
		return $this->hasMany(Account::className(), ['package_id' => 'id']);
	}

	public function getPackageTransaction() {
		return $this->hasMany(PackageTransaction::className(), ['package_id' => 'id']);
	}

	/**
	 * Trả về số $ thực nhận;
	 */
	public function getAmount() {
		$dolar = 22.630;
		$euro  = 26.8;
		if($this->user->accounts->package_id == null) {
			$this->money = $this->money + 25;
		}
		$amount = ($this->money * $euro) / $dolar;
		return $amount;
	}

	/**
	 * Update tiền hoa hồng trực tiếp.
	 */
	public function updateDirectBonus() {
		$sponsor = $this->user->sponsors;
		$package = $this;
		if($sponsor) {
			if($sponsor->currentBonus) {
				$current_bonus   = $sponsor->currentBonus;
				$current_cash    = $current_bonus->current_cash;
				$current_trading = $current_bonus->current_trading;
				$current_direct  = $current_bonus->direct_bonus;
				$current_total   = $current_bonus->total_bonus;
				$new_direct      = $current_direct + (0.1 * $package->money);
				$new_cash        = $current_cash + (0.06 * $package->money);
				$new_trading     = $current_trading + (0.04 * $package->money);
				$new_total       = $current_total + (0.1 * $package->money);
				$current_bonus->updateAttributes([
					'current_cash'    => $new_cash,
					'current_trading' => $new_trading,
					'total_bonus'     => $new_total,
					'direct_bonus'    => $new_direct,
				]);
			} else {
				$current_bonus          = new CurrentBonus();
				$current_bonus->user_id = $sponsor->id;
				if($current_bonus->save()) {
					$current_cash    = $current_bonus->current_cash;
					$current_trading = $current_bonus->current_trading;
					$current_total   = $current_bonus->total_bonus;
					$current_direct  = $current_bonus->direct_bonus;
					$new_direct      = $current_direct + (0.1 * $package->money);
					$new_cash        = $current_cash + (0.06 * $package->money);
					$new_trading     = $current_trading + (0.04 * $package->money);
					$new_total       = $current_total + (0.1 * $package->money);
					$current_bonus->updateAttributes([
						'current_cash'    => $new_cash,
						'current_trading' => $new_trading,
						'total_bonus'     => $new_total,
						'direct_bonus'    => $new_direct,
					]);
				} else {
					echo '<pre>';
					print_r($current_bonus->errors);
					die;
				};
			}
		}
	}

	public function updateDirectManual($user_id) {
		$user    = User::findOne($user_id);
		$sponsor = $user->sponsors;
		$package = $this;
		if($sponsor) {
			if($sponsor->currentBonus) {
				$current_bonus   = $sponsor->currentBonus;
				$current_cash    = $current_bonus->current_cash;
				$current_trading = $current_bonus->current_trading;
				$current_direct  = $current_bonus->direct_bonus;
				$current_total   = $current_bonus->total_bonus;
				$new_direct      = $current_direct + (0.1 * $package->money);
				$new_cash        = $current_cash + (0.06 * $package->money);
				$new_trading     = $current_trading + (0.04 * $package->money);
				$new_total       = $current_total + (0.1 * $package->money);
				$current_bonus->updateAttributes([
					'current_cash'    => $new_cash,
					'current_trading' => $new_trading,
					'total_bonus'     => $new_total,
					'direct_bonus'    => $new_direct,
				]);
			} else {
				$current_bonus          = new CurrentBonus();
				$current_bonus->user_id = $sponsor->id;
				if($current_bonus->save()) {
					$current_cash    = $current_bonus->current_cash;
					$current_trading = $current_bonus->current_trading;
					$current_total   = $current_bonus->total_bonus;
					$current_direct  = $current_bonus->direct_bonus;
					$new_direct      = $current_direct + (0.1 * $package->money);
					$new_cash        = $current_cash + (0.06 * $package->money);
					$new_trading     = $current_trading + (0.04 * $package->money);
					$new_total       = $current_total + (0.1 * $package->money);
					$current_bonus->updateAttributes([
						'current_cash'    => $new_cash,
						'current_trading' => $new_trading,
						'total_bonus'     => $new_total,
						'direct_bonus'    => $new_direct,
					]);
				} else {
					echo '<pre>';
					print_r($current_bonus->errors);
					die;
				};
			}
		}
	}

	public function updateTeamBonus() {
		$array_parent = $this->getTotalParent($this->user->id);
		$position     = $this->user->position;
		$package      = Package::findOne($this->id);
		$bv           = $package->money;
		if($position == User::POSITION_LEFT) {
			foreach($array_parent as $key => $parent) {
				$current_bonus = CurrentBonus::findOne(['user_id' => $parent]);
				/**
				 *Cần thêm trường cho bảng levelcurrent để biết đâu là nhánh yếu đâu là nhánh khỏe.
				 */
				$current_level = LevelCurrent::findOne(['user_id' => $parent]);
				if(!$current_bonus) {
					$current_bonus          = new CurrentBonus();
					$current_bonus->user_id = $parent;
					$current_bonus->save();
				}
				if(!$current_level) {
					$current_level             = new LevelCurrent();
					$current_level->user_id    = $parent;
					$current_level->current_bv = 0;
					$current_level->save();
				}
				if($key == 0) {
					$current_bonus->updateAttributes([
						'current_bv_left' => $current_bonus->current_bv_left + $bv,
					]);
					$current_level->updateAttributes([
						'current_bv_left' => $current_level->current_bv_left + $bv,
					]);
				} else {
					$children = User::findOne($array_parent[$key - 1]);
					if($children) {
						if($children->position == User::POSITION_LEFT) {
							$current_bonus->updateAttributes([
								'current_bv_left' => $current_bonus->current_bv_left + $bv,
							]);
							$current_level->updateAttributes([
								'current_bv_left' => $current_level->current_bv_left + $bv,
							]);
						} else {
							$current_bonus->updateAttributes([
								'current_bv_right' => $current_bonus->current_bv_right + $bv,
							]);
							$current_level->updateAttributes([
								'current_bv_right' => $current_level->current_bv_right + $bv,
							]);
						}
					}
				}
				$team_bonus = $current_bonus->getTeamBonus($parent);
				$current_bonus->updateAttributes(['weak_branch_bonus' => $team_bonus]);
				$new_cash    = $current_bonus->current_cash + ($team_bonus * 0.6);
				$new_trading = $current_bonus->current_trading + ($team_bonus * 0.4);
				$new_total   = $current_bonus->weak_branch_bonus + $current_bonus->direct_bonus + $current_bonus->matching_bonus;
				$current_bonus->updateAttributes([
					'current_cash'    => $new_cash,
					'current_trading' => $new_trading,
					'total_bonus'     => $new_total,
				]);
				if($current_bonus->getWeakBranch($parent) == User::POSITION_LEFT) {
					$current_bonus->updateAttributes(['weak_branch' => User::POSITION_LEFT]);
					$current_bonus->updateAttributes(['weak_branch_bv' => $current_bonus->current_bv_left]);
				} else {
					$current_bonus->updateAttributes(['weak_branch' => User::POSITION_RIGHT]);
					$current_bonus->updateAttributes(['weak_branch_bv' => $current_bonus->current_bv_right]);
				}
				if($current_level->getWeakBranchMonth($parent) == User::POSITION_LEFT) {
					$current_level->updateAttributes(['current_bv' => $current_level->current_bv_left]);
				} else {
					$current_level->updateAttributes(['current_bv' => $current_level->current_bv_right]);
				}
			}
		} else {
			foreach($array_parent as $key => $parent) {
				$current_bonus = CurrentBonus::findOne(['user_id' => $parent]);
				$current_level = LevelCurrent::findOne(['user_id' => $parent]);
				if(!$current_bonus) {
					$current_bonus          = new CurrentBonus();
					$current_bonus->user_id = $parent;
					$current_bonus->save();
				}
				if(!$current_level) {
					$current_level             = new LevelCurrent();
					$current_level->user_id    = $parent;
					$current_level->current_bv = 0;
					$current_level->save();
				}
				if($key == 0) {
					$current_bonus->updateAttributes([
						'current_bv_right' => $current_bonus->current_bv_right + $bv,
					]);
					$current_level->updateAttributes([
						'current_bv_right' => $current_level->current_bv_right + $bv,
					]);
				} else {
					$children = User::findOne($array_parent[$key - 1]);
					if($children) {
						if($children->position == User::POSITION_LEFT) {
							$current_bonus->updateAttributes([
								'current_bv_left' => $current_bonus->current_bv_left + $bv,
							]);
							$current_level->updateAttributes([
								'current_bv_left' => $current_level->current_bv_left + $bv,
							]);
						} else {
							$current_bonus->updateAttributes([
								'current_bv_right' => $current_bonus->current_bv_right + $bv,
							]);
							$current_level->updateAttributes([
								'current_bv_right' => $current_level->current_bv_right + $bv,
							]);
						}
					}
				}
				$team_bonus = $current_bonus->getTeamBonus($parent);
				$current_bonus->updateAttributes(['weak_branch_bonus' => $team_bonus]);
				$new_cash    = $current_bonus->current_cash + ($team_bonus * 0.6);
				$new_trading = $current_bonus->current_trading + ($team_bonus * 0.4);
				$new_total   = $current_bonus->total_bonus + $team_bonus;
				$current_bonus->updateAttributes([
					'current_cash'    => $new_cash,
					'current_trading' => $new_trading,
					'total_bonus'     => $new_total,
				]);
				if($current_bonus->getWeakBranch($parent) == User::POSITION_LEFT) {
					$current_bonus->updateAttributes(['weak_branch' => User::POSITION_LEFT]);
					$current_bonus->updateAttributes(['weak_branch_bv' => $current_bonus->current_bv_left]);
				} else {
					$current_bonus->updateAttributes(['weak_branch' => User::POSITION_RIGHT]);
					$current_bonus->updateAttributes(['weak_branch_bv' => $current_bonus->current_bv_right]);
				}
				if($current_level->getWeakBranchMonth($parent) == User::POSITION_LEFT) {
					$current_bonus->updateAttributes(['weak_branch' => User::POSITION_LEFT]);
					$current_level->updateAttributes(['current_bv' => $current_level->current_bv_left]);
				} else {
					$current_bonus->updateAttributes(['weak_branch' => User::POSITION_RIGHT]);
					$current_level->updateAttributes(['current_bv' => $current_level->current_bv_right]);
				}
			}
		}
	}

	public function updateTeamManual($user_id) {
		$array_parent = $this->getTotalParent($user_id);
		$user         = User::findOne($user_id);
		$position     = $user->position;
		$package      = Package::findOne($this->id);
		$bv           = $package->money;
		if($position == User::POSITION_LEFT) {
			foreach($array_parent as $key => $parent) {
				$current_bonus = CurrentBonus::findOne(['user_id' => $parent]);
				/**
				 *Cần thêm trường cho bảng levelcurrent để biết đâu là nhánh yếu đâu là nhánh khỏe.
				 */
				$current_level = LevelCurrent::findOne(['user_id' => $parent]);
				if(!$current_bonus) {
					$current_bonus          = new CurrentBonus();
					$current_bonus->user_id = $parent;
					$current_bonus->save();
				}
				if(!$current_level) {
					$current_level             = new LevelCurrent();
					$current_level->user_id    = $parent;
					$current_level->current_bv = 0;
					$current_level->save();
				}
				if($key == 0) {
					$current_bonus->updateAttributes([
						'current_bv_left' => $current_bonus->current_bv_left + $bv,
					]);
					$current_level->updateAttributes([
						'current_bv_left' => $current_level->current_bv_left + $bv,
					]);
				} else {
					$children = User::findOne($array_parent[$key - 1]);
					if($children) {
						if($children->position == User::POSITION_LEFT) {
							$current_bonus->updateAttributes([
								'current_bv_left' => $current_bonus->current_bv_left + $bv,
							]);
							$current_level->updateAttributes([
								'current_bv_left' => $current_level->current_bv_left + $bv,
							]);
						} else {
							$current_bonus->updateAttributes([
								'current_bv_right' => $current_bonus->current_bv_right + $bv,
							]);
							$current_level->updateAttributes([
								'current_bv_right' => $current_level->current_bv_right + $bv,
							]);
						}
					}
				}
				$team_bonus = $current_bonus->getTeamBonus($parent);
				$current_bonus->updateAttributes(['weak_branch_bonus' => $team_bonus]);
				$new_cash    = $current_bonus->current_cash + ($team_bonus * 0.6);
				$new_trading = $current_bonus->current_trading + ($team_bonus * 0.4);
				$new_total   = $current_bonus->total_bonus + $team_bonus;
				$current_bonus->updateAttributes([
					'current_cash'    => $new_cash,
					'current_trading' => $new_trading,
					'total_bonus'     => $new_total,
				]);
				if($current_bonus->getWeakBranch($parent) == User::POSITION_LEFT) {
					$current_bonus->updateAttributes(['weak_branch' => User::POSITION_LEFT]);
					$current_bonus->updateAttributes(['weak_branch_bv' => $current_bonus->current_bv_left]);
				} else {
					$current_bonus->updateAttributes(['weak_branch' => User::POSITION_RIGHT]);
					$current_bonus->updateAttributes(['weak_branch_bv' => $current_bonus->current_bv_right]);
				}
				if($current_level->getWeakBranchMonth($parent) == User::POSITION_LEFT) {
					$current_level->updateAttributes(['current_bv' => $current_level->current_bv_left]);
				} else {
					$current_level->updateAttributes(['current_bv' => $current_level->current_bv_right]);
				}
			}
		} else {
			foreach($array_parent as $key => $parent) {
				$current_bonus = CurrentBonus::findOne(['user_id' => $parent]);
				$current_level = LevelCurrent::findOne(['user_id' => $parent]);
				if(!$current_bonus) {
					$current_bonus          = new CurrentBonus();
					$current_bonus->user_id = $parent;
					$current_bonus->save();
				}
				if(!$current_level) {
					$current_level             = new LevelCurrent();
					$current_level->user_id    = $parent;
					$current_level->current_bv = 0;
					$current_level->save();
				}
				if($key == 0) {
					$current_bonus->updateAttributes([
						'current_bv_right' => $current_bonus->current_bv_right + $bv,
					]);
					$current_level->updateAttributes([
						'current_bv_right' => $current_level->current_bv_right + $bv,
					]);
				} else {
					$children = User::findOne($array_parent[$key - 1]);
					if($children) {
						if($children->position == User::POSITION_LEFT) {
							$current_bonus->updateAttributes([
								'current_bv_left' => $current_bonus->current_bv_left + $bv,
							]);
							$current_level->updateAttributes([
								'current_bv_left' => $current_level->current_bv_left + $bv,
							]);
						} else {
							$current_bonus->updateAttributes([
								'current_bv_right' => $current_bonus->current_bv_right + $bv,
							]);
							$current_level->updateAttributes([
								'current_bv_right' => $current_level->current_bv_right + $bv,
							]);
						}
					}
				}
				$team_bonus = $current_bonus->getTeamBonus($parent);
				$current_bonus->updateAttributes(['weak_branch_bonus' => $team_bonus]);
				$new_cash    = $current_bonus->current_cash + ($team_bonus * 0.6);
				$new_trading = $current_bonus->current_trading + ($team_bonus * 0.4);
				$new_total   = $current_bonus->total_bonus + $team_bonus;
				$current_bonus->updateAttributes([
					'current_cash'    => $new_cash,
					'current_trading' => $new_trading,
					'total_bonus'     => $new_total,
				]);
				if($current_bonus->getWeakBranch($parent) == User::POSITION_LEFT) {
					$current_bonus->updateAttributes(['weak_branch' => User::POSITION_LEFT]);
					$current_bonus->updateAttributes(['weak_branch_bv' => $current_bonus->current_bv_left]);
				} else {
					$current_bonus->updateAttributes(['weak_branch' => User::POSITION_RIGHT]);
					$current_bonus->updateAttributes(['weak_branch_bv' => $current_bonus->current_bv_right]);
				}
				if($current_level->getWeakBranchMonth($parent) == User::POSITION_LEFT) {
					$current_bonus->updateAttributes(['weak_branch' => User::POSITION_LEFT]);
					$current_level->updateAttributes(['current_bv' => $current_level->current_bv_left]);
				} else {
					$current_bonus->updateAttributes(['weak_branch' => User::POSITION_RIGHT]);
					$current_level->updateAttributes(['current_bv' => $current_level->current_bv_right]);
				}
			}
		}
	}
}
