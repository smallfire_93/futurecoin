<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "package_transaction".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $package_id
 * @property integer $type
 * @property integer $status
 * @property string  $created_date
 * @property string  $start_time
 * @property string  $end_time
 *
 * @property Package $package
 * @property User    $users
 */
class PackageTransaction extends \app\components\Model {

	const  TYPE           = [
		'Perfect money',
		'Bitcoin',
		'Cash account',
	];

	const  TYPE_BITCOIN   = 1;

	const  TYPE_CASH      = 2;

	const  STATUS         = [
		1 => 'Success',
		2 => 'Pending',
	];

	const  STATUS_PENDING = 2;

	const  STATUS_SUCCESS = 1;

	public $branch;

	public $tree_position;

	public $start_time;

	public $end_time;

	public $available_date;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'package_transaction';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				['user_id'],
				'required',
			],
			[
				[
					'user_id',
					'package_id',
					'type',
					'status',
				],
				'integer',
			],
			[
				[
					'created_date',
					'branch',
					'tree_position',
					'start_time',
					'end_time',
					'available_date',
				],
				'safe',
			],
			[
				['package_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => Package::className(),
				'targetAttribute' => ['package_id' => 'id'],
			],
			[
				['user_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => User::className(),
				'targetAttribute' => ['user_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'             => 'ID',
			'user_id'        => 'User',
			'package_id'     => 'Package',
			'type'           => 'Type',
			'status'         => 'Status',
			'created_date'   => 'Created Date',
			'available_date' => 'Can use your tokens from',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPackage() {
		return $this->hasOne(Package::className(), ['id' => 'package_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUsers() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	public function check_ajax_payment() {
		$payment = CryptoPayments::findOne(['orderID' => $this->orderID]);
		if($payment) {
			return true;
		} else {
			return false;
		}
	}

	public function returnBranch() {
		if(in_array($this->user_id, $this->user->getAllRight())) {
			$branch = 'Right';
		} else {
			$branch = 'Left';
		}
		return $branch;
	}
}
