<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "start_setting".
 *
 * @property int    $id
 * @property string $web_start
 * @property double $coin_price
 * @property double $token_price
 * @property double $token_split
 * @property double $split_percent
 */
class StartSetting extends \yii\db\ActiveRecord {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'start_setting';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				['web_start'],
				'safe',
			],
			[
				[
					'coin_price',
					'token_price',
					'token_split',
					'split_percent',
				],
				'number',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'            => 'ID',
			'web_start'     => 'Web Start',
			'coin_price'    => 'Coin Price',
			'token_price'   => 'Token Price',
			'token_split'   => 'Token Split',
			'split_percent' => 'Split percent',
		];
	}
}
