<?php

namespace app\models;

use app\components\Model;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "support".
 *
 * @property int    $id
 * @property int    $user_id
 * @property int    $parent_id
 * @property int    $status
 * @property int    $flag
 * @property string $title
 * @property string $content
 */
class Support extends Model {

	const STATUS        = [
		'Unread',
		'Read',
	];

	const FLAG          = [
		'Chưa trả lời',
		'Đã trả lời',
	];

	const STATUS_UNREAD = 0;

	const STATUS_READ   = 1;

	const FLAG_REPLIED  = 1;

	const FLAG_NREPLIED = 0;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'support';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'user_id',
					'parent_id',
					'status',
					'flag',
				],
				'integer',
			],
			[
				['content'],
				'string',
			],
			[
				['title'],
				'string',
				'max' => 255,
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'      => 'ID',
			'user_id' => 'User',
			'title'   => 'Title',
			'content' => 'Content',
		];
	}

	public function getUsers() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}

	public function getFilterUser() {
		return ArrayHelper::map(User::find()->all(), 'id', 'username');
	}
}
