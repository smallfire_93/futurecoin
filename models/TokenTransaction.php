<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "token_transaction".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $type
 * @property string  $date
 * @property double  $quantity
 * @property double  $price
 * @property double  $money
 * @property integer $status
 *
 * @property User    $user
 */
class TokenTransaction extends \app\components\Model {

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'token_transaction';
	}

	const TYPE       = [
		'Buy',
		'Buy Package',
		'SELL',
		'Mining',
	];

	const TYPE_PAC   = 1;

	const TYPE_BUY   = 0;

	const TYPE_SELL  = 2;

	const TYPE_MINE  = 3;

	const STATUS     = [
		'Pending',
		'Success',
	];

	const STATUS_PEN = 0;

	const STATUS_SUC = 1;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'user_id',
					'type',
					'status',
				],
				'integer',
			],
			[
				['date'],
				'safe',
			],
			[
				[
					'quantity',
					'price',
					'money',
				],
				'number',
			],
			[
				['user_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => User::className(),
				'targetAttribute' => ['user_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'       => 'ID',
			'user_id'  => 'User ID',
			'type'     => 'Type',
			'date'     => 'Date',
			'quantity' => 'Quantity',
			'price'    => 'Price',
			'money'    => 'Money',
			'status'   => 'Status',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}
}
