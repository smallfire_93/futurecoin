<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "trading_transaction".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $type
 * @property double  $money
 * @property integer $status
 * @property integer $quantity
 * @property string  $date
 *
 * @property User    $user
 */
class TradingTransaction extends \app\components\Model {

	const TYPE           = [
		1 => 'Bonus Startup',
		2 => 'Bonus',
		3 => 'Buy Trading',
		4 => 'Mining coin',
	];

	const TYPE_STA       = 1;

	const TYPE_BON       = 2;

	const TYPE_BUY       = 3;

	const TYPE_MINE      = 3;

	const STATUS         = [
		'Pending',
		'Success',
		'Error',
	];

	const STATUS_SUCCESS = 1;

	const STATUS_PENDING = 0;

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'trading_transaction';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'user_id',
					'type',
					'status',
					'quantity',
				],
				'integer',
			],
			[
				['money'],
				'number',
			],
			[
				['date'],
				'safe',
			],
			[
				['user_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => User::className(),
				'targetAttribute' => ['user_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'       => 'ID',
			'user_id'  => 'User ID',
			'type'     => 'Type',
			'money'    => 'Money',
			'status'   => 'Status',
			'quantity' => 'Quantity',
			'date'     => 'Date',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}
}
