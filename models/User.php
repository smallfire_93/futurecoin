<?php

namespace app\models;

use app\components\Model;
use app\models\form\SignUpForm;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer      $id
 * @property integer      $parent_id
 * @property integer      $sponsor_id
 * @property integer      $level
 * @property integer      $role
 * @property string       $username
 * @property string       $password
 * @property string       $password_2
 * @property string       $image
 * @property string       $full_name
 * @property string       $phone
 * @property string       $email
 * @property integer      $status
 * @property integer      $gender
 * @property integer      $position
 * @property string       $block_note
 * @property string       $bit_coin_address
 * @property string       $bank_name
 * @property string       $bank_branch_name
 * @property string       $bank_account_holder
 * @property string       $bank_account_number
 * @property string       $swift_code
 * @property string       $address
 * @property string       $id_number
 * @property string       $country
 * @property string       $city
 * @property string       $expire_date
 * @property string       $reset_key
 * @property integer      $blocked_at
 * @property integer      $zip_code
 * @property integer      $last_login_at
 * @property integer      $last_activated
 * @property integer      $created_at
 * @property integer      $updated_at
 * @property string       $active_at
 *
 * @property Account      $accounts
 * @property CurrentBonus $currentBonus
 * @property LevelCurrent $currentLevel
 * @property User         $sponsors
 * @property User         $leftUser
 * @property UserKyc      $kyc
 * @property User         $rightUser
 * @property User[]       $users
 */
class User extends Model implements IdentityInterface {

	const ROLE = [
		'Member',
		'Staff',
		'Administrator',
	];

	public $package;

	/**
	 * Cấp độ thành viên, bị ảnh hưởng bởi điều kiện trong setting
	 */
	const ROLE_MEMBER = 0;

	/**
	 * Cấp độ admin thường
	 */
	const ROLE_STAFF = 1;

	/**
	 * Cấp độ admin, full lv
	 */
	const ROLE_ADMIN = 2;

	const STATUS     = [
		'Deactivated',
		'Activated',
		'Blocked',
	];

	const STATUS_VN  = [
		'Chưa kích hoạt',
		'Đã kích hoạt',
		'Đã khóa',
	];

	/**
	 * Tài khoản đã khóa
	 */
	const STATUS_BLOCKED = 2;

	/**
	 * Tài khoản chưa kích hoạt
	 */
	const STATUS_DEACTIVATED = 0;

	/**
	 * Tài khoản đã kích hoạt
	 */
	const STATUS_ACTIVATED = 1;

	const STATUS_REGISTER  = 3;

	public $sponsor;

	/**
	 * @var boolean
	 */
	public $isBlocked;

	const  POSITION_LEFT  = 0;

	const  POSITION_RIGHT = 1;

	/**
	 * @var boolean
	 */
	public $isActivated;

	const  POSITIONS = [
		'Left',
		'Right',
	];

	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'user';
	}

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'parent_id',
					'sponsor_id',
					'level',
					'role',
					'status',
					'gender',
					'position',
					'blocked_at',
					'last_login_at',
					'last_activated',
					'created_at',
					'updated_at',
					'zip_code',
				],
				'integer',
			],
			[
				[
					'username',
					'password',
					'password_2',
					'full_name',
					'phone',
					'email',
				],
				'required',
			],
			[
				['username'],
				'string',
				'max' => 64,
			],
			[
				['reset_key'],
				'string',
				'max' => 255,
			],
			[
				[
					'password',
					'password_2',
					'full_name',
					'block_note',
					'bit_coin_address',
					'bank_name',
					'bank_branch_name',
					'bank_account_holder',
					'bank_account_number',
					'country',
					'id_number',
					'address',
					'swift_code',
					'city',
					'expire_date',
					'package',
				],
				'string',
				'max' => 255,
			],
			[
				['image'],
				'string',
				'max' => 225,
			],
			[
				['phone'],
				'string',
				'max' => 16,
			],
			[
				['email'],
				'string',
				'max' => 128,
			],
			[
				['username'],
				'unique',
			],
			//			[
			//				['email'],
			//				'unique',
			//			],
			[
				['total_bonus'],
				'safe',
			],
			[
				['sponsor_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => User::className(),
				'targetAttribute' => ['sponsor_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'                  => 'ID',
			'parent_id'           => 'Parent ID',
			'sponsor_id'          => 'Sponsor ID',
			'level'               => 'Level',
			'role'                => 'Role',
			'username'            => 'Username',
			'password'            => 'Password',
			'password_2'          => 'Password 2',
			'image'               => 'Image',
			'full_name'           => 'Full Name',
			'phone'               => 'Phone',
			'email'               => 'Email',
			'status'              => 'Status',
			'gender'              => 'Gender',
			'position'            => 'Position',
			'block_note'          => 'Block Note',
			'blocked_at'          => 'Blocked At',
			'last_login_at'       => 'Last Login At',
			'last_activated'      => 'Last Activated',
			'created_at'          => 'Created At',
			'updated_at'          => 'Updated At',
			'bit_coin_address'    => 'BitCoin Address',
			'bank_name'           => 'Bank Name',
			'bank_branch_name'    => 'Bank Branch Name',
			'bank_account_holder' => 'Bank Account holder',
			'bank_account_number' => 'Bank Account Number',
			'id_number'           => 'ID number',
			'address'             => 'Address',
			'country'             => 'Country',
			'swift_code'          => 'Swift code',
			'zip_code'            => 'Zip code',
			'city'                => 'City',
			'expire_date'         => 'Expired date',
		];
	}

	/**
	 * @return ActiveQuery
	 */
	public function getAccounts() {
		return $this->hasOne(Account::className(), ['user_id' => 'id']);
	}

	public function getPackage() {
		return $this->hasOne(Package::className(), ['id' => 'package_id'])->viaTable('account', ['user_id' => 'id']);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getCurrentBonus() {
		return $this->hasOne(CurrentBonus::className(), ['user_id' => 'id']);
	}

	public function getCurrentLevel() {
		return $this->hasOne(LevelCurrent::className(), ['user_id' => 'id']);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getSponsors() {
		return $this->hasOne(User::className(), ['id' => 'sponsor_id']);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getUsers() {
		return $this->hasMany(User::className(), ['sponsor_id' => 'id']);
	}

	/**
	 * Finds an identity by the given ID.
	 *
	 * @param string|int $id the ID to be looked for
	 *
	 * @return IdentityInterface the identity object that matches the given ID.
	 * Null should be returned if such an identity cannot be found
	 * or the identity is not in an active state (disabled, deleted, etc.)
	 */
	public static function findIdentity($id) {
		return self::findOne($id);
	}

	/**
	 * Finds an identity by the given token.
	 *
	 * @param mixed $token the token to be looked for
	 * @param mixed $type  the type of the token. The value of this parameter depends on the implementation.
	 *                     For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be
	 *                     `yii\filters\auth\HttpBearerAuth`.
	 *
	 * @return IdentityInterface the identity object that matches the given token.
	 * Null should be returned if such an identity cannot be found
	 * or the identity is not in an active state (disabled, deleted, etc.)
	 */
	public static function findIdentityByAccessToken($token, $type = null) {
		// TODO: Implement findIdentityByAccessToken() method.
	}

	/**
	 * Returns an ID that can uniquely identify a user identity.
	 * @return string|int an ID that uniquely identifies a user identity.
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Returns a key that can be used to check the validity of a given identity ID.
	 *
	 * The key should be unique for each individual user, and should be persistent
	 * so that it can be used to check the validity of the user identity.
	 *
	 * The space of such keys should be big enough to defeat potential identity attacks.
	 *
	 * This is required if [[User::enableAutoLogin]] is enabled.
	 * @return string a key that is used to check the validity of a given identity ID.
	 * @see validateAuthKey()
	 */
	public function getAuthKey() {
		// TODO: Implement getAuthKey() method.
	}

	/**
	 * Validates the given auth key.
	 *
	 * This is required if [[User::enableAutoLogin]] is enabled.
	 *
	 * @param string $authKey the given auth key
	 *
	 * @return bool whether the given auth key is valid.
	 * @see getAuthKey()
	 */
	public function validateAuthKey($authKey) {
		// TODO: Implement validateAuthKey() method.
	}

	/**
	 * @return ActiveQuery
	 */
	public function getRightUser() {
		return $this->hasOne(User::className(), ['parent_id' => 'id'])->andWhere(['position' => self::POSITION_RIGHT]);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getLeftUser() {
		return $this->hasOne(User::className(), ['parent_id' => 'id'])->andWhere(['position' => self::POSITION_LEFT]);
	}

	/**
	 * get direct right child
	 * @return ActiveQuery
	 */
	public function getRightChild() {
		return $this->hasOne(User::className(), ['sponsor_id' => 'id'])->andWhere(['position' => self::POSITION_RIGHT]);
	}

	/**
	 * get direct left child
	 * @return ActiveQuery
	 */
	public function getLeftChild() {
		return $this->hasOne(User::className(), ['sponsor_id' => 'id'])->andWhere(['position' => self::POSITION_LEFT]);
	}

	public function getKyc() {
		return $this->hasOne(UserKyc::className(), ['user_id' => 'id']);
	}

	/**
	 * Tìm user theo tên đăng nhập
	 *
	 * @param $username
	 *
	 * @return static
	 */
	public static function findByUsername($username) {
		return self::findOne(['username' => $username]);
	}

	/**
	 * @param $password
	 *
	 * @return bool
	 */
	public function validatePassword($password) {
		return ($this->password) ? Yii::$app->security->validatePassword($password, $this->password) : true;
	}

	/**
	 * @param $id
	 *
	 * @return array
	 */
	public function getTotal($id) {
		/**@var self[] $lv1s */
		$array_children = [];
		$lv1s           = User::find()->where([
			'parent_id' => $id,
		])->all();
		foreach($lv1s as $lv1) {
			$array_children[] = $lv1->id;
		}
		return $array_children;
	}

	/**
	 * @param       $parent_id
	 * @param array $array_children
	 *
	 * @return array
	 */
	public function getTotalChildren($parent_id, $array_children = []) {
		$tree = $this->getTotal($parent_id);
		if(count($tree) > 0 && is_array($tree)) {
			$array_children = ArrayHelper::merge($array_children, $tree);
		}
		foreach($tree as $item => $id) {
			$array_children = $this->getTotalChildren($id, $array_children);
		}
		return $array_children;
	}

	public function getPositionLeft() {
		$sign      = new SignUpForm();
		$last_left = $sign->getLastLeft($this->user);
		$parent    = $this::findOne(['id' => $last_left->parent_id]);
		if($parent && $parent->id != $this->user->parent_id) {
			//			for($i = 1; $i < 2; $i ++) {
			$parent = $this::findOne(['id' => $parent->id]);
			if(!$parent) {
				$parent = null;
				//					break;
			}
			//			}
		} else {
			$parent = null;
		}
		return $parent;
	}

	/**
	 * Lấy vị trí bên phải trên cây nhị phân
	 */
	public function getPositionRight() {
		$sign       = new SignUpForm();
		$last_right = $sign->getLastRight($this->user);
		$parent     = $this::findOne(['id' => $last_right->parent_id]);
		if($parent && $parent->id != $this->user->parent_id) {
			//			for($i = 1; $i < 2; $i ++) {
			$parent = $this::findOne(['id' => $parent->id]);
			if(!$parent) {
				$parent = null;
				//					break;
			}
			//			}
		} else {
			$parent = null;
		}
		return $parent;
	}

	/**
	 *Reset nhánh yếu
	 */
	public function resetBranch() {
		$current_bonus = $this->currentBonus;
		$current_bonus->updateAttributes([
			'current_bv_right' => ($current_bonus->current_bv_right - $current_bonus->weak_branch_bv),
			'current_bv_left'  => $current_bonus->current_bv_left - $current_bonus->weak_branch_bv,
		]);
		return $current_bonus->updateAttributes(['weak_branch_bonus' => 0]);
	}

	/**
	 *Trả về tất cả id nhánh phải
	 */
	public function getAllRight() {
		$right_child    = User::find()->where(['parent_id' => $this->id])->andWhere(['position' => $this::POSITION_RIGHT])->one();
		$children_array = [];
		if($right_child) {
			$children_array = ArrayHelper::merge([$right_child->id], $this->getTotalChildren($right_child->id));
		}
		return $children_array;
	}

	/**
	 *Trả về tất cả id nhánh phải
	 */
	public function getAllLeft() {
		$left_child     = User::find()->where(['parent_id' => $this->id])->andWhere(['position' => $this::POSITION_LEFT])->one();
		$children_array = [];
		if($left_child) {
			$children_array = ArrayHelper::merge([$left_child->id], $this->getTotalChildren($left_child->id));
		}
		return $children_array;
	}

	public function getTreePosition($id) {
		if(in_array($id, $this->getTotalDownline($this->id))) {
			$current_user = User::findOne($id);
			$position     = 0;
			if($current_user) {
				$parent   = $current_user->sponsor_id;
				$position = 1;
				while($parent != $this->id) {
					$current_user = User::findOne($parent);
					if($current_user) {
						$parent = $current_user->sponsor_id;
						if($parent != null) {
							$position ++;
						} else {
							$position = 0;
						}
					} else {
						$position = 0;
					}
				}
			}
			return 'Downline level' . $position;
		} else {
			$position = 'Downline';
			return $position;
		}
	}
}
