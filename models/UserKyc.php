<?php
namespace app\models;

use Yii;

/**
 * This is the model class for table "user_kyc".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string  $id_frond
 * @property string  $id_back
 * @property string  $address_proof
 * @property string  $address_proof2
 * @property integer $kyc_status
 *
 * @property User    $users
 */
class UserKyc extends \app\components\Model {

	/**
	 * @inheritdoc
	 */
	const STATUS      = [
		'Not KYC',
		'KYC pending',
		'KYC accepted',
		'KYC rejected',
	];

	const NOT_KYC     = 0;

	const KYC_PENDING = 1;

	const KYC_ACCEPT  = 2;

	const KYC_REJECT  = 3;

	public static function tableName() {
		return 'user_kyc';
	}

	public $my_frond;

	public $my_back;

	public $my_address;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'user_id',
					'kyc_status',
				],
				'integer',
			],
			[
				[
					'id_frond',
					'id_back',
					'address_proof',
					'address_proof2',
				],
				'string',
				'max' => 255,
			],
			[
				[
					'my_frond',
					'my_back',
					'my_address',
				],
				'file',
				'extensions' => 'png,jpg,gif',
				//				'skipOnEmpty' => false,
			],
			//			[
			//				[
			//					'my_frond',
			//					'my_back',
			//				],
			//				'required',
			//			],
			[
				['user_id'],
				'exist',
				'skipOnError'     => true,
				'targetClass'     => User::className(),
				'targetAttribute' => ['user_id' => 'id'],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'id'             => 'ID',
			'user_id'        => 'User ID',
			'id_frond'       => 'Id Frond',
			'id_back'        => 'Id Back',
			'address_proof'  => 'Address Proof',
			'address_proof2' => 'Address Proof2',
			'kyc_status'     => 'Kyc Status',
			'my_frond'       => 'Frond',
			'my_back'        => 'Back',
			'my_address'     => 'Address document',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUsers() {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}
}
