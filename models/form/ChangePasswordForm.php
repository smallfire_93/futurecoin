<?php

namespace app\models\form;

use app\components\Form;
use Yii;

/**
 * PinTransferForm form
 */
class ChangePasswordForm extends Form {


	public $new_password;

	public $new_password_confirm;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'new_password',
					'new_password_confirm',
				],
				'required',
			],
			[
				'new_password_confirm',
				'compare',
				'compareAttribute' => 'new_password',
				'message'          => "Password confirmation dose not match.",
			],
//			[
//				'current_password',
//				'validateCurrentPassword',
//			],
		];
	}

	/**
	 * Verify T Password
	 */
//	public function validateCurrentPassword() {
//		$validate = Yii::$app->security->validatePassword($this->current_password, $this->user->password);
//		if(!$validate) {
//			$this->addError("current_password", "Current password  incorrect");
//		}
//	}

	public function changePass() {
		if($this->validate()) {
			$this->user->updateAttributes(['password' => Yii::$app->security->generatePasswordHash($this->new_password)]);
			return true;
		} else {
			return false;
		}
	}
}
