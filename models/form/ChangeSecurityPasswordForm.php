<?php
/**
 * Created by Navatech.
 * @project enterfund
 * @author  LocPX
 * @email   loc.xuanphama1t1[at]gmail.com
 * @date    10/18/2016
 * @time    2:34 PM
 */
namespace app\models\form;

use app\components\Form;
use Yii;

class ChangeSecurityPasswordForm extends Form {

	public $current_password;

	public $new_password;

	public $new_password_confirm;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'current_password',
					'new_password',
					'new_password_confirm',
				],
				'required',
			],
			[
				'new_password_confirm',
				'compare',
				'compareAttribute' => 'new_password',
				'message'          => "Password confirmation dose not match.",
			],
			[
				'current_password',
				'validateCurrentPassword',
			],
		];
	}

	/**
	 * Verify T Password
	 */
	public function validateCurrentPassword() {
		$validate = Yii::$app->security->validatePassword($this->current_password, $this->user->password);
		if(!$validate) {
			$this->addError("current_password", "Current password  incorrect");
		}
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'current_password'     => 'Current password 1',
			'new_password'         => 'New password 2',
			'new_password_confirm' => 'New password 2 confirm',
		];
	}
	public function changePass() {
		if($this->validate()) {
			$this->user->updateAttributes(['password_2' => Yii::$app->security->generatePasswordHash($this->new_password)]);
			return true;
		} else {
			return false;
		}
	}
}