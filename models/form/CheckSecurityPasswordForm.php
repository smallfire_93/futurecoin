<?php
/**
 * Created by PhpStorm.
 * User: Yamon-PC
 * Date: 28-Mar-17
 * Time: 7:06 PM
 */
namespace app\models\form;

use app\components\Form;
use app\models\Package;
use Yii;

class CheckSecurityPasswordForm extends Form {

	public $t_password;

	public function rules() {
		return [
			[
				't_password',
				'validateCurrentPassword',
			],
			[
				't_password',
				'validateAccount',
			],
			[
				['t_password',],
				'required',
			],
		];
	}

	public function validateCurrentPassword($attribute) {
		$validate = Yii::$app->security->validatePassword($this->t_password, $this->user->password_2);
		if(!$validate) {
			$this->addError($attribute, "Security password incorrect");
		}
	}

	public function validateAccount($attribute) {
		if(isset($_GET['id'])) {
			$package = Package::findOne($_GET['id']);
			if($package) {
				if($this->user->accounts->package_id == null) {
					if($this->user->accounts->cash_account < $package->money + $package::FEE) {
						$this->addError('t_password', 'Your cash account is not enough money');
					}
				} else {
					if($this->user->accounts->cash_account < $package->money) {
						$this->addError($attribute, 'Your cash account is not enough money');
					}
				}
			}
		}
	}

	public function attributeLabels() {
		return ['t_password' => 'Security password'];
	}
}