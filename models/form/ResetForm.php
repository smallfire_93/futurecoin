<?php
namespace app\models\form;

use app\components\Form;
use app\models\User;
use Yii;

/**
 * PinTransferForm form
 */
class ResetForm extends Form {

	public $email;

	public $username;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'email',
					'username',
				],
				'required',
			],
			[
				'email',
				'validateEmail',
			],
		];
	}

	/**
	 * Verify T Password
	 */
	public function validateEmail() {
		$user = User::findOne([
			'email'    => $this->email,
			'username' => $this->username,
		]);
		if(!$user) {
			$this->addError("username", "User is incorrect");
		} else {
			$user->updateAttributes(['reset_key' => $user->username . rand(1000, 9999)]);
		}
	}

	//	public function changePass() {
	//		if($this->validate()) {
	//			$this->user->updateAttributes(['password' => Yii::$app->security->generatePasswordHash($this->new_password)]);
	//			return true;
	//		} else {
	//			return false;
	//		}
	//	}
}
