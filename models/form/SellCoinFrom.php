<?php
/**
 * Created by PhpStorm.
 * User: Yamon-PC
 * Date: 07-Jun-17
 * Time: 7:29 PM
 */
namespace app\models\form;
use app\components\Form;
use Yii;

class SellCoinFrom extends Form {
	public $quantity;

	public $t_password;

	public $cash;

	public $price;

	public function rules() {
		return [
			[
				't_password',
				'validateCurrentPassword',
			],
			[
				'quantity',
				'number',
				'min' => 1,
			],
			[
				['cash'],
				'validateQuantity',
			],
			[
				[
					't_password',
					'quantity',
				],
				'required',
			],
		];
	}

	public function validateCurrentPassword($attribute) {
		$validate = Yii::$app->security->validatePassword($this->t_password, $this->user->password_2);
		if(!$validate) {
			$this->addError($attribute, "Security password incorrect");
		}
	}

	public function validateQuantity($attribute) {
		$trading = $this->user->accounts->cash_account;
		if($attribute > $trading) {
			$this->addError($attribute, 'Your cash is not enough');
		}
	}
}