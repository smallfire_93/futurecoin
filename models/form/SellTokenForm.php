<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 5/14/2017
 * Time: 5:02 PM
 */

namespace app\models\form;

use app\components\Form;
use app\models\CashTransaction;
use app\models\Package;
use app\models\PackageTransaction;
use app\models\StartSetting;
use app\models\TokenTransaction;
use Yii;

class SellTokenForm extends Form {

	public $t_password;

	public $quantity;

	public function rules() {
		return [
			[
				't_password',
				'validateCurrentPassword',
			],
			[
				'quantity',
				'number',
				'min' => 1,
			],
			[
				['quantity'],
				'validateQuantity',
			],
			[
				[
					't_password',
					'quantity',
				],
				'required',
			],
		];
	}

	public function validateCurrentPassword($attribute) {
		$validate = Yii::$app->security->validatePassword($this->t_password, $this->user->password_2);
		if(!$validate) {
			$this->addError($attribute, "Security password incorrect");
		}
	}

	public static function getAvailableToken() {
		$token_sold      = TokenTransaction::find()->where([
			'user_id' => Yii::$app->user->id,
			'type'    => TokenTransaction::TYPE_SELL,
		])->andWhere([
			'>=',
			'date',
			date('Y-m-d'),
		])->sum('quantity');
		$token_sold_past = TokenTransaction::find()->where([
			'user_id' => Yii::$app->user->id,
			'type'    => TokenTransaction::TYPE_SELL,
		])->andWhere([
			'<',
			'date',
			date('Y-m-d'),
		])->sum('quantity');
		$token_sold_past = $token_sold_past ? $token_sold_past : 0;
		$token_sold      = $token_sold ? $token_sold : 0;
		$date               = strtotime('-54 days', strtotime(date('Y-m-d H:i:s')));
		$past_date          = date('Y-m-d H:i:s', $date);
		$available_packages = PackageTransaction::find()->where([
			'<=',
			'created_date',
			$past_date,
		])->andWhere([
			'user_id' => Yii::$app->user->id,
			'status'  => PackageTransaction::STATUS_SUCCESS,
		])->all();
		$available_token    = 0;
		foreach($available_packages as $available_package) {
			$package_token = Package::findOne($available_package->package_id);
			if($package_token) {
				$available_token += $package_token->token;
			}
		}
		$available_token = $available_token - $token_sold_past;
		$available_token = 0.01 * $available_token - $token_sold;
		return $available_token;
	}

	public function validateQuantity($attribute) {
		//		$token_sold         = TokenTransaction::find()->where([
		//			'user_id' => $this->user->id,
		//			'type'    => TokenTransaction::TYPE_SELL,
		//		])->andWhere([
		//			'>=',
		//			'date',
		//			date('Y-m-d H:i:s'),
		//		])->sum('quantity');
		//		$token_sold         = $token_sold ? $token_sold : 0;
		//		$date               = strtotime('-54 days', strtotime(date('Y-m-d H:i:s')));
		//		$past_date          = date('Y-m-d H:i:s', $date);
		//		$available_packages = PackageTransaction::find()->where([
		//			'<=',
		//			'created_date',
		//			$past_date,
		//		])->andWhere([
		//			'user_id' => $this->user->id,
		//			'status'  => PackageTransaction::STATUS_SUCCESS,
		//		])->all();
		//		$available_token    = 0;
		//		foreach($available_packages as $available_package) {
		//			$package_token = Package::findOne($available_package->package_id);
		//			if($package_token) {
		//				$available_token += $package_token->token;
		//			}
		//		}
		$available_token = SellTokenForm::getAvailableToken();
		if($this->quantity > $available_token) {
			$this->addError($attribute, 'Your token must lower than ' . $available_token);
		}
	}

	public function attributeLabels() {
		return [
			't_password' => 'Security password',
			'quantity'   => 'Tokens quantity',
		];
	}

	public function sellTokens() {
		$account = $this->user->accounts;
		$account->updateAttributes(['token' => $account->token - $this->quantity]);
		$account->updateAttributes(['total_tokens' => $account->total_tokens - $this->quantity]);
		$current_token = StartSetting::find()->orderBy('id DESC')->one();
		$money         = ($this->quantity - 0.05 * $this->quantity) * $current_token->token_price;
		$account->updateAttributes(['cash_account' => $account->cash_account + $money]);
		$cash_transaction          = new CashTransaction();
		$cash_transaction->user_id = $this->user->id;
		$cash_transaction->type    = $cash_transaction::TYPE_TOKEN;
		$cash_transaction->money   = $money;
		$cash_transaction->status  = $cash_transaction::STATUS_SUCCESS;
		$cash_transaction->date    = date('Y-m-d H:i:s');
		$cash_transaction->save();
		$transaction           = new TokenTransaction();
		$transaction->quantity = $this->quantity;
		$transaction->money    = $money;
		$transaction->price    = $current_token->token_price;
		$transaction->status   = $transaction::STATUS_SUC;
		$transaction->type     = $transaction::TYPE_SELL;
		$transaction->date     = date('Y-m-d H:i:s');
		$transaction->user_id  = $this->user->id;
		if($transaction->save()) {
			return true;
		} else {
			return false;
		}
	}
}