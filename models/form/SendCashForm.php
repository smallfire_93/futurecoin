<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 4/19/2017
 * Time: 10:15 AM
 */

namespace app\models\form;

use app\components\Form;
use app\models\CashTransaction;
use app\models\Package;
use app\models\User;
use Yii;

class SendCashForm extends Form {

	public $user_id;

	public $money;

	public $t_password;

	public function rules() {
		return [
			[
				't_password',
				'validateCurrentPassword',
			],
			[
				'money',
				'validateAccount',
			],
			[
				[
					'user_id',
				],
				'integer',
			],
			[
				'money',
				'integer',
				'min' => 1,
			],
			[
				[
					't_password',
					'money',
					'user_id',
				],
				'required',
			],
		];
	}

	public function validateCurrentPassword($attribute) {
		$validate = Yii::$app->security->validatePassword($this->t_password, $this->user->password_2);
		if(!$validate) {
			$this->addError($attribute, "Security password incorrect");
		}
	}

	public function validateAccount($attribute) {
		if($this->user->accounts->cash_account < $this->money) {
			$this->addError($attribute, 'Your cash account is not enough money');
		}
	}

	public function attributeLabels() {
		return [
			't_password' => 'Security password',
			'user_id'    => 'Username',
		];
	}

	public function sendUp() {
		$current_cash = $this->user->accounts->cash_account;
		$this->user->accounts->updateAttributes(['cash_account' => $current_cash - $this->money]);
		$user_receipt                 = User::findOne($this->user_id);
		$current_cash_receipt         = $user_receipt->accounts->cash_account;
		$cash_transaction             = new CashTransaction();
		$cash_transaction->money      = $this->money;
		$cash_transaction->type       = CashTransaction::TYPE_UPLINE;
		$cash_transaction->status     = CashTransaction::STATUS_SUCCESS;
		$cash_transaction->receive_id = $this->user_id;
		$cash_transaction->user_id    = $this->user->id;
		$cash_transaction->date       = date('Y-m-d H:i:s');
		if($cash_transaction->save()) {
			$cash_transaction             = new CashTransaction();
			$cash_transaction->money      = $this->money;
			$cash_transaction->type       = CashTransaction::TYPE_RECEIVE;
			$cash_transaction->status     = CashTransaction::STATUS_SUCCESS;
			$cash_transaction->receive_id = $this->user_id;
			$cash_transaction->date       = date('Y-m-d H:i:s');
			$cash_transaction->user_id    = $this->user->id;
			$cash_transaction->save();
		};
		if($user_receipt->accounts->updateAttributes(['cash_account' => $current_cash_receipt + $this->money]) != 0) {
			$status = true;
		} else {
			$status = false;
		};
		return $status;
	}

	public function sendDown() {
		$current_cash = $this->user->accounts->cash_account;
		$this->user->accounts->updateAttributes(['cash_account' => $current_cash - $this->money]);
		$user_receipt                 = User::findOne($this->user_id);
		$current_cash_receipt         = $user_receipt->accounts->cash_account;
		$cash_transaction             = new CashTransaction();
		$cash_transaction->money      = $this->money;
		$cash_transaction->type       = CashTransaction::TYPE_DOWNLINE;
		$cash_transaction->status     = CashTransaction::STATUS_SUCCESS;
		$cash_transaction->receive_id = $this->user_id;
		$cash_transaction->date       = date('Y-m-d H:i:s');
		$cash_transaction->user_id    = $this->user->id;
		if($cash_transaction->save()) {
			$cash_transaction             = new CashTransaction();
			$cash_transaction->money      = $this->money;
			$cash_transaction->type       = CashTransaction::TYPE_RECEIVE;
			$cash_transaction->status     = CashTransaction::STATUS_SUCCESS;
			$cash_transaction->receive_id = $this->user_id;
			$cash_transaction->date       = date('Y-m-d H:i:s');
			$cash_transaction->user_id    = $this->user->id;
			$cash_transaction->save();
		};
		if($user_receipt->accounts->updateAttributes(['cash_account' => $current_cash_receipt + $this->money]) != 0) {
			$status = true;
		} else {
			$status = false;
		};
		return $status;
	}
}