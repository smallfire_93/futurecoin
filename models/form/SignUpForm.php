<?php
namespace app\models\form;

use app\models\Account;
use app\models\User;
use Yii;
use yii\base\Model;

/**
 * SignUpForm
 */
class SignUpForm extends Model {

	public $sponsor;

	public $username;

	public $full_name;

	public $email;

	public $password;

	public $password_confirm;

	public $password_2;

	public $password_2_confirm;

	public $created_at;

	public $status;

	public $phone;

	public $bank_name;

	public $position;

	public $bank_branch_name;

	public $bank_account_holder;

	public $bank_account_number;

	public $agree;

	const POSITION_LEFT  = 0;

	const POSITION_RIGHT = 1;

	public $id_number;

	public $address;

	public $country;

	public $url;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'username',
					'email',
				],
				'filter',
				'filter' => 'trim',
			],
			[
				[
					'full_name',
					'username',
					'sponsor',
					'password',
					'password_confirm',
					'email',
					'phone',
					'password_2',
					'password_2_confirm',
					'position',
					//					'id_number',
					//					'country',
					//					'address',
				],
				'required',
			],
			[
				[
					'agree',
				],
				'required',
				'requiredValue' => 1,
				'message'       => 'You have to agree with our term first',
			],
			[
				[
					'password',
					'password_2',
				],
				'validatePassword',
			],
			[
				'username',
				'match',
				'not'     => true,
				'pattern' => '/[^a-zA-Z0-9]/',
				'message' => 'Username is incorrect',
			],
			[
				'bank_account_number',
				'match',
				'not'     => true,
				'pattern' => '/[^0-9]/',
				'message' => '{attribute} is incorrect',
			],
			[
				'phone',
				'match',
				'not'     => true,
				'pattern' => '/[^0-9+]/',
				'message' => '{attribute} is incorrect',
			],
			[
				'username',
				'unique',
				'targetClass' => '\app\models\User',
			],
			[
				[
					'bank_branch_name',
					'bank_account_holder',
					'bank_name',
				],
				'string',
				'min' => 2,
				'max' => 255,
			],
			[
				'email',
				'email',
			],
			[
				[
					'username',
					'password',
				],
				'string',
				'min'      => 4,
				'max'      => 20,
				'tooShort' => '{attribute} too short',
				'tooLong'  => '{attribute} too long',
			],
			[
				'password_confirm',
				'compare',
				'compareAttribute' => 'password',
				'message'          => 'Password confirmation dose not match',
			],
			[
				'password_2_confirm',
				'compare',
				'compareAttribute' => 'password_2',
				'message'          => 'Password 2 confirmation dose not match',
			],
			[
				'sponsor',
				'checkUpLine',
			],
			[
				'username',
				'checkUsername',
			],
			[
				'url',
				'safe',
			],
		];
	}

	/**
	 * @param $attribute
	 * @param $params
	 */
	public function validatePassword($attribute, $params) {
		if(preg_match('/\s/', $this->$attribute) > 0) {
			$this->addError($attribute, 'Incorrect character ' . $attribute);
		}
	}

	/**
	 * @param $attribute
	 * @param $params
	 */
	public function checkUpLine($attribute, $params) {
		/** @var \app\models\User $user */
		$user = User::findByUsername($this->$attribute);
		if(!$user) {
			$this->addError($attribute, 'Sponsor not found');
		}
	}

	/**
	 * @param $attribute
	 * @param $params
	 */
	public function checkUsername($attribute, $params) {
		if(strpos(strtolower($this->$attribute), 'admin') !== false) {
			$this->addError($attribute, 'Username not available');
		}
	}

	/**
	 * @param $attribute
	 * @param $params
	 */
	public function checkBankAccountNumber($attribute, $params) {
		$count = User::find()->where(['bank_account_number' => $this->$attribute])->count();
	}

	/**
	 * @param $attribute
	 * @param $params
	 */
	public function checkEmail($attribute, $params) {
		$count = User::find()->where(['email' => $this->$attribute])->count();
	}

	/**
	 * @param User $user
	 *
	 * @return  User
	 */
	public function getLastLeft(User $user) {
		if(!$user->getLeftUser()->exists()) {
			return $user;
		} else {
			while(true) {
				$user = $this->getLastLeft($user->leftUser);
				if(!$user->getLeftUser()->exists()) {
					break;
				}
			}
		}
		return $user;
	}

	/**
	 * @param User $user
	 *
	 * @return  User
	 */
	public function getLastRight(User $user) {
		if(!$user->getRightUser()->exists()) {
			return $user;
		} else {
			while(true) {
				$user = $this->getLastRight($user->rightUser);
				if(!$user->getRightUser()->exists()) {
					break;
				}
			}
		}
		return $user;
	}

	/**
	 * Signs user up.
	 *
	 * @return boolean the saved model or null if saving fails
	 */
	public function register() {
		if($this->validate()) {
			$user                      = new User();
			$user->username            = $this->username;
			$user->email               = $this->email;
			$user->full_name           = $this->full_name;
			$user->bank_name           = $this->bank_name;
			$user->bank_branch_name    = $this->bank_branch_name;
			$user->bank_account_holder = $this->bank_account_holder;
			$user->bank_account_number = $this->bank_account_number;
			$user->phone               = $this->phone;
			$sponsor                   = User::findByUsername($this->sponsor);
			$user->sponsor_id          = $sponsor->id;
			$user->position            = $this->position;
			$user->country             = $this->country;
			$user->address             = $this->address;
			$user->id_number           = $this->id_number;
			if($this->position == self::POSITION_LEFT) {
				if(!$sponsor->getLeftUser()->exists()) {
					$user->parent_id = $sponsor->id;
				} else {
					$lastLeft        = $this->getLastLeft($sponsor);
					$user->parent_id = $lastLeft->id;
				}
			} else {
				if(!$sponsor->getRightUser()->exists()) {
					$user->parent_id = $sponsor->id;
				} else {
					$lastRight       = $this->getLastRight($sponsor);
					$user->parent_id = $lastRight->id;
				}
			}
			$user->password   = Yii::$app->security->generatePasswordHash($this->password);
			$user->password_2 = Yii::$app->security->generatePasswordHash($this->password_2);
			if(!$user->save()) {
				//				echo '<pre>';
				//				print_r($user->getErrors());
				//				die;
			} else {
				$account          = new Account();
				$account->user_id = $user->id;
				$account->save();
				//			Yii::$app->mailer->compose()->setFrom(['michaelmarketcoins@gmail.com' => 'welcome'])->setTo($user->email)->setHtmlBody('<b>Thank you</b>')->setSubject('Welcome ' . $user->username)->setTextBody('Thank you ' . $user->username)->send();
				Yii::$app->mailer->compose('welcome', ['password' => $this->password])->setFrom(['support@marketcoins.org' => 'marketcoins'])->setTo($user->email)->setSubject('Welcome notification')->send();
			}
			return $user->save() ? $user : false;
		} else {
			echo '<pre>';
			print_r($this->errors);
			die;
		}
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [
			'username'            => 'Username',
			'full_name'           => 'Full name',
			'password'            => 'Password',
			'password_2'          => 'Password 2',
			'password_confirm'    => 'Password confirm',
			'password_2_confirm'  => 'Password 2 confirm',
			'phone'               => 'Phone number',
			'bank_name'           => 'Bank',
			'bank_branch_name'    => 'Bank branch',
			'bank_account_number' => 'Bank account number',
			'bank_account_holder' => 'Bank account holder',
			'agree'               => 'Agree to our terms of service',
			'address'             => 'Address',
			'id_number'           => 'ID number',
			'country'             => 'Country',
		];
	}
}
