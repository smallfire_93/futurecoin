<?php
/**
 * Created by PhpStorm.
 * User: Yamon-PC
 * Date: 07-Jun-17
 * Time: 7:29 PM
 */

namespace app\models\form;

use app\components\Form;
use app\models\Support;
use Yii;

class SupportForm extends Form {

	public $title;

	public $content;

	public function rules() {
		return [
			[
				[
					//					'title',
					'content',
				],
				'required',
			],
			[
				[
					'title',
				],
				'string',
				'max' => 500,
			],
			[
				[
					'content',
				],
				'string',
				'max' => 1000,
			],
		];
	}

	public function attributeLabels() {
		return [
			'title'   => 'Title',
			'content' => 'Content',
		];
	}

	public function sendMail() {
		$model          = new Support();
		$model->user_id = $this->user->id;
		$model->title   = $this->title != null ? $this->title : 'No subject';
		$model->content = $this->content;
		$model->status  = $model::STATUS_READ;
		$model->flag    = $model::FLAG_NREPLIED;
		$model->save();
		if($model->save()) {
			Yii::$app->mailer->compose('support')->setFrom(['contact.marketcoins@gmail.com' => 'Marketcoin'])->setTo($this->user->email)->setSubject('Marketcoins auto reply')->send();
		}
	}
}