<?php
/**
 * Created by PhpStorm.
 * User: Yamon-PC
 * Date: 31-May-17
 * Time: 7:42 PM
 */
namespace app\models\form;

use app\components\Form;
use app\models\CoinMining;
use app\models\CoinTransaction;
use app\models\TokenTransaction;
use Yii;

class TokenMiningForm extends Form {

	public $quantity;

	public $t_password;

	public function rules() {
		return [
			[
				['t_password'],
				'validateCurrentPassword',
				'on' => 'mining',
			],
			[
				'quantity',
				'number',
				'min' => 1,
				'on'  => 'mining',
			],
			[
				['quantity'],
				'validateQuantity',
				'on' => 'mining',
			],
			[
				[
					't_password',
					'quantity',
				],
				'required',
				'on' => 'mining',
			],
		];
	}

	public function validateCurrentPassword($attribute) {
		$validate = Yii::$app->security->validatePassword($this->t_password, $this->user->password_2);
		if(!$validate) {
			$this->addError($attribute, "Security password incorrect");
		}
	}

	public function validateQuantity($attribute) {
		$token = $this->user->accounts->total_tokens;
		if($attribute > $token) {
			$this->addError($attribute, 'Your token is not enough');
		}
	}

	public function attributeLabels() {
		return [
			't_password' => 'Security password',
			'quantity'   => 'Tokens quantity',
		];
	}

	public function tokenMining() {
		$coin_mining                    = new CoinMining();
		$coin_mining->user_id           = $this->user->id;
		$coin_mining->start_date        = date('Y-m-d H:i:s');
		$coin_mining->begin_return_coin = date('Y-m-d H:i:s', strtotime("+30 days"));
		$coin_mining->type              = $coin_mining::TOKEN;
		$coin_mining->quantity          = $this->quantity / 2;
		$coin_mining->save();
		$transaction           = new CoinTransaction();
		$transaction->date     = date('Y-m-d H:i:s');
		$transaction->quantity = $coin_mining->quantity;
		$transaction->status   = $transaction::PENDING;
		$transaction->user_id  = $this->user->id;
		$transaction->type     = $transaction::RECEIVE;
		$transaction->save();
		$token_account = $this->user->accounts;
		$token_account->updateAttributes(['total_tokens' => $token_account->total_tokens - $this->quantity]);
		if($token_account->total_tokens < $token_account->token) {
			$token_account->updateAttributes(['token' => $token_account->total_tokens]);
		}
		$token_transaction           = new TokenTransaction();
		$token_transaction->user_id  = $coin_mining->user_id;
		$token_transaction->quantity = - $this->quantity;
		$token_transaction->status   = $token_transaction::STATUS_SUC;
		$token_transaction->type     = $token_transaction::TYPE_MINE;
		$token_transaction->date     = date('Y-m-d H:i:s');
		$token_transaction->save();
		return true;
	}
}