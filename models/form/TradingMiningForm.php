<?php
/**
 * Created by PhpStorm.
 * User: Yamon-PC
 * Date: 31-May-17
 * Time: 7:42 PM
 */
namespace app\models\form;

use app\components\Form;
use app\models\CoinMining;
use app\models\CoinTransaction;
use app\models\TradingTransaction;
use Yii;

class TradingMiningForm extends Form {

	public $quantity;

	public $t_password;

	public function rules() {
		return [
			[
				['t_password'],
				'validateCurrentPassword',
				'on' => 'trading',
			],
			[
				'quantity',
				'number',
				'min' => 1,
				'on'  => 'trading',
			],
			[
				['quantity'],
				'validateQuantity',
				'on' => 'trading',
			],
			[
				[
					't_password',
					'quantity',
				],
				'required',
				'on' => 'trading',
			],
		];
	}

	public function validateCurrentPassword($attribute) {
		$validate = Yii::$app->security->validatePassword($this->t_password, $this->user->password_2);
		if(!$validate) {
			$this->addError($attribute, "Security password incorrect");
		}
	}

	public function validateQuantity($attribute) {
		$trading = $this->user->accounts->trading_account;
		if($attribute > $trading) {
			$this->addError($attribute, 'Your token is not enough');
		}
	}

	public function attributeLabels() {
		return [
			't_password' => 'Security password',
			'quantity'   => 'Trading money',
		];
	}

	public function tradingMining() {
		$coin_mining                    = new CoinMining();
		$coin_mining->user_id           = $this->user->id;
		$coin_mining->start_date        = date('Y-m-d H:i:s');
		$coin_mining->begin_return_coin = date('Y-m-d H:i:s', strtotime("+30 days"));
		$coin_mining->type              = $coin_mining::TRADING;
		$coin_mining->quantity          = $this->quantity / 0.36;
		$coin_mining->save();
		$transaction           = new CoinTransaction();
		$transaction->date     = date('Y-m-d H:i:s');
		$transaction->quantity = $coin_mining->quantity;
		$transaction->status   = $transaction::PENDING;
		$transaction->user_id  = $this->user->id;
		$transaction->type     = $transaction::RECEIVE;
		$transaction->save();
		$account = $this->user->accounts;
		$account->updateAttributes(['trading_account' => $account->trading_account - $this->quantity]);
		$trading_transaction          = new TradingTransaction();
		$trading_transaction->user_id = $coin_mining->user_id;
		$trading_transaction->money   = - $this->quantity;
		$trading_transaction->status  = $trading_transaction::STATUS_SUCCESS;
		$trading_transaction->type    = $trading_transaction::TYPE_MINE;
		$transaction->date            = date('Y-m-d H:i:s');
		$trading_transaction->save();
		return true;
	}
}