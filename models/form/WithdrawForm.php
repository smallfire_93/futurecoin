<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 4/19/2017
 * Time: 10:45 AM
 */

namespace app\models\form;

use app\components\Form;
use app\models\UserKyc;
use Yii;

class WithdrawForm extends Form {

	public $money;

	public $t_password;

	public function rules() {
		return [
			[
				't_password',
				'validateCurrentPassword',
			],
			[
				'money',
				'validateAccount',
			],
			[
				'money',
				'validateKYC',
			],
			[
				[
					't_password',
					'money',
				],
				'required',
			],
		];
	}

	public function validateCurrentPassword($attribute) {
		$validate = Yii::$app->security->validatePassword($this->t_password, $this->user->password_2);
		if(!$validate) {
			$this->addError($attribute, "Security password incorrect");
		}
	}

	public function validateAccount($attribute) {
		if($this->user->accounts->cash_account < $this->money) {
			$this->addError($attribute, 'Your cash account is not enough money');
		}
	}

	public function validateKYC($attribute) {
		if(!$this->user->kyc || $this->user->kyc->kyc_status != UserKyc::KYC_ACCEPT) {
			$this->addError($attribute, 'You need to KYC first');
		}
	}

	public function attributeLabels() {
		return [
			't_password' => 'Security password',
		];
	}
}