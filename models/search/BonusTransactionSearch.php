<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BonusTransaction;

/**
 * BonusTransactionSearch represents the model behind the search form about `app\models\BonusTransaction`.
 */
class BonusTransactionSearch extends BonusTransaction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['receipted_date'], 'safe'],
            [['total_bonus', 'cash', 'trading', 'direct_bonus', 'matching_bonus', 'weak_branch_bonus'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BonusTransaction::find();

        // add conditions that should always apply here

	    $dataProvider = new ActiveDataProvider([
		    'query' => $query,
		    'sort'  => ['defaultOrder' => ['id' => SORT_DESC]],
	    ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user->id,
            'receipted_date' => $this->receipted_date,
            'total_bonus' => $this->total_bonus,
            'cash' => $this->cash,
            'trading' => $this->trading,
            'direct_bonus' => $this->direct_bonus,
            'matching_bonus' => $this->matching_bonus,
            'weak_branch_bonus' => $this->weak_branch_bonus,
        ]);

        return $dataProvider;
    }
}
