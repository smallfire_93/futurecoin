<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CashTransaction;

/**
 * CashTransactionSearch represents the model behind the search form about `app\models\CashTransaction`.
 */
class CashTransactionSearch extends CashTransaction {

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'user_id',
					'type',
					'status',
					'receive_id',
				],
				'integer',
			],
			[
				['money'],
				'number',
			],
			[
				[
					'date',
					'bit_address',
				],
				'safe',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function searchWithdraw($params) {
		$query = CashTransaction::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => ['defaultOrder' => ['id' => SORT_DESC]],
		]);
		$this->load($params);
		if(!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id'      => $this->id,
			'user_id' => $this->user_id,
			'type'    => $this::TYPE_WIT,
			'money'   => $this->money,
			'status'  => $this->status,
			'date'    => $this->date,
		]);
		return $dataProvider;
	}

	public function search($params) {
		$query = CashTransaction::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => ['defaultOrder' => ['id' => SORT_DESC]],
		]);
		$this->load($params);
		if(!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id'      => $this->id,
			'user_id' => $this->user->id,
			'type'    => $this->type,
			'money'   => $this->money,
			'status'  => $this->status,
			'date'    => $this->date,
		])->andFilterWhere([
			'!=',
			'type',
			$this::TYPE_RECEIVE,
		]);
		$query->orFilterWhere([
			'id'         => $this->id,
			'receive_id' => $this->user->id,
			'type'       => $this::TYPE_RECEIVE,
			'money'      => $this->money,
			'status'     => $this->status,
			'date'       => $this->date,
		]);
		return $dataProvider;
	}
}
