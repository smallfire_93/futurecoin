<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CurrentBonus;

/**
 * CurrentBonusSearch represents the model behind the search form about `app\models\CurrentBonus`.
 */
class CurrentBonusSearch extends CurrentBonus
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'weak_branch'], 'integer'],
            [['current_cash', 'current_trading', 'current_bv_left', 'current_bv_right', 'weak_branch_bv', 'total_bonus', 'direct_bonus', 'matching_bonus', 'weak_branch_bonus'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CurrentBonus::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'current_cash' => $this->current_cash,
            'current_trading' => $this->current_trading,
            'current_bv_left' => $this->current_bv_left,
            'current_bv_right' => $this->current_bv_right,
            'weak_branch' => $this->weak_branch,
            'weak_branch_bv' => $this->weak_branch_bv,
            'total_bonus' => $this->total_bonus,
            'direct_bonus' => $this->direct_bonus,
            'matching_bonus' => $this->matching_bonus,
            'weak_branch_bonus' => $this->weak_branch_bonus,
        ]);

        return $dataProvider;
    }
}
