<?php

namespace app\models\search;

use app\models\Package;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PackageTransaction;

/**
 * PackageTransactionSearch represents the model behind the search form about `app\models\PackageTransaction`.
 */
class PackageTransactionSearch extends PackageTransaction {

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'user_id',
					'package_id',
					'type',
					'status',
				],
				'integer',
			],
			[
				[
					'created_date',
					'branch',
					'tree_position',
					'start_time',
					'end_time',
				],
				'safe',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = PackageTransaction::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => ['defaultOrder' => ['id' => SORT_DESC]],
		]);
		$this->load($params);
		if(!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id'           => $this->id,
			'user_id'      => $this->user->id,
			'package_id'   => $this->package_id,
			'type'         => $this->type,
			'created_date' => $this->created_date,
			'status'       => $this::STATUS_SUCCESS,
		]);
		return $dataProvider;
	}

	public function searchData($params, $param = null) {
		$query = PackageTransaction::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => ['defaultOrder' => ['id' => SORT_DESC]],
		]);
		$query->joinWith(['package']);
		$this->load($params);
		if(!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		$query->andFilterWhere([
			'between',
			'created_date',
			$this->start_time,
			$this->end_time,
		]);
		// grid filtering conditions
		$query->andFilterWhere([
			'id'         => $this->id,
			'user_id'    => $this->user_id,
			'package_id' => $this->package_id,
			'type'       => $this->type,
			'status'     => $this::STATUS_SUCCESS,
		]);
		if($param != null) {
			switch($param) {
				case 'quantity':
					$data = $query->count();
					break;
				case 'money':
					$data = $query->sum('package.money');
					break;
				case 'beginner':
					$query->andFilterWhere(['package.level' => 0]);
					$data = $query->count();
					break;
				case 'bronze':
					$query->andFilterWhere(['package.level' => 1]);
					$data = $query->count();
					break;
				case 'silver':
					$query->andFilterWhere(['package.level' => 2]);
					$data = $query->count();
					break;
				case 'gold':
					$query->andFilterWhere(['package.level' => 3]);
					$data = $query->count();
					break;
				case 'gplus':
					$query->andFilterWhere(['package.level' => 4]);
					$data = $query->count();
					break;
				case 'platinum':
					$query->andFilterWhere(['package.level' => 5]);
					$data = $query->count();
					break;
				case 'pplus':
					$query->andFilterWhere(['package.level' => 6]);
					$data = $query->count();
					break;
				case 'vip':
					$query->andFilterWhere(['package.level' => 7]);
					$data = $query->count();
					break;
				default:
					$data = 0;
			}
			return $data;
		} else {
			return $dataProvider;
		}
	}

	public function transaction($params) {
		$query = PackageTransaction::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => ['defaultOrder' => ['id' => SORT_DESC]],
		]);
		$this->load($params);
		if(!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		$query->andFilterWhere([
			'IN',
			'user_id',
			$this->user->getTotalChildren($this->user->id),
		]);
		// grid filtering conditions
		$query->andFilterWhere([
			'between',
			'created_date',
			$this->start_time,
			$this->end_time,
		]);
		$query->andFilterWhere([
			'id'         => $this->id,
			//			'user_id'      => $this->user->id,
			'package_id' => $this->package_id,
			'type'       => $this->type,
			'status'     => $this::STATUS_SUCCESS,
		]);
		return $dataProvider;
	}
}
