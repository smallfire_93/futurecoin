<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Support;

/**
 * SupportSearch represents the model behind the search form of `app\models\Support`.
 */
class SupportSearch extends Support {

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'user_id',
					'status',
					'flag',
				],
				'integer',
			],
			[
				[
					'title',
					'content',
				],
				'safe',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Support::find()->where(['parent_id' => null]);
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => [
				'defaultOrder' => [
					'flag' => SORT_ASC,
					'id'   => SORT_DESC,
				],
			],
		]);
		$this->load($params);
		if(!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id'      => $this->id,
			'user_id' => $this->user_id,
			'flag'    => $this->flag,
			'status'  => $this->status,
		]);
		$query->andFilterWhere([
			'like',
			'title',
			$this->title,
		])->andFilterWhere([
			'like',
			'content',
			$this->content,
		]);
		return $dataProvider;
	}
}
