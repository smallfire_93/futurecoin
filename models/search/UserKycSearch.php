<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\UserKyc;

/**
 * UserKycSearch represents the model behind the search form of `app\models\UserKyc`.
 */
class UserKycSearch extends UserKyc
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'kyc_status'], 'integer'],
            [['id_frond', 'id_back', 'address_proof', 'address_proof2'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserKyc::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'kyc_status' => $this->kyc_status,
        ]);

        $query->andFilterWhere(['like', 'id_frond', $this->id_frond])
            ->andFilterWhere(['like', 'id_back', $this->id_back])
            ->andFilterWhere(['like', 'address_proof', $this->address_proof])
            ->andFilterWhere(['like', 'address_proof2', $this->address_proof2]);

        return $dataProvider;
    }
}
