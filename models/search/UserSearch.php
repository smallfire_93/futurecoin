<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class UserSearch extends User {

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'id',
					'parent_id',
					'sponsor_id',
					'level',
					'role',
					'status',
					'gender',
					'position',
					'blocked_at',
					'last_login_at',
					'last_activated',
					'created_at',
					'updated_at',
				],
				'integer',
			],
			[
				[
					'username',
					'password',
					'password_2',
					'image',
					'full_name',
					'phone',
					'email',
					'block_note',
				],
				'safe',
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = User::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => ['defaultOrder' => ['id' => SORT_DESC]],
		]);
		$this->load($params);
		if(!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id'             => $this->id,
			'parent_id'      => $this->parent_id,
			'sponsor_id'     => $this->sponsor_id,
			'level'          => $this->level,
			'role'           => $this->role,
			'status'         => $this->status,
			'gender'         => $this->gender,
			'position'       => $this->position,
			'blocked_at'     => $this->blocked_at,
			'last_login_at'  => $this->last_login_at,
			'last_activated' => $this->last_activated,
			'created_at'     => $this->created_at,
			'updated_at'     => $this->updated_at,
		]);
		$query->andFilterWhere([
			'like',
			'username',
			$this->username,
		])->andFilterWhere([
				'like',
				'password',
				$this->password,
			])->andFilterWhere([
				'like',
				'password_2',
				$this->password_2,
			])->andFilterWhere([
				'like',
				'image',
				$this->image,
			])->andFilterWhere([
				'like',
				'full_name',
				$this->full_name,
			])->andFilterWhere([
				'like',
				'phone',
				$this->phone,
			])->andFilterWhere([
				'like',
				'email',
				$this->email,
			])->andFilterWhere([
				'like',
				'block_note',
				$this->block_note,
			]);
		return $dataProvider;
	}

	public function getDirect($params) {
		$query = User::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort'  => ['defaultOrder' => ['id' => SORT_DESC]],
		]);
		$this->load($params);
		if(!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere([
			'id'             => $this->id,
			'parent_id'      => $this->parent_id,
			'sponsor_id'     => $this->user->id,
			'level'          => $this->level,
			'role'           => $this->role,
			'status'         => $this->status,
			'gender'         => $this->gender,
			'position'       => $this->position,
			'blocked_at'     => $this->blocked_at,
			'last_login_at'  => $this->last_login_at,
			'last_activated' => $this->last_activated,
			'created_at'     => $this->created_at,
			'updated_at'     => $this->updated_at,
		]);
		$query->andFilterWhere([
			'like',
			'username',
			$this->username,
		])->andFilterWhere([
				'like',
				'password',
				$this->password,
			])->andFilterWhere([
				'like',
				'password_2',
				$this->password_2,
			])->andFilterWhere([
				'like',
				'image',
				$this->image,
			])->andFilterWhere([
				'like',
				'full_name',
				$this->full_name,
			])->andFilterWhere([
				'like',
				'phone',
				$this->phone,
			])->andFilterWhere([
				'like',
				'email',
				$this->email,
			])->andFilterWhere([
				'like',
				'block_note',
				$this->block_note,
			]);
		return $dataProvider;
	}
}
