<?php
namespace app\modules\manager;

use Yii;
use yii\web\User;

/**
 * manager module definition class
 */
class Module extends \yii\base\Module {

	public $controllerNamespace = 'app\modules\manager\controllers';

	public $defaultRoute        = 'site/index';

	public $layout              = 'main';

	/**
	 * @inheritdoc
	 */
	public function init() {
		parent::init();
		$this->setComponents([
			'user'    => [
				'class'           => User::className(),
				'identityClass'   => 'app\models\User',
				'enableAutoLogin' => true,
			],
			'request' => [
				'class'               => '\yii\web\Request',
				// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
				'cookieValidationKey' => 'aXbVOydPsTbPSA76HhPHyQJc7QgLqUU5',
			],
			'view'    => [
				'class' => 'app\components\View',
				'theme' => [
					'basePath' => '@app/modules/manager/views',
					'baseUrl'  => '@web/modules/manager/views',
				],
			],
			'setting' => [
				'class' => 'navatech\setting\Setting',
			],
		]);
		$this->setModules([
			'setting' => [
				'class'               => 'navatech\setting\Module',
				'controllerNamespace' => 'navatech\setting\controllers',
			],
			'roxymce' => [
				'class' => '\navatech\roxymce\Module',
			],
		]);
	}
}