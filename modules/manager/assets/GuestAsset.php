<?php
namespace app\modules\manager\assets;

use app\components\View;
use yii\web\AssetBundle;

class GuestAsset extends AssetBundle {

	public $sourcePath = '@app/modules/manager/web/ace';

	public $css        = [
		'assets/css/bootstrap.min.css',
		'assets/font-awesome/4.2.0/css/font-awesome.min.css',
		'assets/fonts/fonts.googleapis.com.css',
		'assets/css/ace.min.css',
		'assets/css/ace-part2.min.css',
		'assets/css/ace-rtl.min.css',
		'assets/css/ace-ie.min.css',
	];

	public $js         = [
		'assets/js/html5shiv.min.js',
		'assets/js/respond.min.js',
	];

	public $depends    = [
		'yii\web\YiiAsset',
		'yii\bootstrap\BootstrapAsset',
	];

	public $jsOptions  = ['position' => View::POS_HEAD];
}