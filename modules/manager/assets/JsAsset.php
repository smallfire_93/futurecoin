<?php
/**
 * Created by Navatech.
 * @project enterfund
 * @author  LocPX
 * @email   loc.xuanphama1t1[at]gmail.com
 * @date    10/9/2016
 * @time    3:56 AM
 */
namespace app\modules\manager\assets;

use yii\web\AssetBundle;

class JsAsset extends AssetBundle {

	public $sourcePath = '@app/modules/manager/web/ace';

	public $js         = [
		'assets/js/bootstrap.min.js',
		'assets/js/jquery-ui.custom.min.js',
		'assets/js/jquery.ui.touch-punch.min.js',
		'assets/js/jquery.easypiechart.min.js',
		'assets/js/jquery.sparkline.min.js',
		'assets/js/jquery.flot.min.js',
		'assets/js/jquery.flot.pie.min.js',
		'assets/js/jquery.flot.resize.min.js',
		'assets/js/ace-elements.min.js',
		'assets/js/ace.min.js',
	];
}