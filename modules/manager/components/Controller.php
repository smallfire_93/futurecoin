<?php
/**
 * Created by Navatech.
 * @project attravel
 * @author  LocPX
 * @email   loc.xuanphama1t1[at]gmail.com
 * @date    2/19/2016
 * @time    4:53 PM
 */
namespace app\modules\manager\components;

use app\models\User;
use Yii;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * {@inheritDoc}
 */
class Controller extends \yii\web\Controller {

	/**@var User */
	public $user;

	/**
	 * {@inheritDoc}
	 */
	public function init() {
		parent::init();
		if(!Yii::$app->session->isActive) {
			Yii::$app->session->open();
		}
		$this->user = Yii::$app->getUser()->getIdentity();
		if(!Yii::$app->user->isGuest && $this->user->role != User::ROLE_ADMIN) {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public function beforeAction($action) {
		if(Yii::$app->user->isGuest && Yii::$app->controller->action->id !== 'login') {
			$this->redirect(Url::to(['site/login']));
			return false;
		} else {
			return parent::beforeAction($action);
		}
	}
}