<?php

namespace app\modules\manager\controllers;

use app\models\CashTransaction;
use app\models\PackageTransaction;
use app\models\User;
use app\modules\manager\models\BitActiveForm;
use Yii;
use app\models\Package;
use app\models\search\PackageSearch;
use app\components\Controller;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PackageController implements the CRUD actions for Package model.
 */
class PackageController extends Controller {

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all Package models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new PackageSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single Package model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Package model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Package();
		if($model->load(Yii::$app->request->post()) && $model->save()) {
			$img = $model->uploadPicture('image', 'package_img');
			if($model->save()) {
				if($img !== false) {
					$path = $model->getPictureFile('image');
					$img->saveAs($path);
				}
			}
			return $this->redirect(['index']);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing Package model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model    = $this->findModel($id);
		$oldImage = $model->image;
		if($model->load(Yii::$app->request->post()) && $model->save()) {
			$img = $model->uploadPicture('image', 'package_img');
			if($img == false) {
				$model->image = $oldImage;
			}
			if($model->save()) {
				if($img !== false) {
					$path = $model->getPictureFile('image');
					$img->saveAs($path);
				}
			} else {
				echo '<pre>';
				print_r($model->errors);
				die;
			}
			return $this->redirect([
				'index',
			]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Package model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	public function actionCashPackage() {
		$userAll    = ArrayHelper::map(User::find()->all(), 'id', 'username');
		$packageAll = ArrayHelper::map(Package::find()->all(), 'id', 'name');
		$model      = new BitActiveForm();
		//TODO validate ajax
		if($model->load(Yii::$app->request->post()) && $model->validate()) {
			$package             = $this->findModel((int) $model->package);
			$order               = new PackageTransaction();
			$order->created_date = date('Y-m-d H:i:s');
			$order->package_id   = $package->id;
			$order->type         = $order::TYPE_BITCOIN;
			$order->user_id      = $model->username;
			$order->status       = $order::STATUS_SUCCESS;
			$order->save();
			$package->updateDirectManual((int) $model->username);
			$package->updateTeamManual((int) $model->username);
			$package->activeUserManual($package->id, (int) $model->username);
			if($order->save()) {
				Yii::$app->session->setFlash('success', 'Thành công');
			} else {
				Yii::$app->session->setFlash('danger', 'Thất bại');
			};
		}
		return $this->render('manual-package', [
			'model'      => $model,
			'userAll'    => $userAll,
			'packageAll' => $packageAll,
		]);
	}

	/**
	 * Finds the Package model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Package the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if(($model = Package::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
