<?php
namespace app\modules\manager\controllers;


use app\models\User;
use app\modules\manager\components\Controller;
use app\modules\manager\models\LoginForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Response;

class SiteController extends Controller {

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [
							'login',
						],
						'allow'   => true,
						'roles'   => ['?'],
					],
					[
						'actions' => [
							'logout',
							'index',
						],
						'allow'   => true,
						'roles'   => ['@'],
					],
				],
			],
			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	/**
	 * {@inheritDoc}
	 */
	public function actions() {
		return [
			'error'   => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class'           => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}

	/**
	 * @return string
	 */
	public function actionIndex() {

		return $this->render('index', [

		]);
	}

	/**
	 * @return string|Response
	 */
	public function actionLogin() {
		$this->layout = 'guest';
		if(!\Yii::$app->user->isGuest) {
			return $this->goBack(['/manager']);
		}
		$model = new LoginForm();
		if($model->load(Yii::$app->request->post()) && $model->login()) {
			$this->user = Yii::$app->getUser()->getIdentity();
			Yii::$app->session->setFlash('success', 'Welcome back ' . $this->user->username);
			return $this->redirect(['/manager']);
		}
		return $this->render('login', [
			'model' => $model,
		]);
	}

	/**
	 * @return Response
	 */
	public function actionLogout() {
		Yii::$app->user->logout();
		return $this->goBack(['/manager']);
	}
}
