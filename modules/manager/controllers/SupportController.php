<?php

namespace app\modules\manager\controllers;

use app\models\form\SupportForm;
use app\modules\manager\components\Controller;
use Yii;
use app\models\Support;
use app\models\search\SupportSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SupportController implements the CRUD actions for Support model.
 */
class SupportController extends Controller {

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all Support models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new SupportSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$tickets      = Support::find()->andWhere(['parent_id' => null])->orderBy('id DESC')->all();
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
			'tickets'      => $tickets,
		]);
	}

	public function actionSupportView($id) {
		$model = Support::find()->where(['id' => $id])->one();
		if($model) {
			$modelForm = new SupportForm();
			if($modelForm->load(Yii::$app->request->post())) {
				$modelAdd            = new Support();
				$modelAdd->user_id   = $this->user->id;
				$modelAdd->title     = $model->title;
				$modelAdd->content   = $modelForm->content;
				$modelAdd->parent_id = $model->id;
				if($modelAdd->save()) {
					$model->updateAttributes([
						'flag'   => Support::FLAG_REPLIED,
						'status' => Support::STATUS_UNREAD,
					]);
				}
				return $this->refresh();
			}
			$replies = Support::find()->where(['parent_id' => $model->id])->all();
			$model->updateAttributes(['status' => Support::STATUS_READ]);
			return $this->render('support-view', [
				'model'     => $model,
				'replies'   => $replies,
				'modelForm' => $modelForm,
			]);
		} else {
			return $this->redirect(['/support/index']);
		}
	}

	/**
	 * Displays a single Support model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new Support model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Support();
		if($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect([
				'view',
				'id' => $model->id,
			]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing Support model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel($id);
		if($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect([
				'view',
				'id' => $model->id,
			]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing Support model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	/**
	 * Finds the Support model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Support the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if(($model = Support::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
