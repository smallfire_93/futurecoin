<?php

namespace app\modules\manager\controllers;

use app\models\CashTransaction;
use app\models\search\CashTransactionSearch;
use Yii;
use app\models\PackageTransaction;
use app\models\search\PackageTransactionSearch;
use app\modules\manager\components\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TransactionController implements the CRUD actions for PackageTransaction model.
 */
class TransactionController extends Controller {

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all PackageTransaction models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel    = new PackageTransactionSearch();
		$params         = Yii::$app->request->queryParams;
		$dataProvider   = $searchModel->searchData(Yii::$app->request->queryParams);
		$total_money    = $searchModel->searchData($params, 'money');
		$total_quantiy  = $searchModel->searchData($params, 'quantity');
		$total_beginner = $searchModel->searchData($params, 'beginner');
		$total_bronze   = $searchModel->searchData($params, 'bronze');
		$total_silver   = $searchModel->searchData($params, 'silver');
		$total_gold     = $searchModel->searchData($params, 'gold');
		$total_gplus    = $searchModel->searchData($params, 'gplus');
		$total_platinum = $searchModel->searchData($params, 'platinum');
		$total_pplus    = $searchModel->searchData($params, 'pplus');
		$total_vip      = $searchModel->searchData($params, 'vip');
		return $this->render('index', [
			'searchModel'    => $searchModel,
			'dataProvider'   => $dataProvider,
			'total_money'    => $total_money,
			'total_quantity' => $total_quantiy,
			'total_beginner' => $total_beginner,
			'total_bronze'   => $total_bronze,
			'total_silver'   => $total_silver,
			'total_gold'     => $total_gold,
			'total_gplus'    => $total_gplus,
			'total_platinum' => $total_platinum,
			'total_pplus'    => $total_pplus,
			'total_vip'      => $total_vip,
		]);
	}

	/**
	 * Displays a single PackageTransaction model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new PackageTransaction model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new PackageTransaction();
		if($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect([
				'view',
				'id' => $model->id,
			]);
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing PackageTransaction model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel($id);
		if($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect([
				'view',
				'id' => $model->id,
			]);
		} else {
			return $this->render('update', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Deletes an existing PackageTransaction model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	/**
	 * Finds the PackageTransaction model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return PackageTransaction the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if(($model = PackageTransaction::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	public function actionWithdraw() {
		$searchModel  = new CashTransactionSearch();
		$dataProvider = $searchModel->searchWithdraw(Yii::$app->request->queryParams);
		return $this->render('withdraw', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionSuccess($id) {
		$model = CashTransaction::findOne($id);
		if($model->status !== $model::STATUS_SUCCESS && $model->status !== $model::STATUS_REJECT) {
			$model->updateAttributes(['status' => $model::STATUS_SUCCESS]);
		}
		return $this->redirect(['withdraw']);
	}

	public function actionReject($id) {
		$model = CashTransaction::findOne($id);
		if($model->status !== $model::STATUS_SUCCESS && $model->status !== $model::STATUS_REJECT) {
			$model->updateAttributes(['status' => $model::STATUS_REJECT]);
			$model->users->accounts->updateAttributes(['cash_account' => $model->users->accounts->cash_account + $model->money]);
		}
		return $this->redirect(['withdraw']);
	}
}
