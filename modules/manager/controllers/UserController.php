<?php
namespace app\modules\manager\controllers;

use app\models\search\UserSearch;
use app\models\User;
use app\modules\manager\components\Controller;
use app\modules\manager\models\ChangePasswordForm;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\widgets\ActiveForm;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller {

	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'verbs' => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => ['POST'],
				],
			],
		];
	}

	/**
	 * Lists all User models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel  = new UserSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->render('index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	/**
	 * Displays a single User model.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}

	/**
	 * Creates a new User model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new User();
		if($model->load(Yii::$app->request->post())) {
			$sponsor          = User::findByUsername($model->sponsor_id);
			$parent_id        = $sponsor->getLastChild();
			$model->parent_id = $parent_id;
			$parent           = User::findOne($parent_id);
			if(!$parent->getLeftUser()->exists()) {
				$model->position = User::POSITION_LEFT;
			} else {
				$model->position = User::POSITION_RIGHT;
			}
			$model->password   = Yii::$app->security->generatePasswordHash($model->password);
			$model->password_2 = Yii::$app->security->generatePasswordHash($model->password_2);
			if($model->save()) {
				return $this->redirect(['index']);
			} else {
				return $this->render('create', [
					'model' => $model,
				]);
			}
		} else {
			return $this->render('create', [
				'model' => $model,
			]);
		}
	}

	/**
	 * Updates an existing User model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model        = $this->findModel($id);
		$oldStatus    = $model->status;
		$oldPassword  = $model->password;
		$oldPassword2 = $model->password_2;
		$oldSponsorId = $model->sponsor_id;
		if($model->load(Yii::$app->request->post())) {
			if(isset($_POST['User']['password']) && $_POST['User']['password'] != '') {
				$model->password = Yii::$app->security->generatePasswordHash($_POST['User']['password']);
			} else {
				$model->password = $oldPassword;
			}
			if(isset($_POST['User']['password_2']) && $_POST['User']['password_2'] != '') {
				$model->password_2 = Yii::$app->security->generatePasswordHash($_POST['User']['password_2']);
			} else {
				$model->password_2 = $oldPassword2;
			}
			if($oldSponsorId != $model->sponsor_id) {
				$sponsor          = User::findOne($model->sponsor_id);
				$parent_id        = $sponsor->getLastChild();
				$model->parent_id = $parent_id;
				$parent           = User::findOne($parent_id);
				if(!$parent->getLeftUser()->exists()) {
					$model->position = User::POSITION_LEFT;
				} else {
					$model->position = User::POSITION_RIGHT;
				}
			}
			if($model->save()) {
				return $this->redirect([
					'view',
					'id' => $model->id,
				]);
			}
		}
		$model->password   = '';
		$model->password_2 = '';
		return $this->render('update', [
			'model' => $model,
		]);
	}

	/**
	 * Deletes an existing User model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel($id)->delete();
		return $this->redirect(['index']);
	}

	/**
	 * Finds the User model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return User the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if(($model = User::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	public function actionChangePassword() {
		$model = new ChangePasswordForm;
		if($model->load(Yii::$app->request->post()) && count(ActiveForm::validate($model)) == 0) {
			$this->user->password = Yii::$app->security->generatePasswordHash($model->new_password);
			$this->user->save();
			Yii::$app->session->setFlash("success", "Your password was updated");
			$this->refresh();
		} else {
			return $this->render('changePassword', [
				'model' => $model,
			]);
		}
	}

	public function actionReport() {
		$user['series'] = [
			[
				'name'  => 'Số thành viên',
				'color' => 'black',
			],
			[
				'name'  => 'Số thành viên kích hoạt',
				'color' => 'green',
			],
			[
				'name'  => 'Số thành viên chưa kích hoạt',
				'color' => 'yellow',
			],
			[
				'name'  => 'Số thành viên bị khóa',
				'color' => 'red',
			],
		];
		for($i = 30; $i >= 0; $i --) {
			$user['categories'][]        = date('d/m', strtotime($i . ' days ago'));
			$user['series'][0]['data'][] = (int) User::find()->where([
				'>',
				'updated_at',
				strtotime(date("Y-m-d 00:00:00", strtotime($i . ' days ago'))),
			])->andWhere([
				'<',
				'updated_at',
				strtotime(date("Y-m-d 23:59:59", strtotime($i . ' days ago'))),
			])->count();
			$user['series'][1]['data'][] = (int) User::find()->where(['status' => User::STATUS_ACTIVATED])->andWhere([
				'>',
				'updated_at',
				strtotime(date("Y-m-d 00:00:00", strtotime($i . ' days ago'))),
			])->andWhere([
				'<',
				'updated_at',
				strtotime(date("Y-m-d 23:59:59", strtotime($i . ' days ago'))),
			])->count();
			$user['series'][2]['data'][] = (int) User::find()->where(['status' => User::STATUS_DEACTIVATED])->andWhere([
				'>',
				'updated_at',
				strtotime(date("Y-m-d 00:00:00", strtotime($i . ' days ago'))),
			])->andWhere([
				'<',
				'updated_at',
				strtotime(date("Y-m-d 23:59:59", strtotime($i . ' days ago'))),
			])->count();
			$user['series'][3]['data'][] = (int) User::find()->where(['status' => User::STATUS_BLOCKED])->andWhere([
				'>',
				'updated_at',
				strtotime(date("Y-m-d 00:00:00", strtotime($i . ' days ago'))),
			])->andWhere([
				'<',
				'updated_at',
				strtotime(date("Y-m-d 23:59:59", strtotime($i . ' days ago'))),
			])->count();
		}
		return $this->render('report', [
			'user' => $user,
		]);
	}
}
