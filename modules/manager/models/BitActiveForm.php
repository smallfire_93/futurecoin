<?php

namespace app\modules\manager\models;

use app\models\User;
use Yii;
use app\components\Form as FormModel;

/**
 * Login form
 */
class BitActiveForm extends FormModel {

	public $username;

	public $package;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'username',
					'package',
				],
				'required',
			],
		];
	}
}
