<?php
namespace app\modules\manager\models;

use app\components\Form;
use Yii;

/**
 * PinTransferForm form
 */
class ChangePasswordForm extends Form {

	public $current_password;

	public $new_password;

	public $new_password_confirm;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [
			[
				[
					'current_password',
					'new_password',
					'new_password_confirm',
				],
				'required',
			],
			[
				'new_password_confirm',
				'compare',
				'compareAttribute' => 'new_password',
				'message'          => "Password confirmation dose not match.",
			],
			[
				'current_password',
				'validateCurrentPassword',
			],
		];
	}

	/**
	 */
	public function validateCurrentPassword() {
		$validate = Yii::$app->security->validatePassword($this->current_password, $this->user->password);
		if(!$validate) {
			$this->addError("current_password", "Current password  incorrect");
		}
	}
}
