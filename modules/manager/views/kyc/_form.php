<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserKyc */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-kyc-form">

	<?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>

	<?= $form->field($model, 'kyc_status')->dropDownList([$model::STATUS])->label('Trạng thái') ?>

	<div class="form-group col-sm-3">
		<?= Html::submitButton('Cập nhật trạng thái', ['class' => 'btn btn-success pull-right']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
