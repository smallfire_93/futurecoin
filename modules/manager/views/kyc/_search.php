<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\UserKycSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-kyc-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'id_frond') ?>

    <?= $form->field($model, 'id_back') ?>

    <?= $form->field($model, 'address_proof') ?>

    <?php // echo $form->field($model, 'address_proof2') ?>

    <?php // echo $form->field($model, 'kyc_status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
