<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\UserKyc */

$this->title = 'Create User Kyc';
$this->params['breadcrumbs'][] = ['label' => 'User Kycs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-kyc-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
