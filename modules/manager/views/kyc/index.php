<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UserKycSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Yêu cầu KYC';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-kyc-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>


	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			[
				'attribute' => 'user_id',
				'value'     => function (\app\models\UserKyc $data) {
					return $data->user_id != null ? $data->users->username : null;
				},
				'filter'    => false,
			],
			[
				'attribute' => 'id_frond',
				'value'     => function (\app\models\UserKyc $data) {
					return Html::img($data->getPictureUrl('id_frond'), ['class' => 'file-preview-image']);
				},
				'filter'    => false,
				'format'    => 'raw',
			],
			[
				'attribute' => 'id_back',
				'value'     => function (\app\models\UserKyc $data) {
					return Html::img($data->getPictureUrl('id_back'), ['class' => 'file-preview-image']);
				},
				'filter'    => false,
				'format'    => 'raw',
			],
			[
				'attribute' => 'address_proof',
				'value'     => function (\app\models\UserKyc $data) {
					return Html::img($data->getPictureUrl('address_proof'), ['class' => 'file-preview-image']);
				},
				'filter'    => false,
				'format'    => 'raw',
			],
			//			[
			//				'attribute' => 'address_proof2',
			//				'value'     => function (\app\models\UserKyc $data) {
			//					Html::img($data->getPictureUrl('address_proof2'), ['class' => 'file-preview-image']);
			//				},
			//				'filter'    => false,
			//			],
			[
				'attribute' => 'kyc_status',
				'value'     => function (\app\models\UserKyc $data) {
					return $data::STATUS[$data->kyc_status];
				},
				'filter'    => $searchModel::STATUS,
			],
			[
				'class'    => 'yii\grid\ActionColumn',
				'template' => '{update}',
				'header'   => 'Cập nhật trạng thái',
				//				'buttons'  => [
				//					'update' => function ($url, \app\models\UserKyc $model) {
				//						return Html::a('Cập nhật', \yii\helpers\Url::to([
				//							'transaction/success',
				//							'id' => $model->id,
				//						]));
				//					},
				//				],
				//			],
			],
		],
	]); ?>
</div>
