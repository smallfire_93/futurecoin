<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UserKyc */

$this->title = 'Cập nhật KYC: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Kycs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-kyc-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
