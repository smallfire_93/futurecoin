<?php
/* @var $this app\components\View */
/* @var $content string */
use app\modules\manager\assets\AppAsset;
use app\modules\manager\assets\JsAsset;
use app\modules\manager\widgets\Alert;
use app\modules\manager\widgets\LeftSidebar;
use app\modules\manager\widgets\TopBar;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

?>
<?php
AppAsset::register($this);
JsAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body class="no-skin">
<?php $this->beginBody() ?>
<?= TopBar::widget() ?>
<div class="main-container" id="main-container">
	<script type="text/javascript">
		try {
			ace.settings.check('main-container', 'fixed')
		} catch(e) {
		}
	</script>

	<?= LeftSidebar::widget() ?>
	<div class="main-content">
		<div class="main-content-inner">
			<div class="breadcrumbs" id="breadcrumbs">
				<script type="text/javascript">
					try {
						ace.settings.check('breadcrumbs', 'fixed')
					} catch(e) {
					}
				</script>
				<?= Breadcrumbs::widget([
					'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
				]) ?>
			</div>
			<div class="page-content">

				<?= Alert::widget() ?>
				<?= $content ?>
			</div>
		</div>
	</div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
