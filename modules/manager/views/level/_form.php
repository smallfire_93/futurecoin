<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Level */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="level-form">

	<?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'bv')->textInput() ?>
	<?= $form->field($model, 'bv_range')->textInput() ?>
	<?= $form->field($model, 'order_by')->textInput() ?>

	<?= $form->field($model, 'number_sap')->textInput() ?>

	<?= $form->field($model, 'number_rub')->textInput() ?>

	<?= $form->field($model, 'number_eme')->textInput() ?>

	<?= $form->field($model, 'number_dia')->textInput() ?>

	<?= $form->field($model, 'number_bla')->textInput() ?>

	<?= $form->field($model, 'number_gre')->textInput() ?>

	<?= $form->field($model, 'bonus')->textInput() ?>

	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
