<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\LevelSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="level-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name') ?>

    <?= $form->field($model, 'bv') ?>

    <?= $form->field($model, 'number_sap') ?>

    <?= $form->field($model, 'number_rub') ?>

    <?php // echo $form->field($model, 'number_eme') ?>

    <?php // echo $form->field($model, 'number_dia') ?>

    <?php // echo $form->field($model, 'number_bla') ?>

    <?php // echo $form->field($model, 'number_gre') ?>

    <?php // echo $form->field($model, 'bonus') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
