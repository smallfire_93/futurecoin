<?php
use kartik\widgets\FileInput;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Package */
?>

<div class="package-form">

	<?php $form = ActiveForm::begin([
		'layout'  => 'horizontal',
		'options' => [
			'enctype' => 'multipart/form-data',
		],
	]); ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
	<?= $form->field($model, 'package_img')->widget(FileInput::className(), [
		'options'       => ['accept' => 'image/*'],
		'pluginOptions' => [
			'allowedFileExtensions' => [
				'jpg',
				'gif',
				'png',
			],
			'showUpload'            => false,
			'initialPreview'        => $model->getIsNewRecord() ? [
				Html::img(Yii::$app->urlManager->baseUrl . '/uploads/no_image_thumb.gif', ['class' => 'file-preview-image']),
			] : [
				Html::img($model->getPictureUrl('image'), ['class' => 'file-preview-image']),
			],
		],
	]); ?>
	<?= $form->field($model, 'money')->textInput() ?>

	<?= $form->field($model, 'token')->textInput() ?>
	<?= $form->field($model, 'level')->textInput() ?>


	<div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Thêm mới' : 'Cập nhật', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
