<?php
/* @var $this yii\web\View */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Alert;
use yii\helpers\Html;

/* @var $model app\modules\manager\models\BitActiveForm */
?>

<div class="package-form">
	<div class="row">
		<div class="col-xs-12">

		</div>
	</div>
	<?php $form = ActiveForm::begin([
		'layout' => 'horizontal',
	]); ?>

	<?= $form->field($model, 'username')->widget(\kartik\select2\Select2::className(), ['data' => $userAll]) ?>

	<?= $form->field($model, 'package')->widget(\kartik\select2\Select2::className(), ['data' => $packageAll]) ?>

	<div class="form-group col-sm-3">
		<?= Html::submitButton('Kích hoạt', ['class' => 'btn btn-primary pull-right']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>