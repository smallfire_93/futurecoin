<?php
/**
 * @var View   $this
 * @var User[] $recentUsers
 * @var array  $transfer
 * @var array  $statistic
 */
use app\components\View;
use app\models\User;
use app\components\widgets\Alert;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

$this->title                   = 'Bảng điều khiển';
$this->params['breadcrumbs'][] = $this->title;
?>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<div class="page-header">
	<h1>
		<?= $this->title ?>
	</h1>
</div>
<?= Alert::widget(); ?>

