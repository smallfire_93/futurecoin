<?php
/* @var $this \app\components\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\form\SignUpForm */
/* @var $sponsor app\models\User */
use app\models\Country;
use app\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title                   = 'Sign Up';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card-box">
	<?php $form = ActiveForm::begin([
		'id'          => 'login-form',
		'layout'      => 'horizontal',
		'fieldConfig' => [
			'template'     => "{label}\n<div class=\"col-lg-6\">{input}</div>\n<div class=\"col-lg-offset-3 col-lg-6\">{error}</div>",
			'labelOptions' => ['class' => 'col-lg-3 control-label'],
		],
		'enableAjaxValidation' => true,

	]); ?>

	<?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
	<?= $form->field($model, 'full_name')->textInput(['autofocus' => true]) ?>
	<?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
	<?= $form->field($model, 'phone')->textInput() ?>
	<?= $form->field($model, 'sponsor', ['inputTemplate' => "<div class=\"col-lg-9 row\">{input}</div>",])->textInput([
		'value'    => $this->user->username,
		'readonly' => 'readonly',
	]) ?>

	<?= $form->field($model, 'password')->passwordInput() ?>
	<?= $form->field($model, 'agree')->checkbox(['template' => "<div class=\"col-lg-offset-3 col-lg-8 checkbox checkbox-primary\"><div class= \"col-lg-6\">{input} {label}</div></div>\n<div class=\"col-lg-8\">{error}</div>",]) ?>
	<div class="form-group">
		<div class="col-lg-offset-3 col-lg-11">
			<?= Html::submitButton('Submit', [
				'class' => 'btn btn-primary',
				'name'  => 'login-button',
			]) ?>
		</div>
	</div>

	<?php ActiveForm::end(); ?>
</div>
