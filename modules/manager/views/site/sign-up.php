<?php
use kartik\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this \app\components\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\form\SignUpForm */
/* @var $sponsor app\models\User */
$this->title                   = 'Sign Up';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="wrapper-page">
	<div class=" card-box">
		<div class="panel-heading">
			<h3 class="text-center"> Sign Up to <strong class="text-custom">UBold</strong></h3>
		</div>
		<div class="panel-body">
			<?php $form = ActiveForm::begin([
				'id'          => 'login-form',
				'layout'      => 'horizontal',
				'fieldConfig' => [
					'template'     => "<div class=\"col-xs-12\">{input}</div>\n<div class=\"col-xs-12\">{error}</div>",
					'labelOptions' => ['class' => 'col-lg-3 control-label'],
				],
				'enableAjaxValidation' => true,
			]); ?>
			<?= $form->field($model, 'username')->textInput([
				'autofocus'   => true,
				'placeholder' => 'Username',
			])->label(false) ?>
			<?= $form->field($model, 'full_name')->textInput([
				'autofocus'   => true,
				'placeholder' => 'Full Name',
			])->label(false) ?>
			<?= $form->field($model, 'email')->textInput([
				'autofocus'   => true,
				'placeholder' => 'Email',
			])->label(false) ?>
			<?= $form->field($model, 'phone')->textInput([
				'autofocus'   => true,
				'placeholder' => 'Phone',
			])->label(false) ?>
			<?= $form->field($model, 'sponsor', ['inputTemplate' => "<div class=\"col-lg-9 row\">{input}</div><div class=\"btn btn-primary check-sponsor\">Check</div>",])->textInput([
				'value'       => ($sponsor) ? $sponsor->username : '',
				'placeholder' => 'Sponsor username',
			]) ?>
			<div class="col-lg-9">
				<div class="help-block help-block ">
					<div id="showSponsor"></div>
				</div>
			</div>
			<?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Password']) ?>
			<?= $form->field($model, 'password_confirm')->passwordInput(['placeholder' => 'Password confirmation']) ?>

			<?= $form->field($model, 'agree')->checkbox([
				'template' => "<div class=\"col-lg-12 checkbox checkbox-primary\"><div class= \"col-lg-12\">{input} {label}</div></div>\n<div class=\"col-lg-8\">{error}</div>",
			]) ?>
			<div class="form-group text-center m-t-40">
				<div class="col-xs-12">
					<?= Html::submitButton('Register', [
						'class' => 'btn btn-pink btn-block text-uppercase waves-effect waves-ligh',
						'name'  => 'login-button',
					]) ?></div>
			</div>
		</div>
	</div>
	<?php ActiveForm::end(); ?>
</div>
</div>
<div class="row">
	<div class="col-sm-12 text-center">
		<p>
			Already have account?<a href="<?= Url::to(['site/login']) ?>" class="text-primary m-l-5"><b>Sign In</b></a>
		</p>
	</div>
</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$(document).on("click", '.check-sponsor', function() {
			$.LoadingOverlay("show");
			var fieldSponsor = $('.field-signupform-sponsor');
			$.ajax({
				type   : "GET",
				url    : '<?= Url::to(["site/check-sponsor"])?>',
				data   : 'username=' + $('#signupform-sponsor').val(),
				success: function(data) {
					$.LoadingOverlay("hide");
					if(data) {
						fieldSponsor.removeClass('has-error').addClass('has-success');
						$("#showSponsor").html("Full name: " + data.full_name).show();
					} else {
						fieldSponsor.removeClass('has-success').addClass('has-error');
						$("#showSponsor").html("Not found").show();
					}
				}
			})
		});
	});
</script>