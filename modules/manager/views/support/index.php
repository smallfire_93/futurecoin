<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SupportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Supports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="support-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>


	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			[
				'attribute' => 'user_id',
				'value'     => function ($data) {
					return $data->users->username;
				},
				'filter'    => $searchModel->getFilterUser(),
			],
			'title',
			'content:ntext',
			[
				'attribute' => 'flag',
				'value'     => function (\app\models\Support $data) {
					return $data::FLAG[$data->flag];
				},
				'filter'    => $searchModel::FLAG,
			],
			[
				'class'    => 'yii\grid\ActionColumn',
				'template' => '{view-support}',
				'buttons'  => [
					'view-support' => function ($url, $model) {
						return Html::a('<span class="glyphicon glyphicon-plus">Trả lời</span>', \yii\helpers\Url::to([
							'support-view',
							'id' => $model->id,
						]), [
							'title' => 'Trả lời',
						]);
					},
				],
			],
		],
	]); ?>
</div>
