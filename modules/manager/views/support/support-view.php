<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 12/2/2017
 * Time: 3:42 PM
 */
use yii\bootstrap\ActiveForm;

/** @var app\models\Support $reply */
?>

<?php
?>
<h3 class="btn-success"> <?= $model->title ?></h3>
<p class="content-ticket"> <?= $model->user->username .': '. $model->content ?></p>
<?php
foreach($replies as $reply) {
	?>
	<p class="<?= $reply->user_id != $reply->user->id ? 'rep' : 'content-ticket' ?>"> <?= $reply->user->username ?>: <?= $reply->content ?></p>
	<?php
}
?>
<?php
$form = ActiveForm::begin(['layout' => 'horizontal']);
echo $form->field($modelForm, 'content')->textarea([
	'rows' => 6,
])->label('Trả lời', ['style' => 'color:blue; font-weight:bold']);
?>
<div class="col-sm-offset-3">
	<?php
	echo \yii\bootstrap\Html::submitButton('Send', ['class' => 'btn btn-success']);
	?>
</div>
<?php
ActiveForm::end()
?>
