<?php
use kartik\date\DatePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\search\PackageTransactionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="package-transaction-search">

	<?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
		'layout' => 'horizontal',
	]); ?>
	<div class="col-sm-6">
		<?= $form->field($model, 'start_time')->widget(DatePicker::className(), [
			'pluginOptions' => [
				'format'         => 'yyyy-mm-dd',
				'todayHighlight' => true,
			],
		]) ?>
	</div>
	<div class="col-sm-6">
		<?= $form->field($model, 'end_time')->widget(DatePicker::className(), [
			'pluginOptions' => [
				'format'         => 'yyyy-mm-dd',
				'todayHighlight' => true,
			],
		]) ?>
	</div>
	<div class="form-group">
		<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
		<?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
	</div>

	<?php ActiveForm::end(); ?>

</div>
