<?php
use app\models\PackageTransaction;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\\PackageTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Lịch sửa mua gói';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="package-transaction-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<div class="panel panel-success">
		<div class="panel-heading">Tìm kiếm</div>
		<div class="panel-body">
			<?php echo $this->render('_search', ['model' => $searchModel]); ?>
		</div>
	</div>
	<div class="panel panel-success">
		<div class="panel-heading">Báo cáo doanh số</div>
		<div class="panel-body">
			<div class="col-sm-4" style="margin-bottom: 20px">Số gói đã bán: <?= $total_quantity ?></div>
			<div class="col-sm-4" style="margin-bottom: 20px">Tổng doanh số đã bán: <?= $total_money ?> €</div>
			<div class="col-sm-4" style="margin-bottom: 20px">Số gói beginner đã bán: <?= $total_beginner ?> </div>
			<div class="col-sm-4" style="margin-bottom: 20px">Số gói bronze đã bán: <?= $total_bronze ?></div>
			<div class="col-sm-4" style="margin-bottom: 20px">Số gói silver đã bán: <?= $total_silver ?></div>
			<div class="col-sm-4" style="margin-bottom: 20px">Số gói gold đã bán: <?= $total_gold ?></div>
			<div class="col-sm-4" style="margin-bottom: 20px">Số gói gold plus đã bán: <?= $total_gplus ?></div>
			<div class="col-sm-4" style="margin-bottom: 20px">Số gói platinum đã bán: <?= $total_platinum ?></div>
			<div class="col-sm-4" style="margin-bottom: 20px">Số gói platinum plus đã bán: <?= $total_pplus ?></div>
			<div class="col-sm-4" style="margin-bottom: 20px">Số gói VIP đã bán: <?= $total_vip ?></div>
		</div>
	</div>
	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			'id',
			[
				'attribute' => 'user_id',
				'value'     => function (PackageTransaction $model) {
					return $model->users->username;
				},
			],
			[
				'attribute' => 'package_id',
				'value'     => function (PackageTransaction $model) {
					return $model->package->name;
				},
			],
			'type',
			'created_date',
			['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>
</div>
