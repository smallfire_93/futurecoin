<?php
use app\models\CashTransaction;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CashTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Yêu cầu nhận hoa hồng';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cash-transaction-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
	</p>
	<div class="table-responsive">
		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel'  => $searchModel,
			//		'tableOptions' => ['class' => 'table-responsive'],
			'columns'      => [
				['class' => 'yii\grid\SerialColumn'],
				[
					'attribute' => 'user_id',
					'value'     => function (CashTransaction $data) {
						return $data->user_id != null ? $data->users->username : null;
					},
					'filter'    => false,
				],
				[
					'attribute' => 'money',
					'value'     => function (CashTransaction $data) {
						return $data->money;
					},
				],
				[
					'attribute' => 'status',
					'value'     => function (CashTransaction $data) {
						return $data::STATUS[$data->status];
					},
					'filter'    => $searchModel::STATUS,
				],
				'date',
				[
					'attribute' => 'bit_address',
					'value'     => function (CashTransaction $data) {
						return $data->users->bit_coin_address;
					},
				],
				// 'date',
				[
					'class'    => 'yii\grid\ActionColumn',
					'template' => '{update} {reject}',
					'header'   => 'Cập nhật trạng thái',
					'buttons'  => [
						'update' => function ($url, CashTransaction $model) {
							return Html::a('Chấp nhận', \yii\helpers\Url::to([
								'transaction/success',
								'id' => $model->id,
							]));
						},
						'reject' => function ($url, CashTransaction $model) {
							return Html::a('Từ chối', \yii\helpers\Url::to([
								'transaction/reject',
								'id' => $model->id,
							]));
						},
					],
				],
			],
		]); ?>
	</div>
</div>
