<?php
use app\models\User;
use kartik\builder\Form;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

<?php $form = ActiveForm::begin(['type' => ActiveForm::TYPE_HORIZONTAL]);
echo Form::widget([
	'model'      => $model,
	'form'       => $form,
	'columns'    => 1,
	'attributes' => [
		'role'                => [
			'label' => 'Nhóm thành viên',
			'type'  => Form::INPUT_DROPDOWN_LIST,
			'items' => User::ROLE,
		],
		'sponsor_id'           => [
			'label'       => 'Sponsor',
			'type'        => Form::INPUT_WIDGET,
			'widgetClass' => Select2::className(),
			'options'     => [
				'data'    => ArrayHelper::map(User::find()->all(), 'id', 'username'),
				'options' => ['prompt' => 'Chọn sponsor...'],
			],
		],
		'username'            => [
			'label'   => 'Tên đăng nhập',
			'type'    => Form::INPUT_TEXT,
			'options' => [
				'maxlength' => 255,
			],
		],
		'password'            => [
			'label'   => 'Mật khẩu',
			'type'    => Form::INPUT_PASSWORD,
			'options' => [
				'maxlength' => 255,
			],
		],
		'password_2'          => [
			'label'   => 'Mật khẩu cấp 2',
			'type'    => Form::INPUT_TEXT,
			'options' => [
				'maxlength' => 255,
			],
		],
		'full_name'            => [
			'label'   => 'Họ và tên',
			'type'    => Form::INPUT_TEXT,
			'options' => [
				'maxlength' => 255,
			],
		],
		'email'               => [
			'type'    => Form::INPUT_TEXT,
			'options' => [
				'maxlength' => 255,
			],
		],
		'level'          => [
			'label'   => 'Cấp độ',
			'type'    => Form::INPUT_TEXT,
			'options' => [
				'maxlength' => 255,
			],
		],
		'phone'               => [
			'label'   => 'Số ĐT',
			'type'    => Form::INPUT_TEXT,
			'options' => [
				'maxlength' => 15,
			],
		],
		'bank_name'           => [
			'label'   => 'Tên ngân hàng',
			'type'    => Form::INPUT_TEXT,
			'options' => [
				'maxlength' => 255,
			],
		],
		'bank_branch_name'    => [
			'label'   => 'Tên chi nhánh',
			'type'    => Form::INPUT_TEXT,
			'options' => [
				'maxlength' => 255,
			],
		],
		'bank_account_holder' => [
			'label'   => 'Tên chủ tài khoản',
			'type'    => Form::INPUT_TEXT,
			'options' => [
				'maxlength' => 255,
			],
		],
		'bank_account_number' => [
			'label'   => 'Số tài khoản',
			'type'    => Form::INPUT_TEXT,
			'options' => [
				'maxlength' => 255,
			],
		],
		'status'              => [
			'label' => 'Trạng thái',
			'type'  => Form::INPUT_DROPDOWN_LIST,
			'items' => User::STATUS,
		],
	],
]);
echo Html::submitButton('Lưu lại', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']);
ActiveForm::end(); ?>

</div>
