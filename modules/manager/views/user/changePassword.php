<?php
/**
 * Created by Navatech.
 * @project enterfund
 * @author  LocPX
 * @email   loc.xuanphama1t1[at]gmail.com
 * @date    10/16/2016
 * @time    3:36 PM
 */
/** @var $model app\models\form\ChangePasswordForm */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Change password'
?>
<div class="user-form">
	<?php $form = ActiveForm::begin([
		'layout' => 'horizontal',
	]) ?>
	<?= $form->field($model, 'current_password')->passwordInput() ?>
	<?= $form->field($model, 'new_password')->passwordInput() ?>
	<?= $form->field($model, 'new_password_confirm')->passwordInput() ?>
	<div class="col-sm-3">
		<?= Html::submitButton('Update Profile', [
			'class' => 'mws-button red pull-right',
			'name'  => 'login-button',
		]) ?>
	</div>
	<?php ActiveForm::end() ?>
</div>