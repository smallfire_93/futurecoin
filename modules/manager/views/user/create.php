<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\User $model
 */

$this->title = 'Thêm mới';
$this->params['breadcrumbs'][] = ['label' => 'Thành viên', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">
	<div class="page-header">
		<h1><?= Html::encode($this->title) ?></h1>
	</div>
	<?= $this->render('_form', [
		'model' => $model,
	]) ?>

</div>
