<?php
use app\models\User;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var yii\web\View                 $this
 * @var yii\data\ActiveDataProvider  $dataProvider
 * @var app\models\search\UserSearch $searchModel
 */
$this->title                   = 'Thành viên';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
	<div class="page-header">
		<h1><?= Html::encode($this->title) ?></h1>
	</div>
	<?php Pjax::begin();
	echo GridView::widget([
		'dataProvider' => $dataProvider,
		'filterModel'  => $searchModel,
		'export'       => false,
		'columns'      => [
			[
				'attribute' => 'id',
				'options'   => [
					'style' => 'width:70px',
				],
			],
			[
				'attribute'           => 'sponsor_id',
				'label'               => 'Người giới thiệu',
				'filterType'          => GridView::FILTER_SELECT2,
				'filterWidgetOptions' => [
					'data'    => ArrayHelper::map(User::find()->all(), 'id', 'username'),
					'options' => [
						'placeholder' => 'Vui lòng chọn',
						'allowClear'  => true,
					],
				],
				'value'               => function (User $data) {
					if($data->sponsor_id != "") {
						return ($data->sponsors) ? $data->sponsors->username : "";
					}
					return 'Không có';
				},
			],
			[
				'attribute'           => 'parent_id',
				'label'               => 'Tuyến trên',
				'filterType'          => GridView::FILTER_SELECT2,
				'filterWidgetOptions' => [
					'data'    => ArrayHelper::map(User::find()->all(), 'id', 'username'),
					'options' => [
						'placeholder' => 'Vui lòng chọn',
						'allowClear'  => true,
					],
				],
				'value'               => function (User $data) {
					if($data->parent_id != "") {
						return ($data->sponsors) ? $data->sponsors->username : "";
					}
					return 'Không có';
				},
			],
			[
				'attribute' => 'username',
				'label'     => 'Tài khoản',
			],
//			[
//				'attribute' => 'level',
//				'label'     => 'Cấp độ',
//			],
			[
				'attribute' => 'phone',
				'label'     => 'Điện thoại',
			],
//			[
//				'attribute' => 'status',
//				'label'     => 'Trạng thái',
//				'filter'    => [
//					'Chưa kích hoạt',
//					'Đã kích hoạt',
//					'Bị khóa',
//				],
//				'value'     => function (User $data) {
//					switch($data->status) {
//						case 0:
//							return "Chưa kích hoạt";
//						case 1:
//							return "Đã kích hoạt";
//						case 2:
//							return "Bị khóa";
//						default:
//							return "Chưa kích hoạt";
//					}
//				},
//			],
//			[
//				'attribute' => 'last_activated',
//				'label'     => 'Ngày năng động',
//				'format'    => [
//					'date',
//					'php:Y-m-d H:i:s',
//				],
//			],
			[
				'attribute' => 'last_login_at',
				'label'     => 'Lần cuối đăng nhập',
				'format'    => [
					'date',
					'php:Y-m-d H:i:s',
				],
			],
			[
				'class'    => 'yii\grid\ActionColumn',
				'template' => '{view}{update}{delete}',
//				'buttons'  => [
//					'login' => function ($url, User $model) {
//						return '<a href="' . \yii\helpers\Url::to([
//							'/site/impersonate',
//							'username' => $model->username,
//						]) . '" data-method="POST" data-confirm="Bạn có chắc sẽ đăng nhập vào tài khoản này?"><span class="glyphicon glyphicon-log-in"></span></a>';
//					},
//				],
			],
		],
		'responsive'   => true,
		'hover'        => true,
		'condensed'    => true,
		'floatHeader'  => true,
		'panel'        => [
			'heading'    => '<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> ' . Html::encode($this->title) . ' </h3>',
			'type'       => 'info',
			'before'     => Html::a('<i class="glyphicon glyphicon-plus"></i> Add', ['create'], ['class' => 'btn btn-success']),
			'after'      => Html::a('<i class="glyphicon glyphicon-repeat"></i> Reset List', ['index'], ['class' => 'btn btn-info']),
			'showFooter' => false,
		],
	]);
	Pjax::end(); ?>

</div>
