<?php
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\helpers\Json;

/* @var $this yii\web\View */
/* @var $model app\models\User */
$this->title                   = 'Báo cáo thành viên';
$this->params['breadcrumbs'][] = [
	'label' => 'Thành viên',
	'url'   => ['index'],
];
$this->params['breadcrumbs'][] = [
	'label' => 'Báo cáo',
	'url'   => [
		'report',
	],
];
$this->params['breadcrumbs'][] = 'Update';
?>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<div class="page-header">
	<h1>
		<?= $this->title ?>
	</h1>
</div>
<?= Alert::widget(); ?>
<div class="row">
	<div class="col-sm-12">
		<div class="widget-box transparent">
			<div class="widget-header widget-header-flat">
				<h4 class="widget-title lighter">
					<i class="ace-icon fa fa-user"></i>
					Thống kê số lương thành viên
				</h4>

				<div class="widget-toolbar">
					<a href="#" data-action="collapse">
						<i class="ace-icon fa fa-chevron-up"></i>
					</a>
				</div>
			</div>

			<div class="widget-body">
				<div class="widget-main padding-4">
					<div class="statistic">
						<div id="user-statistic"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(function() {
		$('#user-statistic').highcharts({
			chart      : {
				type: 'area'
			},
			title      : {
				text: false
			},
			subtitle   : {
				text: false
			},
			xAxis      : {
				categories       : <?=Json::encode($user['categories'])?>,
				tickmarkPlacement: 'on',
				title            : {
					enabled: false
				}
			},
			yAxis      : {
				title : {
					text: 'Thành viên'
				},
				labels: {
					formatter: function() {
						return this.value / 1000;
					}
				}
			},
			tooltip    : {
				shared     : true,
				valueSuffix: ' Thành viên'
			},
			plotOptions: {
				area: {
					stacking : 'normal',
					lineColor: '#666666',
					lineWidth: 1,
					marker   : {
						lineWidth: 1,
						lineColor: '#666666'
					}
				}
			},
			series     : <?=Json::encode($user['series'])?>
		});
	});
</script>
