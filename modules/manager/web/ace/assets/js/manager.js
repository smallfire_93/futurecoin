/**
 * Created by Pham Xuan Loc on 2/24/2016.
 */
var initPreview      = function(selector) {
	$("#" + selector).change(function() {
		var reader    = new FileReader();
		reader.onload = imageIsLoaded;
		reader.readAsDataURL(this.files[0]);
	});
	function imageIsLoaded(e) {
		$('#' + selector + '_preview_image').attr('src', e.target.result).attr('width', '100px');
	}
};
var initUpload       = function() {
	$(document).on("ready", function() {
		$('.file-picker').ace_file_input({
			no_file      : 'No File ...',
			btn_choose   : 'Choose',
			btn_change   : 'Change',
			droppable    : true,
			onchange     : null,
			thumbnail    : 'small',
			before_change: function(files, dropped) {
				var allowed_files = [];
				for(var i = 0; i < files.length; i++) {
					var file = files[i];
					if(typeof file === "string") {
						if(!(/\.(jpe?g|png|gif|bmp|ico)$/i).test(file)) {
							return false;
						}
					}
					else {
						var type = $.trim(file.type);
						if(( type.length > 0 && !(/^image\/(jpe?g|png|gif|bmp|ico)$/i).test(type) )
							|| ( type.length == 0 && !(/\.(jpe?g|png|gif|bmp|ico)$/i).test(file.name) )
						) {
							alert('Invalid file type.');
							continue;
						}
					}
					allowed_files.push(file);
				}
				if(allowed_files.length == 0) {
					return false;
				}
				return allowed_files;
			}
		});
	});
};

