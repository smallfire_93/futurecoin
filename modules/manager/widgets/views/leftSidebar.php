<?php
use yii\widgets\Menu;

?>
<div id="sidebar" class="sidebar  responsive">
	<script type="text/javascript">
		try {
			ace.settings.check('sidebar', 'fixed')
		} catch(e) {
		}
	</script>

	<div class="sidebar-shortcuts" id="sidebar-shortcuts">
		<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
			<button class="btn btn-success">
				<i class="ace-icon fa fa-signal"></i>
			</button>

			<button class="btn btn-info">
				<i class="ace-icon fa fa-pencil"></i>
			</button>

			<button class="btn btn-warning">
				<i class="ace-icon fa fa-users"></i>
			</button>

			<button class="btn btn-danger">
				<i class="ace-icon fa fa-cogs"></i>
			</button>
		</div>

		<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
			<span class="btn btn-success"></span>

			<span class="btn btn-info"></span>

			<span class="btn btn-warning"></span>

			<span class="btn btn-danger"></span>
		</div>
	</div><!-- /.sidebar-shortcuts -->

	<?php
	echo Menu::widget([
		'items'           => [
			[
				'label'    => '	<i class="menu-icon fa fa-tachometer"></i>
				<span class="menu-text"> Dashboard </span>',
				'url'      => ['site/index'],
				'template' => '<a href="{url}" class="" style="">{label}</a><b class="arrow"></b>',
			],
			[
				'label'   => '<i class="menu-icon fa fa-user"></i>
							<span class="menu-text">
								Thành viên
							</span>

				<b class="arrow fa fa-angle-down"></b>',
				'url'     => '#',
				'options' => ['class' => ''],
				'items'   => [
					[
						'label'    => '<i class="menu-icon fa fa-caret-right"></i>Danh sách',
						'url'      => ['user/index'],
						'template' => '<a href="{url}" class="" style="">{label}</a><b class="arrow"></b>',
					],
					//					[
					//						'label'    => '<i class="menu-icon fa fa-caret-right"></i>Thêm mới',
					//						'url'      => ['user/create'],
					//						'template' => '<a href="{url}" class="" style="">{label}</a><b class="arrow"></b>',
					//					],
					[
						'label'    => '<i class="menu-icon fa fa-caret-right"></i>Thống kê',
						'url'      => ['user/report'],
						'template' => '<a href="{url}" class="" style="">{label}</a><b class="arrow"></b>',
					],
				],
			],
			[
				'label' => '<i class="menu-icon fa fa-credit-card"></i><span class="menu-text"> Kích hoạt bằng tay </span> 
			<b class="arrow fa fa-angle-down"></b>',
				'url'     => ['package/cash-package'],
				'options' => ['class' => ''],
				'items'   => [
					[
						'label'    => '<i class="menu-icon fa fa-caret-right"></i>Kích bitcoin',
						'url'      => ['package/cash-package'],
						'template' => '<a href="{url}" class="" style="">{label}</a><b class="arrow"></b>',
					],
				],
			],
			[
				'label'   => '<i class="menu-icon fa fa-credit-card"></i><span class="menu-text"> Quản lý gói </span> 
				<b class="arrow fa fa-angle-down"></b>',
				'url'     => '#',
				'options' => ['class' => ''],
				'items'   => [
					[
						'label'    => '<i class="menu-icon fa fa-caret-right"></i>Danh sách gói',
						'url'      => ['package/index'],
						'template' => '<a href="{url}" class="" style="">{label}</a><b class="arrow"></b>',
					],
					[
						'label'    => '<i class="menu-icon fa fa-caret-right"></i>Thêm mới gói',
						'url'      => ['package/create'],
						'template' => '<a href="{url}" class="" style="">{label}</a><b class="arrow"></b>',
					],
				],
			],
			[
				'label'   => '<i class="menu-icon fa fa-credit-card"></i><span class="menu-text"> Quản lý liên hệ</span> 
				<b class="arrow fa fa-angle-down"></b>',
				'url'     => '#',
				'options' => ['class' => ''],
				'items'   => [
					[
						'label'    => '<i class="menu-icon fa fa-caret-right"></i>Danh sách liên hệ',
						'url'      => ['support/index'],
						'template' => '<a href="{url}" class="" style="">{label}</a><b class="arrow"></b>',
					],
					//					[
					//						'label'    => '<i class="menu-icon fa fa-caret-right"></i>Gửi email',
					//						'url'      => ['buy-pin/index'],
					//						'template' => '<a href="{url}" class="" style="">{label}</a><b class="arrow"></b>',
					//					],
				],
			],
			[
				'label'   => '<i class="menu-icon fa fa-book"></i><span class="menu-text"> Báo cáo doanh số </span> 
				<b class="arrow fa fa-angle-down"></b>',
				'url'     => '#',
				'options' => ['class' => ''],
				'items'   => [
					[
						'label'    => '<i class="menu-icon fa fa-caret-right"></i> Danh sách',
						'url'      => ['transaction/index'],
						'template' => '<a href="{url}" class="" style="">{label}</a><b class="arrow"></b>',
					],
				],
			],
			[
				'label'   => '<i class="menu-icon fa fa-book"></i><span class="menu-text"> Quản lý trả thưởng </span> 
				<b class="arrow fa fa-angle-down"></b>',
				'url'     => '#',
				'options' => ['class' => ''],
				'items'   => [
					[
						'label'    => '<i class="menu-icon fa fa-caret-right"></i> Danh sách',
						'url'      => ['transaction/withdraw'],
						'template' => '<a href="{url}" class="" style="">{label}</a><b class="arrow"></b>',
					],
				],
			],
			[
				'label'   => '<i class="menu-icon fa fa-book"></i><span class="menu-text"> Quản lý KYC </span> 
				<b class="arrow fa fa-angle-down"></b>',
				'url'     => '#',
				'options' => ['class' => ''],
				'items'   => [
					[
						'label'    => '<i class="menu-icon fa fa-caret-right"></i> Danh sách',
						'url'      => ['kyc/index'],
						'template' => '<a href="{url}" class="" style="">{label}</a><b class="arrow"></b>',
					],
				],
			],
			//			[
			//				'label'   => '<i class="menu-icon fa fa-cog"></i><span class="menu-text"> Hệ thống </span><b class="arrow fa fa-angle-down"></b>',
			//				'url'     => '#',
			//				'options' => ['class' => ''],
			//				'items'   => [
			//					[
			//						'label'    => '<i class="menu-icon fa fa-caret-right"></i>Cấu hình hệ thống',
			//						'url'      => ['manager/setting'],
			//						'template' => '<a href="{url}" class="" style="">{label}</a><b class="arrow"></b>',
			//					],
			//				],
			//			],
		],
		'encodeLabels'    => false,
		'submenuTemplate' => "\n<ul class='submenu' style='display: none'>\n{items}\n</ul>\n",
		'options'         => array('class' => 'nav nav-list'),
		'linkTemplate'    => '<a href="{url}" class="dropdown-toggle" style="">{label}</a><b class="arrow"></b>',
		'activeCssClass'  => 'active nav-show open',
		'activateParents' => true,
	]);
	?>

	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
	</div>

	<script type="text/javascript">
		try {
			ace.settings.check('sidebar', 'collapsed')
		} catch(e) {
		}
	</script>
</div>