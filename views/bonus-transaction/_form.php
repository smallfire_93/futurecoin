<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BonusTransaction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bonus-transaction-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'receipted_date')->textInput() ?>

    <?= $form->field($model, 'total_bonus')->textInput() ?>

    <?= $form->field($model, 'cash')->textInput() ?>

    <?= $form->field($model, 'trading')->textInput() ?>

    <?= $form->field($model, 'direct_bonus')->textInput() ?>

    <?= $form->field($model, 'matching_bonus')->textInput() ?>

    <?= $form->field($model, 'weak_branch_bonus')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
