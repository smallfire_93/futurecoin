<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\BonusTransactionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="bonus-transaction-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'receipted_date') ?>

    <?= $form->field($model, 'total_bonus') ?>

    <?= $form->field($model, 'cash') ?>

    <?php // echo $form->field($model, 'trading') ?>

    <?php // echo $form->field($model, 'direct_bonus') ?>

    <?php // echo $form->field($model, 'matching_bonus') ?>

    <?php // echo $form->field($model, 'weak_branch_bonus') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
