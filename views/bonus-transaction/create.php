<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BonusTransaction */

$this->title = 'Create Bonus Transaction';
$this->params['breadcrumbs'][] = ['label' => 'Bonus Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bonus-transaction-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
