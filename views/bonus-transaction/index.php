<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\BonusTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bonus Transactions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bonus-transaction-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Bonus Transaction', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_id',
            'receipted_date',
            'total_bonus',
            'cash',
            // 'trading',
            // 'direct_bonus',
            // 'matching_bonus',
            // 'weak_branch_bonus',

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
