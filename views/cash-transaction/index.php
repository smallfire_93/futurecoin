<?php
use app\models\CashTransaction;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CashTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Cash History';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cash-transaction-index">

	<div class="panel panel-success">
		<div class="panel-heading">MY cash account</div>
		<div class="panel-body">
			<p>Cash account: € <?= number_format($searchModel->user->accounts->cash_account) ?></p>
		</div>
	</div>
	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
	</p>
	<?php
	echo Html::a('Send cash to upline', ['/cash-transaction/send-upline'], ['class' => 'btn btn-warning']);
	echo Html::a('Send cash to downline', ['/cash-transaction/send-downline'], ['class' => 'btn btn-success']);
	echo Html::a('Request Withdrawal', ['/cash-transaction/withdraw'], ['class' => 'btn btn-danger'])
	?>
	<div class="table-responsive">
		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel'  => $searchModel,
			//		'tableOptions' => ['class' => 'table-responsive'],
			'columns'      => [
				['class' => 'yii\grid\SerialColumn'],
				[
					'attribute' => 'type',
					'value'     => function (CashTransaction $data) {
						return $data::TYPE[$data->type];
					},
					'filter'    => $searchModel::TYPE,
				],
				[
					'attribute' => 'user_id',
					'value'     => function (CashTransaction $data) {
						return $data->user_id != null ? $data->users->username : null;
					},
					'filter'    => false,
				],
				[
					'attribute' => 'receive_id',
					'value'     => function (CashTransaction $data) {
						return $data->receive_id != null ? $data->receiver->username : null;
					},
					'filter'    => false,
				],
				[
					'attribute' => 'money',
					'value'     => function (CashTransaction $data) {
						return $data->money;
					},
				],
				[
					'attribute' => 'status',
					'value'     => function (CashTransaction $data) {
						return $data::STATUS[$data->status];
					},
					'filter'    => $searchModel::STATUS,
				],
				'date'
				// 'date',
				//			['class' => 'yii\grid\ActionColumn'],
			],
		]); ?>
	</div>
</div>
