<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CashTransaction */
$this->title                   = 'Send cash to downline';
$this->params['breadcrumbs'][] = [
	'label' => 'Send downline',
	'url'   => ['send-downline'],
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cash-transaction-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_send', [
		'model'    => $model,
		'children' => $children,
	]) ?>

</div>