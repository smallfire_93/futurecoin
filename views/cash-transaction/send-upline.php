<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CashTransaction */
$this->title                   = 'Send cash to upline';
$this->params['breadcrumbs'][] = [
	'label' => 'Send upline',
	'url'   => ['send-upline'],
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cash-transaction-create">

	<h1><?= Html::encode($this->title) ?></h1>

	<?= $this->render('_send', [
		'model'    => $model,
		'children' => $children,
	]) ?>

</div>