<?php
/**
 * Created by PhpStorm.
 * User: Yamon-PC
 * Date: 31-May-17
 * Time: 4:18 PM
 */
use yii\helpers\Url;

?>
<?= \yii\bootstrap\Html::a('Bitcoin withdrawal', \yii\helpers\Url::to([
	'cash-transaction/withdraw',
	'type' => 1,
]), ['class' => 'btn btn-warning']) ?>
<?= \yii\bootstrap\Html::a('Bank withdrawal', '#', ['class' => 'btn btn-success']) ?>
<?= \yii\bootstrap\Html::a('Perfect money withdrawal', '#', ['class' => 'btn btn-info']) ?>
