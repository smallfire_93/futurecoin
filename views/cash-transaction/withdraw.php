<?php
use app\models\User;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\form\SendCashForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="send-cash-form">
	<?php $form = ActiveForm::begin([
		'layout'               => 'horizontal',
		'enableAjaxValidation' => true,
	]); ?>
	<?= $form->field($model, 't_password')->passwordInput() ?>
	<?= $form->field($model, 'money')->textInput() ?>
	<div class="col-sm-offset-3">
		<?= Html::submitButton('Send', ['class' => 'btn btn-primary']) ?>
	</div>

	<?php ActiveForm::end(); ?>
</div>