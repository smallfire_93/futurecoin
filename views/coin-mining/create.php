<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CoinMining */

$this->title = 'Create Coin Mining';
$this->params['breadcrumbs'][] = ['label' => 'Coin Minings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coin-mining-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
