<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CoinMiningSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Coin Minings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coin-mining-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<div class="table-responsive">

		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			'columns'      => [
				['class' => 'yii\grid\SerialColumn'],
				'id',
				'start_date',
				'begin_return_coin',
				'quantity',
			],
		]); ?>
	</div>
</div>