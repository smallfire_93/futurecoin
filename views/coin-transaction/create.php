<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CoinTransaction */

$this->title = 'Create Coin Transaction';
$this->params['breadcrumbs'][] = ['label' => 'Coin Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coin-transaction-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
