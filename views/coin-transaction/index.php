<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CoinTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Coin Transactions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coin-transaction-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
		<?= Html::a('Create Coin Transaction', ['create'], ['class' => 'btn btn-success']) ?>
	</p>
	<div class="table-responsive">

		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel'  => $searchModel,
			'columns'      => [
				['class' => 'yii\grid\SerialColumn'],
				'id',
				'user_id',
				'type',
				'money',
				'status',
				// 'quantity',
				// 'date',
			],
		]); ?>
	</div>
</div>
