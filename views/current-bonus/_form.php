<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CurrentBonus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="current-bonus-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'current_cash')->textInput() ?>

    <?= $form->field($model, 'current_trading')->textInput() ?>

    <?= $form->field($model, 'current_bv_left')->textInput() ?>

    <?= $form->field($model, 'current_bv_right')->textInput() ?>

    <?= $form->field($model, 'weak_branch')->textInput() ?>

    <?= $form->field($model, 'weak_branch_bv')->textInput() ?>

    <?= $form->field($model, 'total_bonus')->textInput() ?>

    <?= $form->field($model, 'direct_bonus')->textInput() ?>

    <?= $form->field($model, 'matching_bonus')->textInput() ?>

    <?= $form->field($model, 'weak_branch_bonus')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
