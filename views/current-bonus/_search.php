<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\CurrentBonusSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="current-bonus-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'current_cash') ?>

    <?= $form->field($model, 'current_trading') ?>

    <?= $form->field($model, 'current_bv_left') ?>

    <?php // echo $form->field($model, 'current_bv_right') ?>

    <?php // echo $form->field($model, 'weak_branch') ?>

    <?php // echo $form->field($model, 'weak_branch_bv') ?>

    <?php // echo $form->field($model, 'total_bonus') ?>

    <?php // echo $form->field($model, 'direct_bonus') ?>

    <?php // echo $form->field($model, 'matching_bonus') ?>

    <?php // echo $form->field($model, 'weak_branch_bonus') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
