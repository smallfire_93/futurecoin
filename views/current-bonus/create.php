<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CurrentBonus */

$this->title = 'Create Current Bonus';
$this->params['breadcrumbs'][] = ['label' => 'Current Bonuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="current-bonus-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
