<?php
use app\models\PackageTransaction;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CurrentBonusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Current Bonus';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="current-bonus-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<div class="panel panel-info">
		<div class="panel-heading">Current Bonus</div>
		<div class="panel-body font-bonus">
			<div class="col-sm-6"><span style="font-weight: bold">Direct bonus: </span><?= $direct ?> </div>
			<div class="col-sm-6"><span style="font-weight: bold">Weak branch bonus: </span><?= $branch_bonus ?></div>
			<div class="col-sm-6"><span style="font-weight: bold">Matching bonus: </span><?= $matching_bonus ?></div>
			<div class="col-sm-6"><span style="font-weight: bold">Current BV left: </span><?= $bv_left ?></div>
			<div class="col-sm-6"><span style="font-weight: bold">Current BV right: </span><?= $bv_right ?></div>
			<div class="col-sm-6"><span style="font-weight: bold">Current Total bonus: </span><?= $total_bonus ?></div>
			<div class="col-sm-6"><span style="font-weight: bold">Current Level: </span><?= $current_level ?></div>
		</div>
	</div>
	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#package">Package history</a></li>
		<li><a data-toggle="tab" href="#bonus">Bonus history</a></li>
	</ul>

	<div class="tab-content">

		<div id="package" class="tab-pane fade in active table-responsive">
			<?php Pjax::begin() ?>
			<?= GridView::widget([
				'dataProvider' => $dataProvider,
				'columns'      => [
					['class' => 'yii\grid\SerialColumn'],
					[
						'attribute' => 'package_id',
						'value'     => function (PackageTransaction $data) {
							return $data->package->name;
						},
					],
					[
						'attribute' => 'user_id',
						'value'     => function (PackageTransaction $data) {
							return $data->users->username;
						},
					],
					[
						'attribute' => 'branch',
						'value'     => function (PackageTransaction $data) {
							return $data->returnBranch();
						},
					],
					[
						'attribute' => 'tree_position',
						'value'     => function (PackageTransaction $data) {
							return $data->user->getTreePosition($data->user_id);
						},
					],
					['attribute' => 'package.money'],
					[
						'attribute' => 'package.money',
						'header'    => 'BV',
					],
					[
						'attribute' => 'type',
						'value'     => function (PackageTransaction $data) {
							return $data::TYPE[$data->type];
						},
					],
					'created_date',
				],
			]); ?>
			<?php Pjax::end() ?>

		</div>
		<div id="bonus" class="tab-pane fade table-responsive">
			<?= GridView::widget([
				'dataProvider' => $bonusData,
				'columns'      => [
					['class' => 'yii\grid\SerialColumn'],
					'id',
					'receipted_date',
					'total_bonus',
					'cash',
					'trading',
					'direct_bonus',
					'matching_bonus',
					'weak_branch_bonus',
//					['class' => 'yii\grid\ActionColumn'],
				],
			]); ?>
		</div>
	</div>

</div>
