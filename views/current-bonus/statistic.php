<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 5/2/2017
 * Time: 5:56 PM
 */
use yii\grid\GridView;

?>
<?= GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'columns' => [
		['class' => 'yii\grid\SerialColumn'],

		'id',
		'user_id',
		'current_cash',
		'current_trading',
		'current_bv_left',
		// 'current_bv_right',
		// 'weak_branch',
		// 'weak_branch_bv',
		// 'total_bonus',
		// 'direct_bonus',
		// 'matching_bonus',
		// 'weak_branch_bonus',

		['class' => 'yii\grid\ActionColumn'],
	],
]); ?>