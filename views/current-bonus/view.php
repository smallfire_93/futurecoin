<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CurrentBonus */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Current Bonuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="current-bonus-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            'current_cash',
            'current_trading',
            'current_bv_left',
            'current_bv_right',
            'weak_branch',
            'weak_branch_bv',
            'total_bonus',
            'direct_bonus',
            'matching_bonus',
            'weak_branch_bonus',
        ],
    ]) ?>

</div>
