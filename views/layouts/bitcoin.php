<?php
/* @var $this \yii\web\View */
/* @var $content string */
use app\assets\BitcoinAsset;
use yii\bootstrap\Html;

BitcoinAsset::register($this)
?>
<?php
$this->beginPage() ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
		"http://www.w3.org/TR/html4/loose.dtd">
<html lang="<?= Yii::$app->language ?>">
<head>
	<head>
		<title> Pay-Per-Product Cryptocoin Payment </title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv='cache-control' content='no-cache'>
		<meta http-equiv='Expires' content='-1'>
		<meta name='robots' content='all'>
	<?php $this->head() ?>
</head>
<body style='font-family:Arial,Helvetica,sans-serif;font-size:13px;color:#666;margin:0'>
<?php $this->beginBody() ?>
<?= $content ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

