<?php
/* @var $this \yii\web\View */
/* @var $content string */
use app\components\widgets\LeftSidebar;
use app\components\widgets\TopBar;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?= Html::csrfMetaTags() ?>
	<title><?= Html::encode($this->title) ?></title>
	<?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?= TopBar::widget() ?>
<div class="clearfix">
</div>
<div class="page-container">
	<?= LeftSidebar::widget() ?>
	<div class="page-content-wrapper">
		<div class="page-content clearfix">
			<?= $content ?>
		</div>
	</div>
</div>
<footer class="footer">
	<div class="container">
		<p class="pull-left">&copy; future-coin.org <?= date('Y') ?></p>
	</div>
</footer>
<?php $this->endBody() ?>
</body>
<script>
	jQuery(document).ready(function() {
		Metronic.init(); // init metronic core componets
		Layout.init(); // init layout
		QuickSidebar.init(); // init quick sidebar
		Demo.init(); // init demo features
		Index.init();
		//		Index.initDashboardDaterange();
		//		Index.initJQVMAP(); // init index page's custom scripts
		//		Index.initCalendar(); // init index page's custom scripts
		//		Index.initCharts(); // init index page's custom scripts
		//		Index.initChat();
		//		Index.initMiniCharts();
		//		Tasks.initDashboardWidget();
	});
	$('.goog-te-menu-frame').contents().find('.goog-te-menu2').css(
		{
			'max-width' : '100%',
			'overflow'  : 'scroll',
			'box-sizing': 'border-box',
			'height'    : 'auto'
		}
	);
//	function changeGoogleStyles() {
//		if($('.goog-te-menu-frame').contents().find('.goog-te-menu2').length) {
//			$('.goog-te-menu-frame').contents().find('.goog-te-menu2').css({
//				'max-width' : '100%',
//				'overflow'  : 'scroll',
//				'box-sizing': 'border-box',
//				'height'    : 'auto'
//			});
//		} else {
//			setTimeout(changeGoogleStyles, 50);
//		}
//	}
//	changeGoogleStyles();
	function changeGoogleStyles() {
		if(($goog = $('.goog-te-menu-frame').contents().find('body')).length) {
			var stylesHtml = '<style>' +
				'.goog-te-menu2 {' +
				'max-width:100% !important;' +
				'overflow:scroll !important;' +
				'box-sizing:border-box !important;' +
				'height:auto !important;' +
				'-webkit-overflow-scrolling:touch!important;' +
				'}' +
				'</style>';
			$goog.prepend(stylesHtml);
		} else {
			setTimeout(changeGoogleStyles, 50);
		}
	}
	changeGoogleStyles();
	//		$('#google_translate_element').on("ready", function() {
	//
	//			// Change font family and color
	//			$("iframe").contents().find(".goog-te-menu2-item div, .goog-te-menu2-item:link div, .goog-te-menu2-item:visited div, .goog-te-menu2-item:active div, .goog-te-menu2 *")
	//				.css({
	//					'color'      : '#544F4B',
	//					'font-family': 'tahoma',
	//				});
	//			iframe.style.display = '';
	//			iframe.style.height  = '';
	//			iframe.style.width   = '99%!important';
	//			// Change hover effects
	//			$("iframe").contents().find(".goog-te-menu2-item div").hover(function() {
	//				$(this).css('background-color', '#F38256').find('span.text').css('color', 'white');
	//			}, function() {
	//				$(this).css('background-color', 'white').find('span.text').css('color', '#544F4B');
	//			});
	//			$("iframe").css({
	//				'width': '100%'
	//			});
	//			// Change Google's default blue border
	//			$("iframe").contents().find('.goog-te-menu2').css('border', '1px solid #F38256');
	//
	//			// Change the iframe's box shadow
	//			$(".goog-te-menu-frame").css({
	//				'-moz-box-shadow'   : '0 3px 8px 2px #666666',
	//				'-webkit-box-shadow': '0 3px 8px 2px #666',
	//				'box-shadow'        : '0 3px 8px 2px #666'
	//
	//			});
	//		});
	//	function changeGoogleStyles() {
	//		if(($goog = $('.goog-te-menu-frame').contents().find('body')).length) {
	//			var stylesHtml = '<style>' +
	//				'.goog-te-menu2 {' +
	//				'width:100% !important;' +
	//				'overflow:auto !important;' +
	//				'box-sizing:border-box !important;' +
	//				'height:auto !important;' +
	//				'-webkit-overflow-scrolling:touch!important;' +
	//				'}' +
	//				'</style>';
	//			$goog.prepend(stylesHtml);
	//		} else {
	//			setTimeout(changeGoogleStyles, 50);
	//		}
	//	}
	//	changeGoogleStyles();
</script>
</html>
<?php $this->endPage() ?>
