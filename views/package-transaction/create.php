<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PackageTransaction */

$this->title = 'Create Package Transaction';
$this->params['breadcrumbs'][] = ['label' => 'Package Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="package-transaction-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
