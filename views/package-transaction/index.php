<?php
use app\models\PackageTransaction;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PackageTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'My Package';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if($package){ ?>
<div class="package-transaction-index">
	<div class="panel panel-success">
		<div class="panel-heading">MY cash account</div>
		<div class="panel-body">
			<div class="col-sm-3 package" style="margin-top: 10px;">
				<div class="box">
					<div class="box-hd">
						€ <?= number_format($package->money) ?>
					</div>
					<div style="text-align:center;">
						<img src="<?= $package->getPictureUrl('image') ?>" style="max-width:210px; width: auto; height:170px;">
					</div>
					<ul>
						<li><?= $package->name ?></li>
					</ul>
					<input type="hidden" name="bv_value" id="bv_value" value="20">
					<input type="hidden" name="product_value" id="product_value" value="25">
					<input type="hidden" name="tokens" id="tokens" value="200">
					<input type="hidden" name="product_id" id="product_id" value="2">
					<input type="hidden" name="user_id" id="user_id" value="699213">

				</div>
			</div>
		</div>
	</div>
	<?php } ?>
	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

	<p>
	</p>
	<div class="table-responsive">

		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			'columns'      => [
				['class' => 'yii\grid\SerialColumn'],
				[
					'attribute' => 'package_id',
					'value'     => function (PackageTransaction $data) {
						return $data->package->name;
					},
				],
				[
					'attribute' => 'type',
					'value'     => function (PackageTransaction $data) {
						return $data::TYPE[$data->type];
					},
				],
				'created_date',
				[
					'attribute' => 'available_date',
					'value'     => function (PackageTransaction $data) {
						$active_date    = strtotime($data->created_date);
						$available_date = strtotime('+54 days', $active_date);
						return $available_date = date('Y-m-d H:i:s', $available_date);
					},
				],
			],
		]); ?>
	</div>
</div>
