<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PackageTransaction */

$this->title = 'Update Package Transaction: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Package Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="package-transaction-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
