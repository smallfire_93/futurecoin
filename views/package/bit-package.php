<?php
/* @var $this \yii\web\View */
/* @var $box \app\models\Cryptobox */
use app\assets\BitcoinAsset;
use yii\helpers\Url;

BitcoinAsset::register($this)
?>
<div align='center'>
	<!--<div style='width:100%;height:auto;line-height:50px;background-color:#f1f1f1;border-bottom:1px solid #ddd;color:#49abe9;font-size:18px;'>-->
	<!--	1. GoUrl <b>Pay-Per-Product</b> Example (--><?php //echo $coinName; ?><!-- payments). Use it on your website. -->
	<!--	<div style='float:right;'><a style='font-size:15px;color:#389ad8;margin-right:20px' href='https://github.com/cryptoapi/Payment-Gateway/blob/master/Examples/pay-per-product.php'>View Source</a><a style='font-size:15px;color:#389ad8;margin-right:20px' href='--><?php //echo "//".$_SERVER["HTTP_HOST"].str_replace(".php", "-multi.php", $_SERVER["REQUEST_URI"]); ?><!--'>Multiple Crypto</a><a style='font-size:15px;color:#389ad8;margin-right:20px' href='https://gourl.io/--><?php //echo strtolower($coinName); ?><!---payment-gateway-api.html'>Other Examples</a></div>-->
	<!--	-->
	<!--</div>-->

	<!--<h1>Example - Customer Invoice</h1>-->
	<!--<br>-->
	<!--<img style='position:absolute;margin-left:auto;margin-right:auto;left:0;right:0;' alt='status' src='https://gourl.io/images/--><?php //echo ($box->is_paid()?"paid":"unpaid"); ?><!--.png'>-->
	<!--<img alt='Invoice' border='0' height='500' src='https://gourl.io/images/invoice.png'>-->
	<!---->
	<!--<br><br>-->
	<?php if(!$box->is_paid()) {
		echo "<h2>Pay package - $package_name </h2>";
	} else {
		echo "<br><br>";
	} ?>
	<div style='margin:30px 0 5px 300px'>Language: &#160; <?php echo $languages_list; ?></div>
	<?php echo $box->display_cryptobox(false, 580, 230); ?>
	<br><br><br>
	<!--	<h3>Message :</h3>-->
	<!--	<h2 style='color:#999'>--><?php //echo $message; ?><!--</h2>-->

</div>
<!--<br><br><br><br><br><br>-->
<!--<div style='position:absolute;left:0;'>-->
<!--	<a target="_blank" href="http://validator.w3.org/check?uri=--><?php //echo "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?><!--"><img src="https://gourl.io/images/w3c.png" alt="Valid HTML 4.01 Transitional"></a>-->
<!--</div>-->
<script>
	var interval = 2000;
	function doAjax() {
		$.ajax({
				type    : 'POST',
				url     : "<?=\yii\helpers\Url::to([
					'package/bit-package',
					'id'       => $package->id,
					'order_id' => $order_id,
				])?>",
				async   : false,
				data    : {
					check: 1
				},
				dataType: 'json',
				success : function(data) {
					//				$('#hidden').val(data);// first set the value
					console.log(data);
					if(data == 1) {
						location.href = "<?=Url::to(['package-transaction/index'])?>";
					} else {
						if(data == 2) {
							setTimeout(doAjax, interval);

						}

					}

				}
			}
		)
		;
	}
	setTimeout(doAjax, interval);
</script>
