<?php
/**
 * Created by PhpStorm.
 * User: Yamon-PC
 * Date: 28-Mar-17
 * Time: 7:13 PM
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;

?>
<?php
$form = ActiveForm::begin([
	'id'                   => 'check-form',
	'action'               => Url::to([
		'/package/cash-package',
		'id' => $id,
	]),
	'layout'               => 'horizontal',
	'enableAjaxValidation' => true,
]);
Modal::begin([
	'header'       => '<h2>Cash account payment</h2>',
	'toggleButton' => [
		'label' => 'Cash account payment',
		'class' => 'btn btn-success col-sm-12',
	],
]);
echo $form->field($model, 't_password')->passwordInput();
echo Html::submitButton('Buy package', ['class' => 'btn btn-danger']);
Modal::end(); ?>

<?php
ActiveForm::end();
?>

