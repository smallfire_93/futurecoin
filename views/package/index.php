<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PackageSearch */
/* @var $packages app\models\Package */
/* @var $package app\models\Package */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Buying Packages';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="package-index">
	<h1><?= Html::encode($this->title) ?></h1>
	<?php foreach($packages as $package) { ?>
		<div class="col-sm-3 package" style="margin-top: 10px;">
			<div class="box">
				<div class="box-hd">
					€ <?= number_format($package->money) ?>
				</div>
				<div style="text-align:center;">
					<img src="<?= $package->getPictureUrl('image') ?>" style="width: 100%">
				</div>
				<ul>
					<li class="btn-success" style="font-weight: bold"><?= $package->name ?></li>
					<li><font color="#ff0000">Tokens</font>:<?= $package->token ?></li>
					<li>
					<?= Html::a('Buy package', [
						'package/product-purchase',
						'id' => $package->id,
					], ['class' => 'btn btn-info']) ?>
					</li>
				</ul>

				<!--			<button class="but" type="button" onclick="addToCart(2);" name="purchase" id="purchase" style="    border: 0px solid #000 !important;margin: 9px 10px 10px 0px;color: white;">Buy package</button>-->

			</div>
		</div>
	<?php } ?>

</div>
