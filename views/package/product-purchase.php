<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/23/2017
 * Time: 10:51 PM
 */
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this app\components\View */
/* @var $package app\models\Package */
/* @var $form yii\widgets\ActiveForm */
$this->title                   = 'Buying Packs';
$this->params['breadcrumbs'][] = [
	'label' => 'Buying Packs',
	'url'   => ['product-purchase'],
];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>
<div class="col-sm-12">
	<div class="clearfix col-sm-3">
		<?php
		echo $this->render('cash-package', [
			'model' => $model,
			'id'    => $package->id,
		]);
		?>
		<!--			<button type="button" class="btn btn-success">Cash account payment</button>-->
	</div>
	<div class="clearfix col-sm-3">
		<?php echo Html::a('Bitcoin payment', Url::to([
			'bit-package',
			'id' => $package->id,
		]), [
			'class'  => 'btn btn-warning col-sm-12',
			'target' => '_blank',
		]) ?>
	</div>
	<div class="clearfix col-sm-3">
		<button type="button" class="btn btn-success col-sm-12">Perfect money</button>
	</div>
	<div class="clearfix col-sm-3">
		<button type="button" class="btn btn-info col-sm-12">Bank wire</button>
	</div>
</div>
<div class="clearfix col-sm-12" style="margin-top: 15px">
	<div class="col-sm-4">
		<h3 class="text-center"><?= $package->name ?></h3>
		<?= Html::img($package->getPictureUrl('image'), ['class' => 'img-responsive img-rounded']) ?>
	</div>
	<div class="col-sm-4">
		<p></p>
		<p class="text-dangerous">Tokens: <?= $package->token ?></p>
		<p><strong>Price:€ <?= number_format($package->money) ?></p>
		<!--	-->
		<table class="table table-condensed">
			<thead>
			<tr>
				<th>Price</th>
				<th>Quantity</th>
				<th>Total</th>
			</tr>
			</thead>
			<tbody class="total-price">
			<tr>
				<td>€ <?= $package->money ?></td>
				<td id="quantity-update">1</td>
				<td class="total-update">€ <?= $package->money ?></td>
			</tr>

			<tr>
				<td colspan="2"><h4>Total</h4></td>
				<td><h4><strong class="total-update">€ <?= $package->money ?> </strong></h4></td>
			</tr>

			</tbody>
		</table>
	</div>
	<div class="col-sm-4 text-center">
		<!--	<h3>Choose payment method</h3>-->
		<!--	<hr>-->
		<div class="payment-buttons">
			<p class="bg-info">Use accounts</p>
			<p>Cash Account : <strong>€ <?= $this->user->accounts->cash_account ?> </strong></p>
		</div>
	</div>
</div>
<script>
</script>