<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/25/2017
 * Time: 4:52 PM
 */
use miloschuman\highcharts\Highcharts;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\grid\GridView;

?>
<?php
echo \miloschuman\highcharts\Highcharts::widget([
	'options' => '{
      "title": { "text": "Marketcoins price" },
      "xAxis": {
         "categories": ["1", "2", "3","4","5","6",7,8,9,10,11,12,13,14,15,16,17,18]
      },
      "yAxis": {
         "title": { "text": "Price" }
      },
      "series": [
         { "type":"area","name": "Day price", "data": [0.1, 0.11, 0.12, 0.13, 0.14, 0.15,0.16,0.17,0.188,0.19,0.187,0.188,0.19,0.195,0.199,0.2,0.19,0.21] }
      ]
   }',
]);
?>
<?php
//echo Html::a('Mining coin', [''], ['class' => 'btn btn-warning']);
?>
	<div class="row">
		<div class="col-sm-6" style="background-color: darkred">
			<h2 style="font-weight: bold; text-align: center">Buy</h2>
			<?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
			<?= $form->field($model_buy, 'quantity')->textInput(['readOnly' => true]) ?>
			<?= $form->field($model_buy, 'price')->textInput(['readOnly' => true,'value'=>0.362]) ?>
			<?= $form->field($model_buy, 'cash')->textInput(['readOnly' => true]) ?>
			<div class="col-sm-offset-3">
				<?= Html::submitButton($model_buy ? 'Buy' : 'Update', ['class' => $model_buy ? 'btn btn-success col-sm-3' : 'btn btn-primary']) ?>
			</div>
			<?php ActiveForm::end(); ?>
		</div>
		<div class="col-sm-6" style="background-color: #00aa00">
			<h2 style="font-weight: bold; text-align: center">Sell</h2>

			<?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
			<?= $form->field($model_sell, 'quantity')->textInput(['readOnly' => true]) ?>
			<?= $form->field($model_sell, 'price')->textInput(['readOnly' => true,'value'=>0.36]) ?>
			<?= $form->field($model_sell, 'cash')->textInput(['readOnly' => true]) ?>
			<div class="col-sm-offset-3">
				<?= Html::submitButton($model_sell ? 'Sell' : 'Update', ['class' => $model_sell ? 'btn btn-success col-sm-3' : 'btn btn-primary']) ?>
			</div>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
<?= GridView::widget([
	'dataProvider' => $dataProvider,
	'columns'      => [
		['class' => 'yii\grid\SerialColumn'],
		'id',
		'start_date',
		'begin_return_coin',
		'quantity',
	],
]);
