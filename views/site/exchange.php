<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 3/25/2017
 * Time: 4:52 PM
 */
use app\models\TokenTransaction;
use miloschuman\highcharts\Highcharts;
use yii\bootstrap\Html;
use yii\grid\GridView;

?>
<div class="col-sm-8">
	<?php
	echo \miloschuman\highcharts\Highcharts::widget([
		'options' => [
			"title"  => ["text" => "Tokens price"],
			"xAxis"  => [
				"categories" => $date_array,
			],
			"yAxis"  => [
				"title" => ["text" => "Price"],
			],
			"series" => [
				[
					"name" => "Day price",
					"data" => $price_array,
				],
			],
		],
	]);
	?>
</div>
<div class="col-sm-4">
	<?php
	echo Highcharts::widget([
		'options' => [
			'title'       => ['text' => 'Split indicator'],
			'plotOptions' => [
				'pie' => [
					'cursor'     => 'pointer',
					'dataLabels' => [
						'enabled' > true,
						'format' => '<b>{point.name}</b>: {point.percentage:.1f} %',
					],
				],
			],
			'tooltip'     => [
				'pointFormat' => '{series.name}: <b>{point.percentage:.1f}%</b>',
			],
			'series'      => [
				[ // new opening bracket
					'type' => 'pie',
					'name' => 'Split',
					'data' => [
						[
							'Split',
							$current_token->split_percent,
						],
						[
							'Left',
							100 - $current_token->split_percent,
						],
					],
				]
				// new closing bracket
			],
		],
	]);
	?>
</div>
<div class="panel panel-info">
	<div class="panel-heading">Current Tokens</div>
	<div class="panel-body font-bonus">
		<div class="col-sm-6">
			<span style="font-weight: bold">Current tokens price: </span><?= $current_token->token_price ?></div>
		<div class="col-sm-6">
			<span style="font-weight: bold">Current marketcoins price: </span><?= $current_token->coin_price ?></div>
		<div class="col-sm-6"><span style="font-weight: bold">Your tokens: </span><?= $token ?></div>
		<div class="col-sm-6"><span style="font-weight: bold">Your split tokens: </span><?= $token ?></div>
		<div class="col-sm-6"><span style="font-weight: bold">Your total tokens: </span><?= $total_tokens * 2 ?></div>
		<div class="col-sm-6"><span style="font-weight: bold">Your trading accounts: </span><?= $trading ?></div>
		<div class="col-sm-6"><span style="font-weight: bold">Your marketcoins: </span><?= $coin ?></div>
		<div class="col-sm-6">
			<span style="font-weight: bold">Your available selling tokens: </span><?= $available_token ?>
		</div>

	</div>
</div>
<?php
echo $this->render('tokens_sell', ['model' => $model]);
?>
<?php echo $this->render('token-mining', ['model' => $model_token]) ?>
<?php echo $this->render('trading-mining', ['model' => $model_trading]) ?>
<?= GridView::widget([
	'dataProvider' => $dataProvider,
	//        'filterModel' => $searchModel,
	'columns'      => [
		['class' => 'yii\grid\SerialColumn'],
		'id',
		[
			'attribute' => 'type',
			'value'     => function (TokenTransaction $data) {
				return $data::TYPE[$data->type];
			},
		],
		'date',
		'quantity',
		// 'price',
		'money',
		[
			'attribute' => 'status',
			'value'     => function (TokenTransaction $data) {
				return $data::STATUS[$data->status];
			},
		],
	],
]); ?>
