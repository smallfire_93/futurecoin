<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 4/23/2017
 * Time: 8:26 PM
 */
/** @var app\models\UserKyc $model */
use kartik\file\FileInput;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$this->title = 'KYC';
?>
<?php
if(!$model->isNewRecord) {
	echo '<p style="font-size: 25px">Your KYC status is ' . '<span style="color: red; font-weight: bold">' . $model::STATUS[$model->kyc_status] . '</span></p>';
}
?>
<?php
if($model->isNewRecord || $model->kyc_status !== $model::KYC_ACCEPT) {
	$form = ActiveForm::begin([
		'layout'  => 'horizontal',
		'options' => [
			'enctype' => 'multipart/form-data',
		],
	]);
	?>
	<h3>ID/passport*:</h3>
	<?php
	echo $form->field($model, 'my_frond')->widget(FileInput::className(), [
		'options'       => [
			'accept' => 'image/*',
		],
		'pluginOptions' => [
			'allowedFileExtensions' => [
				'jpg',
				'gif',
				'png',
			],
			'showUpload'            => false,
			'initialPreview'        => $model->getIsNewRecord() ? [
				Html::img(Yii::$app->urlManager->baseUrl . '/uploads/no_image_thumb.gif', ['class' => 'file-preview-image']),
			] : [
				Html::img($model->getPictureUrl('id_frond'), ['class' => 'file-preview-image']),
			],
		],
	]);
	echo $form->field($model, 'my_back')->widget(FileInput::className(), [
		'options'       => [
			'accept' => 'image/*',
		],
		'pluginOptions' => [
			'allowedFileExtensions' => [
				'jpg',
				'gif',
				'png',
			],
			'showUpload'            => false,
			'initialPreview'        => $model->getIsNewRecord() ? [
				Html::img(Yii::$app->urlManager->baseUrl . '/uploads/no_image_thumb.gif', ['class' => 'file-preview-image']),
			] : [
				Html::img($model->getPictureUrl('id_back'), ['class' => 'file-preview-image']),
			],
		],
	])
	?>
	<h3>Proof of address:</h3>
	<?php echo $form->field($model, 'my_address')->widget(FileInput::className(), [
		'options'       => [
			'accept' => 'image/*',
		],
		'pluginOptions' => [
			'allowedFileExtensions' => [
				'jpg',
				'gif',
				'png',
			],
			'showUpload'            => false,
			'initialPreview'        => $model->getIsNewRecord() ? [
				Html::img(Yii::$app->urlManager->baseUrl . '/uploads/no_image_thumb.gif', ['class' => 'file-preview-image']),
			] : [
				Html::img($model->getPictureUrl('address_proof'), ['class' => 'file-preview-image']),
			],
		],
	]);
	?>
	<div class="col-sm-offset-3">
		<?php
		echo Html::submitButton('Submit', ['class' => 'btn btn-success'])
		?>
	</div>
	<?php
	ActiveForm::end();
	?>
<?php } ?>

