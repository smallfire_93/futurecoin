<?php
use app\assets\LoginAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

Yii::$app->layout = false;
LoginAsset::register($this);
?>
<?php $this->beginPage() ?>
<?php
$this->title                   = 'Login';
$this->params['breadcrumbs'][] = $this->title; ?>
	<!DOCTYPE html>
	<html lang="en">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
	<head>
		<meta charset="<?= Yii::$app->charset ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?= Html::csrfMetaTags() ?>
		<title><?= Html::encode($this->title) ?></title>
		<?php $this->head() ?>
	</head>
	<!-- END HEAD -->

	<body class=" login">
	<?php $this->beginBody() ?>
	<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
	<div class="menu-toggler sidebar-toggler">
	</div>
	<!-- END SIDEBAR TOGGLER BUTTON -->
	<!-- BEGIN LOGO -->
	<div class="logo">
		<a href="#">
			<?php echo Html::img(Url::base() . '/logo.png', [
				'alt'   => 'futurecoin',
				'width' => '150px',
			]) ?>
		</a>
	</div>
	<!-- END LOGO -->
	<!-- BEGIN LOGIN -->
	<div class="content">
		<!-- BEGIN LOGIN FORM -->
		<?php $form = ActiveForm::begin([
			'id' => 'login-form',
			//		'enableAjaxValidation'   => true,
			//		'enableClientValidation' => false,
			//		'validateOnBlur'         => false,
			//		'validateOnType'         => false,
			//		'validateOnChange'       => false,
		]) ?>
		<h3 class="form-title">Sign In</h3>
		<div class="form-group">
			<?= $form->field($model, 'username', ['labelOptions' => ['class' => 'control-label visible-ie8 visible-ie9']])->textInput([
				'autofocus'   => 'autofocus',
				'class'       => 'form-control form-control-solid placeholder-no-fix',
				'placeholder' => 'Username',
			]) ?>
		</div>
		<div class="form-group">
			<?= $form->field($model, 'password', ['labelOptions' => ['class' => 'control-label visible-ie8 visible-ie9']])->passwordInput([
				'class'       => 'form-control form-control-solid placeholder-no-fix',
				'placeholder' => 'Password',
			]) ?>
		</div>

		<div class="form-actions">
			<?= Html::submitButton('Login', [
				'class'    => 'btn green uppercase',
				'tabindex' => '3',
			]) ?>
			<a href="<?= Url::to(['site/reset']) ?>" id="forget-password" class="forget-password">Forgot Password?</a>
		</div>
		<?php ActiveForm::end(); ?>

		<!-- END FORGOT PASSWORD FORM -->
		<!-- BEGIN REGISTRATION FORM -->
		<!-- END REGISTRATION FORM -->
	</div>
	<div class="copyright">
		2016 © futurecoin Dashboard.
	</div>
	<?php $this->endBody() ?>
	</body>

	</html>
<?php $this->endPage() ?>