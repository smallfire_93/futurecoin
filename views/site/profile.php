<?php
/* @var $this \app\components\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\User */
/* @var $sponsor app\models\User */
use app\models\User;
use borales\extensions\phoneInput\PhoneInput;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title                   = 'Profile';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-sm-12">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Profile 's
				<strong class="text-custom"><?= Yii::$app->user->identity->username ?></strong>
			</div>
			<div class="tools">
				<a href="" class="collapse" data-original-title="" title="">
				</a>
				<a href="#" data-toggle="modal" class="config" data-original-title="" title="">
				</a>
				<a href="" class="reload" data-original-title="" title="">
				</a>
				<a href="" class="remove" data-original-title="" title="">
				</a>
			</div>
		</div>
		<div class="portlet-body form">

			<?php $form = ActiveForm::begin([
				'id'          => 'login-form',
				'layout'      => 'horizontal',
				'fieldConfig' => [
					'template' => "{label}\n<div class=\"col-lg-8\">{input}</div>\n<div class=\"col-lg-offset-3 col-lg-8\">{error}</div>",
				],
				'options'     => [
					'class' => 'mws-form',
				],
			]); ?>
			<div class="col-sm-6" style="margin-top: 25px">
				<?= $form->field($model, 'sponsor', ['inputTemplate' => "<div class=\"col-lg-6 row\">{input}</div>",])->textInput([
					'value'    => $this->user->sponsors ? $this->user->sponsors->username : 'none',
					'readonly' => 'readonly',
				]) ?>
				<?= $form->field($model, 'username')->textInput([
					'autofocus' => true,
					'readOnly'  => true,
				]) ?>
				<?= $form->field($model, 'full_name')->textInput([
					'autofocus' => true,
					'readOnly'  => true,
				]) ?>
				<?= $form->field($model, 'email')->textInput([
					'autofocus' => true,
					'readOnly'  => $model->email,
				]) ?>
				<?php echo $form->field($model, 'phone')->widget(PhoneInput::className(), [
					'jsOptions' => [
						'nationalMode' => false
					],
					'options'=>['readOnly'=>true]
				]);?>

				<?= $form->field($model, 'address')->textInput([
					'readOnly' => $model->address != null ? true : false,
				]) ?>
				<?= $form->field($model, 'city')->textInput([
					'readOnly' => $model->city != null ? true : false,
				]) ?>

				<?= $form->field($model, 'zip_code')->textInput([
					'readOnly' => $model->zip_code != null ? true : false,
				]) ?>
				<?php if($url != null) { ?>
					<?= $form->field($model, 'url')->textInput([
						'autofocus' => true,
						'value'     =>$url,
					]) ?>
				<?php } ?>
			</div>
			<div class="col-sm-6" style="margin-top: 25px">
				<?= $form->field($model, 'id_number')->textInput([
					'readOnly' => $model->id_number != null ? true : false,
				]) ?>
				<?= $form->field($model, 'expire_date')->widget(DatePicker::className(), [
					'options' => [
						'readOnly' => $model->expire_date != null ? true : false,
					],
					'pluginOptions' => [
						'format' => 'yyyy-mm-dd',
						'todayHighlight' => true
					]
				]) ?>

				<?= $form->field($model, 'bit_coin_address')->textInput([
					'maxlength' => true,
					'readOnly'  => $model->bit_coin_address != null ? true : false,
				]) ?>

			</div>
			<div class="col-sm-12" style="margin-top: 15px">
				<div class="form-group">
					<label class="col-lg-3 control-label">

					</label>
					<div class="col-lg-11">
						<?php echo Html::submitButton('Submit', [
							'class' => 'btn btn-primary',
							'name'  => 'login-button',
						]) ?>
					</div>
				</div>
			</div>
		</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>

