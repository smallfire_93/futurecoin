<?php
/* @var $this \app\components\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\form\SignUpForm */
/* @var $sponsor app\models\User */
use app\models\User;
use borales\extensions\phoneInput\PhoneInput;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title                   = 'Sign Up';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-sm-12">
	<div class="portlet box blue">
		<div class="portlet-title">
			<div class="caption">
				<i class="fa fa-gift"></i>Sign Up to <strong class="text-custom">Market Coin</strong>
			</div>
			<div class="tools">
				<a href="" class="collapse" data-original-title="" title="">
				</a>
				<a href="#" data-toggle="modal" class="config" data-original-title="" title="">
				</a>
				<a href="" class="reload" data-original-title="" title="">
				</a>
				<a href="" class="remove" data-original-title="" title="">
				</a>
			</div>
		</div>
		<div class="portlet-body form">

			<?php $form = ActiveForm::begin([
				'id'                   => 'login-form',
				'layout'               => 'horizontal',
				'fieldConfig'          => [
					'template' => "{label}\n<div class=\"col-lg-8\">{input}</div>\n<div class=\"col-lg-offset-3 col-lg-8\">{error}</div>",
				],
				'options'              => [
					'class' => 'mws-form',
				],
				'enableAjaxValidation' => true,
			]); ?>
			<div class="col-sm-6" style="margin-top: 25px">
				<?= $form->field($model, 'sponsor', ['inputTemplate' => "<div class=\"col-lg-6 row\">{input}</div>",])->textInput([
					'value'    => $sponsor->username,
					'readonly' => 'readonly',
				]) ?>
				<?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
				<?= $form->field($model, 'password')->passwordInput() ?>
				<?= $form->field($model, 'password_confirm')->passwordInput() ?>
				<?= $form->field($model, 'password_2')->passwordInput() ?>
				<?= $form->field($model, 'password_2_confirm')->passwordInput() ?>

			</div>
			<div class="col-sm-6" style="margin-top: 25px">
				<?= $form->field($model, 'position', ['inputTemplate' => "<div class=\"col-lg-6 row\">{input}</div>",])->dropDownList(User::POSITIONS, ['disabled' => $key != null ? true : false]) ?>
				<?= $form->field($model, 'full_name')->textInput(['autofocus' => true]) ?>
				<?php echo $form->field($model, 'phone')->widget(PhoneInput::className(), [
					'jsOptions' => [
						'nationalMode' => false,
						'preferredCountries'=>['gb']
					],
				]); ?>
				<?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

				<div class="col-sm-12" style="margin-top: 15px">
					<?= $form->field($model, 'agree')->checkbox([
						'template' => "<div class=\"col-lg-12 checkbox checkbox-primary\"><div class= \"col-lg-6\">{input} {label}</div></div>\n<div class=\"col-lg-offset-3 col-lg-8\">{error}</div>",
					]) ?>
					<div class="form-group">
						<label class="col-lg-3 control-label">

						</label>
						<div class="col-lg-11">
							<?= Html::submitButton('Submit', [
								'class' => 'btn btn-primary',
								'name'  => 'login-button',
							]) ?>
						</div>
					</div>
				</div>
			</div>

		</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>

