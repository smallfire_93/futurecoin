<?php
use app\assets\LoginAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

Yii::$app->layout = false;
LoginAsset::register($this);
?>
<?php $this->beginPage() ?>
<?php
$this->title                   = 'Reset';
$this->params['breadcrumbs'][] = $this->title; ?>
	<!DOCTYPE html>
	<html lang="en">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
	<head>
		<meta charset="<?= Yii::$app->charset ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?= Html::csrfMetaTags() ?>
		<title><?= Html::encode($this->title) ?></title>
		<?php $this->head() ?>
	</head>
	<!-- END HEAD -->

	<body class=" login">
	<?php $this->beginBody() ?>
	<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
	<div class="menu-toggler sidebar-toggler">
	</div>
	<!-- END SIDEBAR TOGGLER BUTTON -->
	<!-- BEGIN LOGO -->
	<div class="logo">
		<a href="#">
			<?php echo Html::img(Url::base() . '/logo.png', [
				'alt'   => 'marketcoin',
				'width' => '150px',
			]) ?>
		</a>
		<div class="col-sm-12"><?php echo Html::img(Url::base() . '/logo_name.png', [
				'alt'   => 'marketcoin',
				'width' => '300px',
			]) ?></div>
	</div>
	<!-- END LOGO -->
	<!-- BEGIN LOGIN -->
	<div class="content">
		<!-- BEGIN LOGIN FORM -->

		<!-- BEGIN FORGOT PASSWORD FORM -->
		<?php $form = ActiveForm::begin([
			'class'                => 'forget-form',
			'enableAjaxValidation' => true,
		]) ?>
		<h3>Forget Password ?</h3>
		<p>
			Enter your e-mail address below to reset your password.
		</p>
		<div class="form-group">
			<?= $form->field($model, 'email')->textInput(['class' => 'form-control placeholder-no-fix']) ?>
		</div>
		<div class="form-group">
			<?= $form->field($model, 'username')->textInput(['class' => 'form-control placeholder-no-fix']) ?>
		</div>
		<div class="form-actions">
			<?= Html::submitButton('Submit', [
				'class' => 'btn btn-success uppercase pull-right',
				//				'tabindex' => '3',
			]) ?>
		</div>
		<?php ActiveForm::end(); ?>

		<!--		<form class="forget-form" action="index.html" method="post">-->
		<!--			<h3>Forget Password ?</h3>-->
		<!--			<p>-->
		<!--				Enter your e-mail address below to reset your password.-->
		<!--			</p>-->
		<!--			<div class="form-group">-->
		<!--				<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>-->
		<!--			</div>-->
		<!--			<div class="form-actions">-->
		<!--				<button type="button" id="back-btn" class="btn btn-default">Back</button>-->
		<!--				<button type="submit" class="btn btn-success uppercase pull-right">Submit</button>-->
		<!--			</div>-->
		<!--		</form>-->
		<!-- END FORGOT PASSWORD FORM -->
		<!-- BEGIN REGISTRATION FORM -->
		<!-- END REGISTRATION FORM -->
	</div>
	<div class="copyright">
		2016 © MarketCoin Dashboard.
	</div>
	<?php $this->endBody() ?>
	</body>

	</html>
<?php $this->endPage() ?>