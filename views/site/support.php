<?php
/**
 * Created by PhpStorm.
 * User: Yamon-PC
 * Date: 05-Jun-17
 * Time: 11:36 AM
 */
use yii\bootstrap\ActiveForm;

/** @var app\models\Support $ticket */
?>
<h1>Contact us for more</h1>
<?php
$form = ActiveForm::begin(['layout' => 'horizontal']);
echo $form->field($model, 'title')->textInput()->label('Title', ['style' => 'color:blue; font-weight:bold']);
echo $form->field($model, 'content')->textarea([
	'rows' => 6,
])->label('Content', ['style' => 'color:blue; font-weight:bold']);
?>
<div class="col-sm-offset-3">
	<?php
	echo \yii\bootstrap\Html::submitButton('Send', ['class' => 'btn btn-success']);
	?>
</div>
<?php
ActiveForm::end()
?>
<h2>List subject</h2>
<?php
$i = 0;
foreach($tickets as $ticket) {
	$i ++;
	?>
	<p class="tickets"><?= $i ?> -
		<a class="<?= $ticket->status == $ticket::STATUS_UNREAD ? 'unread' : 'read' ?>" href="<?= \yii\helpers\Url::to([
			'/site/support-view',
			'id' => $ticket->id,
		]) ?>"><?= $ticket->title ?></a></p>
	<?php
}
?>
