<?php
/**
 * Created by PhpStorm.
 * User: Yamon-PC
 * Date: 28-Mar-17
 * Time: 7:13 PM
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;

?>
<?php
$form = ActiveForm::begin([
	'id'                   => 'check-form',
	'action'               => Url::to([
		'/site/tokens-sell',
	]),
	'layout'               => 'horizontal',
	'enableAjaxValidation' => true,
	'options'              => ['style' => 'display:inline-block'],
]);
Modal::begin([
	'header'       => '<h2>Sell tokens</h2>',
	'toggleButton' => [
		'label' => 'Sell tokens',
		'class' => 'btn btn-success',
	],
]);
echo $form->field($model, 't_password')->passwordInput();
echo $form->field($model, 'quantity')->textInput();
?>
<p><span style="color: red">*</span> Note: Selling fee is 5%.</p>
<?php
echo Html::submitButton('Sell-tokens', ['class' => 'btn btn-danger']);
Modal::end(); ?>

<?php
ActiveForm::end();
?>

