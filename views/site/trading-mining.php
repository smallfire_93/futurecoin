<?php
/**
 * Created by PhpStorm.
 * User: Yamon-PC
 * Date: 28-Mar-17
 * Time: 7:13 PM
 */
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;

?>
<?php
$form = ActiveForm::begin([
	'id'                   => 'mining-form',
	'action'               => Url::to([
		'/site/trading-mining',
	]),
	'layout'               => 'horizontal',
	'enableAjaxValidation' => true,
	'options'              => ['style' => 'display:inline-block'],
]);
Modal::begin([
	'header'       => '<h2>Mining with trading</h2>',
	'toggleButton' => [
		'label' => 'Mining with trading accounts',
		'class' => 'btn btn-warning',
	],
]);
echo $form->field($model, 't_password')->passwordInput();
echo $form->field($model, 'quantity')->textInput();
echo Html::submitButton('trading-mining', ['class' => 'btn btn-danger']);
Modal::end(); ?>

<?php
ActiveForm::end();
?>

