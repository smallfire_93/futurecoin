<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TokenTransaction */

$this->title = 'Create Token Transaction';
$this->params['breadcrumbs'][] = ['label' => 'Token Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="token-transaction-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
