<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TokenTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Token History';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="token-transaction-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<div class="panel panel-info">
		<div class="panel-heading">Current Bonus</div>
		<div class="panel-body font-bonus">
			<div class="col-sm-6"><span style="font-weight: bold">Available selling tokens: </span><?= $tokens ?> </div>
			<div class="col-sm-6"><span style="font-weight: bold">Total tokens: </span><?= $total_tokens ?></div>
		</div>
	</div>
	<div class="table-responsive">

	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		//        'filterModel' => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			'id',
			[
				'attribute' => 'type',
				'value'     => function (\app\models\TokenTransaction $data) {
					return $data::TYPE[$data->type];
				},
			],
			'date',
			'quantity',
			// 'price',
			'money',
			'status',
		],
	]); ?>
	</div>
</div>
