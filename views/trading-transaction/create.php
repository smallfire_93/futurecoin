<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TradingTransaction */

$this->title = 'Create Trading Transaction';
$this->params['breadcrumbs'][] = ['label' => 'Trading Transactions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trading-transaction-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
