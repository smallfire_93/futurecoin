<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TradingTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Trading Transactions';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trading-transaction-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	<div class="table-responsive">

		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel'  => $searchModel,
			'columns'      => [
				['class' => 'yii\grid\SerialColumn'],
//				'user_id',
				'type',
				'money',
				'status',
				// 'quantity',
				// 'date',
//				['class' => 'yii\grid\ActionColumn'],
			],
		]); ?>
	</div>
</div>
