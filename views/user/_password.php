<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

?>
<?php $form = ActiveForm::begin(['layout' => 'horizontal']); ?>
<?= $form->field($model, 'new_password')->passwordInput() ?>
<?= $form->field($model, 'new_password_confirm')->passwordInput() ?>
	<div class="form-group">
		<?= Html::submitButton('Update', ['class'=>'btn btn-success']) ?>
	</div>

<?php ActiveForm::end(); ?>