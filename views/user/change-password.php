<?php
use yii\helpers\Html;

$this->title                   = 'Change password';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<?= $this->render('_password', [
	'model' => $model,
]) ?>

