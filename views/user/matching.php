<?php
use app\models\User;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UserSearch */
/* @var $model app\models\User */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Users';
$this->params['breadcrumbs'][] = $this->title;
\app\assets\TreeAsset::register($this)
?>

<h1><?= Html::encode($this->title) ?></h1>
<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<ul id="org-matching" style="display:none">
	<li>
		<?php if($model->user->status == $model::STATUS_ACTIVATED) { ?>
			<img src="<?= Url::base() ?>/tree/image/matching_active.png" alt="" style="display: block;margin: 0 auto;">
		<?php } else { ?>
			<img src="<?= Url::base() ?>/tree/image/matching_inactive.png" alt="" style="display: block;margin: 0 auto;">
		<?php } ?>
		<?= $model->user->username ?>
		<?php $model->printMatching($children) ?>
	</li>
</ul>
<div class="panel panel-info col-sm-12">
	<div class="panel-heading">Matching Tree</div>
	<div class="panel-body">
		<div id="chart-matching" class="orgChart" style="margin: 0 auto"></div>
	</div>
</div>
<div>
	<?php
	echo GridView::widget([
		'dataProvider' => $dataProvider,
		//        'filterModel' => $searchModel,
		'columns'      => [
			['class' => 'yii\grid\SerialColumn'],
			'username',
			//			[
			//				'attribute' => 'type',
			//				'value'     => function (\app\models\TokenTransaction $data) {
			//					return $data::TYPE[$data->type];
			//				},
			//			],
			'phone',
			'email',
			[
				'attribute' => 'package',
				'value'     => function (User $data) {
					if(isset($data->accounts->package_id)) {
						return $data->accounts->package->name;
					} else {
						return '';
					}
				},
			],
			// 'price',
			//			'money',
			[
				'attribute' => 'status',
				'value'     => function (User $data) {
					return $data::STATUS[$data->status];
				},
			],
		],
	]) ?>
</div>
<script>

</script>
