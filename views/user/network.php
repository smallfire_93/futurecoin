<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UserSearch */
/* @var $model app\models\User */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title                   = 'Network tree';
$this->params['breadcrumbs'][] = $this->title;
\app\assets\TreeAsset::register($this)
?>

<h1><?= Html::encode($this->title) ?></h1>
<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<ul id="org" style="display:none">

	<?php $model->printTree($children) ?>

</ul>
<div class="panel panel-info col-sm-12">
	<div class="panel-heading">Network Tree</div>
	<div class="panel-body">

		<div class="col-sm-12">
			<img src="<?= Url::base() ?>/tree/image/network_2.png" alt="" style="width: 70px;height: 67px"> ACTIVE USER
			<img src="<?= Url::base() ?>/tree/image/network.png" alt="" style="width: 70px;height: 67px"> INACTIVE USER
			<img src="<?= Url::base() ?>/tree/image/sign-up.png" alt="" style="opacity: 0.5;width: 70px;height: 67px"> CREATE NEW USER
		</div>
		<div class="clearfix btn-holder">
		</div>
		<div id="chart" class="orgChart col-xs-12 table-responsive" style="margin: 0 auto"></div>
		<div class="clearfix btn-holder">
			<?php echo Html::a('<i class="fa fa-arrow-down" aria-hidden="true"></i> First available position left', [
				'/user/network',
				'key' => $left ? $left->password : null,
			], [
				'class'    => 'btn btn-sm btn-success pull-left',
				'disabled' => $left ? false : true,
			]) ?>
			<?php echo Html::a('<i class="fa fa-arrow-down" aria-hidden="true"></i> First available position right', [
				'/user/network',
				'key' => $right ? $right->password : null,
			], [
				'class'    => 'btn btn-sm btn-success pull-right',
				'disabled' => $right ? false : true,
			]) ?>
		</div>
	</div>
</div>

<script>

</script>
