/**
 * Created by dev on 4/18/2017.
 */
$(document).on("change", "#quantity", function() {
	$("#quantity-update").text($(this).val());
	var total = $(this).data("price") * $(this).val();
	$(".total-update").text("€ " + total)
})